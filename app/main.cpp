
extern void user_init();
extern void run_tests();

void init() {
#if !defined(RUN_TESTS) || RUN_TESTS == 0 // rebuild required!
  user_init();
#else
  run_tests();
#endif
}
