#include <HardwareSerial.h>
#include <Platform/System.h>

#include <assert.h>

#if defined(RUN_TESTS) && RUN_TESTS > 0
#include <functional>

std::function<void(const char *file, int line, const char *f, const char *msg)>
    assert_catcher;

extern "C" void __my_assert_fun(const char *__assertion, const char *__file,
                                unsigned int __line, const char *__function) {
  if (assert_catcher) {
    assert_catcher(__file, __line, __function, __assertion);
  } else {
    __assert_func(__file, __line, __function, __assertion);
  }
}
#else
extern "C" void __my_assert_fun(const char *__assertion, const char *__file,
                                unsigned int __line, const char *__function) {
  __assert_func(__file, __line, __function, __assertion);
}
#endif

extern "C" void __assert_func(const char *file, int line, const char *f,
                              const char *msg) {
  Serial.printf("At %s line %d: %s\n\n", file, line, msg);
  System.restart();
  while (1)
    ;
}
