#include "timerexecutor.h"

/// works only with
/// https://github.com/ololoshka2871/Sming/commit/809cc0ebe8c2d011814decf24f7072e35c9f59b2

ITimer &TimerExecutor::initializeMs(const uint32_t milliseconds,
                                    const TimerCB &cb) {
  timer.initializeMs(milliseconds, cb);
  return *this;
}

ITimer &TimerExecutor::initializeUs(const uint32_t microseconds,
                                    const TimerCB &cb) {
  timer.initializeUs(microseconds, cb);
  return *this;
}
