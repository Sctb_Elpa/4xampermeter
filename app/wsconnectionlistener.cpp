#include "wsconnectionlistener.h"

#include <Network/Http/Websocket/WebsocketResource.h>
#include <Network/Http/Websocket/WebsocketConnection.h>
#include <Network/HttpServer.h>

#include "make_unique.h"

WSConnectionListener::WSConnectionListener()
    : wsResource(new WebsocketResource()) {
  wsResource->setConnectionHandler(WebsocketDelegate(
      [this](WebsocketConnection &socket) {
        this->WSConnectionListener::wsConnected(socket); 
      }));
  wsResource->setMessageHandler(WebsocketMessageDelegate(
      [this](WebsocketConnection &socket, const String &message) {
        this->WSConnectionListener::wsOnMessage(socket, message);
      }));
  wsResource->setBinaryHandler(WebsocketBinaryDelegate(
      [this](WebsocketConnection &socket, uint8_t *data, size_t size) {
        WSConnectionListener::wsOnBinaryDataRessived(socket, data, size);
      }));
  wsResource->setDisconnectionHandler(WebsocketDelegate(
      [this](WebsocketConnection &socket) {
        WSConnectionListener::wsOnDisconnect(socket);
      }));
}

WSConnectionListener::~WSConnectionListener() { delete wsResource; }

void WSConnectionListener::registerOnServer(HttpServer *server,
                                            const String &path) {
  server->paths.set(path, wsResource);
}

void WSConnectionListener::wsConnected(WebsocketConnection &socket) {
  debugf("Websocket: client connected");
  socket.sendString("Connected");
}

void WSConnectionListener::wsOnMessage(WebsocketConnection &socket,
                                       const String &message) {
  debugf("Websocket: ressived unexpected string: %s\n", message.c_str());
  socket.sendString("Unexpected");
}

void WSConnectionListener::wsOnBinaryDataRessived(WebsocketConnection &socket,
                                                  uint8_t *data, size_t size) {
  std::vector<uint8_t> response;
  auto result = runProcessor(data, size, response);
  if (result)
    socket.sendBinary(response.data(), response.size());
}

void WSConnectionListener::wsOnDisconnect(WebsocketConnection &socket) {
  debugf("Websocket: client disconnected");
}
