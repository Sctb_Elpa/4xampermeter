#include <HardwareSerial.h>
#include <Platform/System.h>

namespace std {

void __throw_bad_alloc() {
  Serial.println("Unable to allocate memory");
  System.restart();
  for (;;)
    ;
}

void __throw_length_error(char const *e) {
  Serial.print("Length Error :");
  Serial.println(e);
  System.restart();
  for (;;)
    ;
}

void __throw_bad_function_call() {
  Serial.println("Bad function call");
  System.restart();
  for (;;)
    ;
}

} // namespace std
