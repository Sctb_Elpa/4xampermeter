#ifndef __TEST__
#include "timerexecutor.h"
#endif

#include "ITimer.h"

std::unique_ptr<ITimer> ITimer::makeTimer() {
#ifdef __TEST__
  return std::make_unique<TestTimer>();
#else
  return std::make_unique<TimerExecutor>();
#endif
}
