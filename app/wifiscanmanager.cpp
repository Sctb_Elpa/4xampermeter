#include <algorithm>
#include <list>
#include <vector>

#include "pb_encode.h"
#include "protocol.pb.h"

#include "Settings.h"
#include "vectoritherator.h"

#include "wifiscanmanager.h"

#include <Wiring/WVector.h>

#if defined(__TEST__) || 0
#define wsm_diag_msg(...)
#else
#include <SmingCore.h>
#define wsm_diag_msg(...) debugf(__VA_ARGS__)
#endif

#include "utils.h"

class WifiNetworksHolder {
public:
  WifiNetworksHolder(const BssList &sortedbssLst) {
    VectorItherator<BssInfo> it(sortedbssLst);
    VectorItherator<BssInfo> end(sortedbssLst, sortedbssLst.size());
    while (it != end) {
      WifiScanNetworkInfoList.emplace_back(
          std::move(WifiScanNetworkInfoHolder::makeNetworkInfo(it, end)));
    }
  }

  void fillCallbacks(pb_callback_t &field) const {
    field.arg = (void *)this;
    field.funcs.encode = encode_WifiScanNetworkInfo;
  }

private:
  static bool encode_WifiScanNetworkInfo(pb_ostream_t *stream,
                                         const pb_field_t *field,
                                         void *const *arg) {
    auto &list =
        static_cast<WifiNetworksHolder *>(*arg)->WifiScanNetworkInfoList;

    for (auto it = list.begin(); it != list.end(); ++it) {
      if (!pb_encode_tag_for_field(stream, field))
        return false;
      if (!pb_encode_submessage(
              stream, ru_sktbelpa_AmpermeterX4_WifiScanNetworkInfo_fields,
              it->updatePointers()))
        return false;
    }

    return true;
  }

  class Bssid_infoHolder : public ru_sktbelpa_AmpermeterX4_Bssid_info {
  public:
    Bssid_infoHolder(const BssInfo &bssInfo)
        : ru_sktbelpa_AmpermeterX4_Bssid_info{
              .bssid = 0, .channel = bssInfo.channel, .rssi = bssInfo.rssi} {
      memcpy(&bssid, &bssInfo.bssid, sizeof(BssInfo::bssid));
    }

    void print() const {
      wsm_diag_msg("\t\t{ bssid = %X%X, chanel = %d, rssi = %d },",
                   (int)(bssid >> 32), (int)bssid, (int)channel, (int)rssi);
    }
  };

  class WifiScanNetworkInfoHolder
      : public ru_sktbelpa_AmpermeterX4_WifiScanNetworkInfo {
  public:
    static WifiScanNetworkInfoHolder
    makeNetworkInfo(VectorItherator<BssInfo> &it,
                    const VectorItherator<BssInfo> &end) {
      WifiScanNetworkInfoHolder res(*it);

      while (res._ssid == it->ssid) {
        res.bssid_data.emplace_back(*it);
        ++it;
        if (it == end)
          break;
      }

      return res;
    }

    WifiScanNetworkInfoHolder(const BssInfo &cit)
        : ru_sktbelpa_AmpermeterX4_WifiScanNetworkInfo

          {.ssid = {.funcs = {.encode = Utils::arg2string_pb_encoder<String>}},
           .authorization = static_cast<
               ru_sktbelpa_AmpermeterX4_WifiScanNetworkInfo_AUTH_MODE>(
               cit.authorization),
           .hidden = cit.hidden,
           .Bssid_info_list = {.funcs = {.encode = encode_bssid_data}}},
          _ssid(std::move(cit.ssid)) {}

    ru_sktbelpa_AmpermeterX4_WifiScanNetworkInfo *updatePointers() {
      Bssid_info_list.arg = this;
      ssid.arg = &_ssid;
      return this;
    }

    void print() const {
      wsm_diag_msg("SSID: %s\n"
                   "\t-> authorization = %d\n"
                   "\t-> hidden = %d",
                   _ssid.c_str(), (int)authorization, (int)hidden);
      wsm_diag_msg("\t-> bssids: [");
      for (auto it = bssid_data.cbegin(); it != bssid_data.cend(); ++it) {
        it->print();
      }
      wsm_diag_msg("\t],");
    }

  private:
    static bool encode_bssid_data(pb_ostream_t *stream, const pb_field_t *field,
                                  void *const *arg) {
      const auto &list =
          static_cast<const WifiScanNetworkInfoHolder *>(*arg)->bssid_data;

      for (auto it = list.cbegin(); it != list.cend(); ++it) {
        if (!pb_encode_tag_for_field(stream, field))
          return false;

        if (!pb_encode_submessage(
                stream, ru_sktbelpa_AmpermeterX4_Bssid_info_fields, &(*it)))
          return false;
      }
      return true;
    }

    String _ssid;
    std::vector<Bssid_infoHolder> bssid_data;
  };

  //-------------------------------------

  // no std::list api in this implementation of stdc++
  std::vector<WifiScanNetworkInfoHolder> WifiScanNetworkInfoList;
};

WifiScanManager::WifiScanManager()
    : wifiNetworksHolder(nullptr), isScanningNow(false) {}

WifiScanManager::~WifiScanManager() {}

void WifiScanManager::StartScan() {
  if (!settings.s.isStationEnabled) {
    WifiStation.enable(true);
  }
  wifiNetworksHolder.reset();
  isScanningNow = true;
  WifiStation.startScan(
      ScanCompletedDelegate([this](bool sucess, BssList bssList) {
        this->WifiScanManager::scanComplead(sucess, bssList);
      }));
}

void WifiScanManager::fillData(
    ru_sktbelpa_AmpermeterX4_WifiScanResult &field) const {
  if (isScanningNow) {
    field.scanStatus =
        ru_sktbelpa_AmpermeterX4_WifiScanResult_ScanStatus_SCAN_PENDING;
  } else if (!wifiNetworksHolder) {
    field.scanStatus =
        ru_sktbelpa_AmpermeterX4_WifiScanResult_ScanStatus_SCAN_IDLE;
  } else {
    field.scanStatus =
        ru_sktbelpa_AmpermeterX4_WifiScanResult_ScanStatus_SCAN_COMPLEAD;

    wifiNetworksHolder->fillCallbacks(field.WifiNetworks);
  }
}

void WifiScanManager::scanComplead(bool sucess, BssList bssList) {
  if (!settings.s.isStationEnabled) {
    WifiStation.enable(false);
  }

  if (sucess) {
    bssList.sort([](const BssInfo &lhs, const BssInfo &rhs) -> int {
      if (lhs.ssid == rhs.ssid)
        return lhs.rssi < rhs.rssi;
      else
        return (lhs.ssid < rhs.ssid) ? -1 : 1;
    });

    wifiNetworksHolder.reset(new WifiNetworksHolder(bssList));
  }

  isScanningNow = false;
}
