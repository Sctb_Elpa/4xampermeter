#include <algorithm>

#include "shaduler.h"

#if defined(__TEST__)
#define shaduler_diag_msg(...)
#else
#include <SmingCore.h>
#define shaduler_diag_msg(...) debugf(__VA_ARGS__)
#endif

Shaduler::Shaduler(uint32_t period)
    : queue(), executor([this]() {
        if (!queue.empty()) {
          DelayedFunction cb(*queue.cbegin());
          queue.erase(queue.begin());
          cb();
        }
      }),
      timer(std::move(ITimer::makeTimer())) {
  setPeriod(period);
}

Shaduler::~Shaduler() { timer->stop(); }

void Shaduler::start() {
  if (!timer->isRunning())
    timer->initializeMs(period, executor).start();
}

void Shaduler::setPeriod(uint32_t period_ms) {
  if (timer->isRunning()) {
    timer->stop();
    timer->initializeMs(period, executor).start();
  }
  period = period_ms;
}

void Shaduler::shadule(const DelayedFunction &task) {
  if (!queue.empty()) {
    if (queue.cend() != std::find_if(queue.cbegin(), queue.cend(),
                                     [&task](const DelayedFunction &item) {
                                       return item == task;
                                     })) {
      shaduler_diag_msg("Task id 0x%X allready shaduled, dropped",
                        task.getID());
      return;
    }
  }
  shaduler_diag_msg("Shaduled task id: 0x%X", task.getID());

  queue.emplace_back(task);
}
