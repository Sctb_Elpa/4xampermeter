#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "crc32.h"

#include <pb_decode.h>
#include <pb_encode.h>

#include "spiffsprotobuffile.h"

#if defined(__TEST__)
#define siffProto_diag_msg(...)
#else
#include <SmingCore.h>
#define siffProto_diag_msg(...) debugf(__VA_ARGS__)
#endif

bool SPIFFSProtobufFile::Encode(const pb_msgdesc_t *fields,
                                const void *src_struct) const {
  pb_ostream_t ostream;
  FileStream fd;

  if (isHasFile()) {
    if (!fd.open(filename, File::Create | File::WriteOnly)) {
      siffProto_diag_msg("Encode failed: can't open \"%s\"", filename.c_str());
      return false;
    }

    ostream = ostreamFromFile(&fd);
  } else {
    ostream = PB_OSTREAM_SIZING;
  }

  auto res = pb_encode(&ostream, fields, src_struct);
  lastEncodedSize = ostream.bytes_written;

  if (isHasFile()) {
    fd.close();
  }

  if (!res) {
    siffProto_diag_msg("Encode failed: %s", ostream.errmsg);
  }

  return res;
}

bool SPIFFSProtobufFile::Decode(const pb_msgdesc_t *fields,
                                void *dest_struct) const {
  if (!isHasFile())
    return false;

   FileStream fd;

  if (!fd.open(filename, File::ReadOnly)) {
    siffProto_diag_msg("Decode failed: No such file \"%s\"", filename.c_str());
    return false;
  }

  auto istream = istreamFromFile(&fd);

  auto res = pb_decode(&istream, fields, dest_struct);

  if (isHasFile()) {
    fd.close();
  }

  if (!res) {
    siffProto_diag_msg("Decode failed: %s", istream.errmsg);
  }

  return res;
}

size_t SPIFFSProtobufFile::size() const {
  if (!isHasFile())
    return lastEncodedSize;

  struct stat _stat;
  return stat(filename.c_str(), &_stat) < 0 ? 0 : _stat.st_size;
}

uint32_t SPIFFSProtobufFile::crc32() const {
  if (!isHasFile())
    return 0;

  FileStream fd;

  if (!fd.open(filename, File::ReadOnly)) {
    siffProto_diag_msg("crc32 failed: No such file \"%s\"", filename.c_str());
    return 0;
  }

  auto res = ::crc32([&fd](uint8_t *res) -> bool {
    return fd.readBytes(res, 1) == 1;
  });

  fd.close();

  return res;
}

pb_istream_t
SPIFFSProtobufFile::istreamFromFile(const SPIFFSProtobufFile::F *fd) {
  return pb_istream_t{
      [](pb_istream_t *stream, pb_byte_t *buf, size_t count) {
        auto _fd = static_cast<SPIFFSProtobufFile::F *>(stream->state);
        auto res = _fd->readBytes(buf, count) == count;
        if (!res)
          stream->bytes_left = 0;
        return res;
      },
      (void *)fd, SIZE_MAX};
}

pb_ostream_t
SPIFFSProtobufFile::ostreamFromFile(const SPIFFSProtobufFile::F *fd) {
  return pb_ostream_t{
      [](pb_ostream_t *stream, const pb_byte_t *buf, size_t count) {
        auto _fd = static_cast<SPIFFSProtobufFile::F *>(stream->state);
        return _fd->write(buf, count) == count;
      },
      (void *)fd, SIZE_MAX, 0};
}
