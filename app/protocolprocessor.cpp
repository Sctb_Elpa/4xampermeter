#include "Monitor.h"
#include "Settings.h"
#include "Setup.h"
#include "sensormanager.h"
#include "settingsvalidators.h"
#include "shaduler.h"
#include "timerexecutor.h"
#include "voltagecontroller.h"
#include "wifiscanmanager.h"

#include "protocolprocessor.h"

#if defined(__TEST__) || 0
#define pp_diag_msg(...)
#else
#include <SmingCore.h>
#define pp_diag_msg(...) debugf(__VA_ARGS__)
#endif

using namespace SettingsValidators;

static WifiScanManager wifiScanManager;

static const uint8_t error_msg[] =
    "This address accepts Protocol Buffer messages only.";

//==============================================================================

static ru_sktbelpa_AmpermeterX4_IPv4
ip2nw(const ru_sktbelpa_AmpermeterX4_IPv4 &v) {
  return ru_sktbelpa_AmpermeterX4_IPv4{.IP_bytes = htonl(v.IP_bytes)};
}

static ru_sktbelpa_AmpermeterX4_IPv4
nw2ip(const ru_sktbelpa_AmpermeterX4_IPv4 &v) {
  return ru_sktbelpa_AmpermeterX4_IPv4{.IP_bytes = ntohl(v.IP_bytes)};
}

void writeMacTo(const uint64_t *dest) {
  auto d = (uint8_t *)dest;
  uint8_t mac[6];
  wifi_get_macaddr(SOFTAP_IF, (uint8_t *)mac);
  for (int8_t i = sizeof(mac) - 1; i >= 0; --i) {
    *d++ = mac[i];
  }
}

//==============================================================================

template <typename T, typename U> static void andFlag(T &flags, U flag) {
  flags = static_cast<T>(flags | flag);
}

//==============================================================================

void SetErrorFlag(ru_sktbelpa_AmpermeterX4_GetSettings &resp, int flag) {
  andFlag(resp.status, flag);
}

static bool
checkIfNetworkNotFullyDisabled(const ru_sktbelpa_AmpermeterX4_SetSettings &ns,
                               ru_sktbelpa_AmpermeterX4_GetSettings &resp) {
  if (ns.has_isAccesPointEnabled | ns.has_isStationEnabled) {
    /// was_enabled | set_enabled | v_set_enabled | warning
    /// ------------+-------------+---------------+---------
    ///      0      |      0      |       0       |   1
    ///      0      |      0      |       1       |   1
    ///      0      |      1      |       0       |   1
    ///      0      |      1      |       1       |   0
    ///      1      |      0      |       0       |   0
    ///      1      |      0      |       1       |   0
    ///      1      |      1      |       0       |   1
    ///      1      |      1      |       1       |   0
    ///
    /// warning = (!was_enabled & !set_enabled) | (set_enabled & !v_set_enabled)
    auto &s = settings.s;

    auto warn = ((!s.isAccesPointEnabled & !ns.has_isAccesPointEnabled) |
                 (ns.has_isAccesPointEnabled & !ns.isAccesPointEnabled)) &
                ((!s.isStationEnabled & !ns.has_isStationEnabled) |
                 (ns.has_isStationEnabled & !ns.isStationEnabled));

    if (warn) {
      SetErrorFlag(
          resp,
          ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_BOTH_STA_AP_DISABLED);
    }

    return !warn;
  }
  return true;
}

static bool update_STA_Settings(const ru_sktbelpa_AmpermeterX4_SetSettings &ns,
                                ru_sktbelpa_AmpermeterX4_GetSettings &resp) {
  if (ns.has_isStationEnabled)
    settings.s.isStationEnabled = ns.isStationEnabled;
  if (ns.has_STA_ESSID) {
    if (ESSIDValidator(sizeof(ru_sktbelpa_AmpermeterX4_SetSettings::STA_ESSID))
            .validate(ns.STA_ESSID)) {
      strcpy(settings.s.STA_ESSID, ns.STA_ESSID);
    } else {
      SetErrorFlag(
          resp,
          ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_STA_ESSID_INCORRECT);
    }
  }
  if (ns.has_STA_password) {
    if (StringLengthValidator().validate(ns.STA_password)) {
      strcpy(settings.s.STA_password, ns.STA_password);
    } else
      SetErrorFlag(
          resp,
          ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_STA_PASSWORD_TOO_LONG);
  }

  resp.isStationEnabled = settings.s.isStationEnabled;
  strcpy(resp.STA_ESSID, settings.s.STA_ESSID);
  strcpy(resp.STA_password, settings.s.STA_password);

  auto r = ns.has_isStationEnabled | ns.has_STA_ESSID | ns.has_STA_password;
  if (r)
    SetErrorFlag(
        resp,
        ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_REBOOT_REQUIRED);

  return r;
}

static bool update_AP_Settings(const ru_sktbelpa_AmpermeterX4_SetSettings &ns,
                               ru_sktbelpa_AmpermeterX4_GetSettings &resp) {
  if (ns.has_isAccesPointEnabled)
    settings.s.isAccesPointEnabled = ns.isAccesPointEnabled;
  if (ns.has_AP_IP) {
    auto ip = nw2ip(ns.AP_IP);
    if (IPValidator().validate(ip.IP_bytes)) {
      settings.s.AP_IP = ip;
    } else
      SetErrorFlag(
          resp,
          ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_AP_WRONG_IP);
  }
  if (ns.has_AP_ESSID) {
    if (ESSIDValidator(sizeof(ru_sktbelpa_AmpermeterX4_SetSettings::AP_ESSID))
            .validate(ns.AP_ESSID)) {
      strcpy(settings.s.AP_ESSID, ns.AP_ESSID);
    } else
      SetErrorFlag(
          resp,
          ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_AP_ESSID_INCORRECT);
  }
  if (ns.has_AP_password) {
    if (StringLengthValidator().validate(ns.AP_password)) {
      strcpy(settings.s.AP_password, ns.AP_password);
    } else
      SetErrorFlag(
          resp,
          ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_AP_WRONG_PASSWORD);
  }

  resp.isAccesPointEnabled = settings.s.isAccesPointEnabled;
  strcpy(resp.AP_ESSID, settings.s.AP_ESSID);
  strcpy(resp.AP_password, settings.s.AP_password);
  resp.AP_IP = ip2nw(settings.s.AP_IP);

  auto r = ns.has_isAccesPointEnabled | ns.has_AP_ESSID | ns.has_AP_password |
           ns.has_AP_IP;
  if (r)
    SetErrorFlag(
        resp,
        ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_REBOOT_REQUIRED);

  return r;
}

static bool
update_measure_interval(const ru_sktbelpa_AmpermeterX4_SetSettings &ns,
                        ru_sktbelpa_AmpermeterX4_GetSettings &resp) {
  if (ns.has_refresh_interval_ms) {
    auto new_update_interval = ns.refresh_interval_ms;
    if (new_update_interval < 25 || new_update_interval > 100) {
      SetErrorFlag(
          resp,
          ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_REFRASH_INTERVAL_OUT_OF_RANGE);
    } else {
      settings.s.update_interval_ms = new_update_interval;
    }
  }
  resp.refresh_interval_ms = settings.s.update_interval_ms;
  return ns.has_refresh_interval_ms;
}

//==============================================================================

void ProtocolProcessor::writeOutputVoltage(
    ru_sktbelpa_AmpermeterX4_MeasureResult &resp) {
  resp.Voltage = sensormanager.getBusVoltage_V();
}

void ProtocolProcessor::writeCurrentValues(
    const ru_sktbelpa_AmpermeterX4_MeasureRequest &req,
    ru_sktbelpa_AmpermeterX4_MeasureResult &resp) {
  auto &count = resp.currentResults_count;
  count = 0;
  for (auto ch = ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS_CHANEL_0;
       ch <= ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS_CHANEL_3;
       ch = static_cast<ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS>(ch << 1)) {
    if (req.getCurrentChanels & ch) {
      resp.currentResults[count] = ru_sktbelpa_AmpermeterX4_CurrentResult{
          ch, sensormanager.getCurrent_mA(count)};
      ++count;
    }
  }
}

//==============================================================================

void ProtocolProcessor::setupNewOutputVoltage(float newVoltage,
                                              uint32_t &status) {

  if (cController.isCalibrating()) {
    andFlag(
        status,
        ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_CALIBRATION_IN_PROGRESS);
    return;
  }

  switch (cController.voltageController.setVoltage(newVoltage)) {
  case VoltageController::OK:
    andFlag(status,
            ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_OK);
    settings.s.output_voltage = newVoltage;
    shaduler.shadule(SHADULABLE([]() { settings.save(); }));
    break;
  case VoltageController::OUT_OF_LIMITS:
  case VoltageController::EXPR_OVERFLOW:
    andFlag(
        status,
        ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_VOLTAGE_OUT_OF_LIMITS);
    break;
  default:
    andFlag(
        status,
        ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_VOLTAGE_SETUP_ERROR_UNKNOWN);
    break;
  }
}

void ProtocolProcessor::startOutputVoltageCalibration(
    const ru_sktbelpa_AmpermeterX4_OutputVoltageCtrl &params,
    uint32_t &status) {
  const ru_sktbelpa_AmpermeterX4_OutputVoltageCtrl defaullt_init = ru_sktbelpa_AmpermeterX4_OutputVoltageCtrl_init_default;
  if (!cController.isCalibrating()) {
    auto pointsCount =
        params.has_calibration_points_count
            ? params.calibration_points_count
            : defaullt_init.calibration_points_count;
    auto delay =
        params.has_calibration_point_time_ms
            ? params.calibration_point_time_ms
            : defaullt_init.calibration_point_time_ms;
    if (pointsCount > 50 || pointsCount < 2 || delay < 5 || delay > 1000) {
      andFlag(
          status,
          ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_CALIBRATION_SETUP_INCORRECT);
      return;
    }
    cController.startCalibration(pointsCount, delay);
  }
}

void ProtocolProcessor::writeCalibrationStatus(
    ru_sktbelpa_AmpermeterX4_OutputVoltageState &dest) {
  if (cController.isCalibrating())
    andFlag(
        dest.state,
        ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_CALIBRATION_IN_PROGRESS);

  dest.calibration_progress = cController.CalibrationProgress();

  dest.outputVoltageSetup = cController.voltageController.Voltage();
  dest.min_voltage = cController.voltageController.voltage_min();
  dest.max_voltage = cController.voltageController.voltage_max();

  cController.voltageController.getCalibrationFunctrion()->writeCoeffsTo(
      &dest.calibrationCoeffs,
      sizeof(ru_sktbelpa_AmpermeterX4_OutputVoltageState::calibrationCoeffs));
}

//==============================================================================

bool ProtocolProcessor::processRequest(const TReq &request, TResp &responce) {
  responce.id = request.id;
  responce.deviceID = ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID;
  responce.protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION;
  responce.Global_status =
      ((request.deviceID == ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER) ||
       (request.deviceID == ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID)) &&
              (request.protocolVersion <=
               ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION)
          ? ru_sktbelpa_AmpermeterX4_Response_STATUS_OK
          : ru_sktbelpa_AmpermeterX4_Response_STATUS_PROTOCOL_ERROR;

  if (responce.has_getSettings = request.has_setSettings) {
    responce.getSettings.status =
        ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_OK;

    writeMacTo(&responce.getSettings.MAC);

    if (checkIfNetworkNotFullyDisabled(request.setSettings,
                                       responce.getSettings)) {
      auto changed =
          update_STA_Settings(request.setSettings, responce.getSettings) |
          update_AP_Settings(request.setSettings, responce.getSettings) |
          update_measure_interval(request.setSettings, responce.getSettings);

      if (changed) {
        shaduler.shadule(SHADULABLE([]() { settings.save(); }));
        if ((responce.getSettings.status &
             ~ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_REBOOT_REQUIRED) !=
            ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_OK) {
          responce.Global_status =
              ru_sktbelpa_AmpermeterX4_Response_STATUS_ERRORS_IN_SUBCOMMANDS;
          pp_diag_msg("Error while processing setSettings");
        }
      }
    } else {
      responce.Global_status =
          ru_sktbelpa_AmpermeterX4_Response_STATUS_ERRORS_IN_SUBCOMMANDS;
      pp_diag_msg("Attempt to disable network!");
    }
  }

  if (responce.has_measureResult = request.has_measureRequest) {
    if (responce.measureResult.has_Voltage =
            request.measureRequest.has_getVoltage)
      writeOutputVoltage(responce.measureResult);

    if (request.measureRequest.has_getCurrentChanels)
      writeCurrentValues(request.measureRequest, responce.measureResult);
    else
      responce.measureResult.currentResults_count = 0;
  }

  if (responce.has_outputVoltageState = request.has_outputVoltageCtrl) {
    responce.outputVoltageState.state =
        ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_OK;

    if (request.outputVoltageCtrl.has_newOutputVoltage) {
      setupNewOutputVoltage(request.outputVoltageCtrl.newOutputVoltage,
                            responce.outputVoltageState.state);
    }

    if (request.outputVoltageCtrl.has_startCalibration &&
        request.outputVoltageCtrl.startCalibration) {
      startOutputVoltageCalibration(request.outputVoltageCtrl,
                                    responce.outputVoltageState.state);
    }

    writeCalibrationStatus(responce.outputVoltageState);

    if ((responce.outputVoltageState.state &
         ~ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_CALIBRATION_IN_PROGRESS) !=
        ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_OK) {
      responce.Global_status =
          ru_sktbelpa_AmpermeterX4_Response_STATUS_ERRORS_IN_SUBCOMMANDS;
      pp_diag_msg("Error while processing outputVoltageCtrl");
    }
  }

  if (request.has_rebootRequest) {
    pp_diag_msg("reboot request");
    const auto defaults = request.rebootRequest.resetDefaults;
    shaduler.shadule(SHADULABLE([defaults]() {
      pp_diag_msg("reboot");
      if (defaults) {
        settings.resetDefaults();
        settings.save();
      }

      System.restart();
    }));
    return false;
  }

  if (responce.has_wifiScanResult = request.has_wifiScanRequest) {
    if (request.wifiScanRequest.startScan) {
      wifiScanManager.StartScan();
    }
    wifiScanManager.fillData(responce.wifiScanResult);
  }

  return true;
}

std::vector<uint8_t> ProtocolProcessor::createProtocolErrorMessage() const {
  return std::vector<uint8_t>(error_msg, error_msg + sizeof(error_msg));
}

int32_t ProtocolProcessor::CalibrationController::CalibrationProgress() const {
  return calibrating ? calibrating_percent : 100;
}

void ProtocolProcessor::CalibrationController::startCalibration(
    const uint32_t pointsCount, const uint32_t stableDelay) {
  pp_diag_msg("Starting Voltage controller calibration..\npointsCount: "
              "%d\ndelay: %d ms",
              pointsCount, stableDelay);

  monitor.switchMode(Monitor::MODE::CALIBRATING)
      .setCalibrationPercentProgressbar(0);

  auto r = voltageController.startCalibration(
      ITimer::makeTimer(),
      std::make_shared<CalibrationFinishedCB>(std::bind(
          &ProtocolProcessor::CalibrationController::calibrationFinished, this,
          std::placeholders::_1)),
      std::make_shared<CalibrationProgressUpdateCB>(std::bind(
          &ProtocolProcessor::CalibrationController::calibrationProgress, this,
          std::placeholders::_1, std::placeholders::_2)),
      pointsCount, stableDelay);
  if (r == VoltageController::OK)
    calibrating = true;
  else
    pp_diag_msg("Calibration failed: %d", r);
}

void ProtocolProcessor::CalibrationController::calibrationFinished(
    VoltageController &vc) {
  calibrating = false;

  extern void onCalibrationFinished(VoltageController & controller);
  onCalibrationFinished(vc);

  voltageController.setVoltage(settings.s.output_voltage);
}

void ProtocolProcessor::CalibrationController::calibrationProgress(
    VoltageController &vc, int percent) {
  calibrating = true;
  calibrating_percent = percent;
  monitor.setCalibrationPercentProgressbar(percent);
  pp_diag_msg("Calibration in progress: %d %%", percent);
}
