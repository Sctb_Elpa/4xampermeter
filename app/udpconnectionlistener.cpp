#include "udpconnectionlistener.h"
#include <Network/UdpConnection.h>

UDPConnectionListener::UDPConnectionListener()
    : connection(new UdpConnection(
          UdpConnectionDataDelegate(&UDPConnectionListener::onRessive, this))) {
}

UDPConnectionListener::~UDPConnectionListener() { delete connection; }

bool UDPConnectionListener::listen(int port) { return connection->listen(port); }

void UDPConnectionListener::onRessive(UdpConnection &connection, char *data,
                                      int size, IpAddress remoteIP,
                                      uint16_t remotePort) {
  std::vector<uint8_t> response;
  auto result = runProcessor((uint8_t *)data, size, response);
  if (result)
    connection.sendTo(remoteIP, remotePort, (const char *)response.data(),
                      response.size());
}
