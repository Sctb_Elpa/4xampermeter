#include <cstring>

#include "crc32.h"
#include "spiffsprotobuffile.h"

#include "Settings.h"

#if defined(__TEST__)
#define s_diag_msg(...)
#else
#include <SmingCore.h>
#define s_diag_msg(...) debugf(__VA_ARGS__)
#endif

Settings settings;

static const ru_sktbelpa_AmpermeterX4_StoredSettings default_settings =
    ru_sktbelpa_AmpermeterX4_StoredSettings_init_default;

static constexpr const char *printBool(bool b) { return b ? "true" : "false"; }

Settings::Settings() { resetDefaults(); }

void Settings::resetAndSave(const String &filename) {
  resetDefaults();
  saveTo(filename);
}

bool Settings::checkIsChanged() {
  SPIFFSProtobufFile file(filename);
  return file.crc32() != crc32(&s, sizeof(s));
}

void Settings::load(const String &filename) {
  SPIFFSProtobufFile file(filename);
  ru_sktbelpa_AmpermeterX4_StoredSettings settings_load;

  this->filename = filename;
  if (file.Decode(ru_sktbelpa_AmpermeterX4_StoredSettings_fields,
                  &settings_load)) {
    s_diag_msg("Settings successfuly loaded");
    std::memcpy(&s, &settings_load, sizeof(settings_load));
  } else {
    s_diag_msg("Error decoding settings file, rewriting file");
    file.Encode(ru_sktbelpa_AmpermeterX4_StoredSettings_fields, &s);
  }

  printSettings();
}

void Settings::save() {
  if (!filename.length())
    s_diag_msg("No valid file specified");
  else
    saveTo(filename);
}

void Settings::saveTo(const String &filename) {
  SPIFFSProtobufFile file(filename);

  if (checkIsChanged()) {
    if (file.Encode(ru_sktbelpa_AmpermeterX4_StoredSettings_fields, &s)) {
      s_diag_msg("Saved settings to %s", filename.c_str());
    } else {
      s_diag_msg("Failed to open file %s to save settings", filename.c_str());
    }
  } else {
    s_diag_msg("Settings not changed, skip writing", filename.c_str());
  }
}

void Settings::resetDefaults() {
  std::memcpy(&s, &default_settings, sizeof(default_settings));
  s_diag_msg("Reset to default settings");
}

void Settings::printSettings() {
  Serial.printf("Settings : %s\n", filename.c_str());
  Serial.printf("|- isStationEnabled = %s\n", printBool(s.isStationEnabled));
  Serial.printf("|- STA_ESSID = %s\n", s.STA_ESSID);
  Serial.printf("|- STA_password = %s\n", s.STA_password);
  Serial.printf("|- isAccesPointEnabled = %s\n",
                printBool(s.isAccesPointEnabled));
  Serial.printf("|- AP_ESSID = %s\n", s.AP_ESSID);
  Serial.printf("|- AP_password = %s\n", s.AP_password);
  Serial.printf("|- AP_IP = %s\n",
                IpAddress(s.AP_IP.IP_bytes).toString().c_str());
  Serial.printf("|- update_interval_ms = %d\n", s.update_interval_ms);
  Serial.printf("\\- output_voltage = %f\n", s.output_voltage);
}
