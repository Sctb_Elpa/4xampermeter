#include <algorithm>
#include <cmath>
#include <functional>

#include "Adafruit_INA219.h"
#include "myassert.h"

#include "sensormanager.h"

static float getVoltage_W_check(Adafruit_INA219 &sensor) {
  auto U = sensor.getBusVoltage_V();
  return U == (0xffff >> 3) * 4 ? std::numeric_limits<float>::quiet_NaN() : U;
}

static float getCurrent_W_check(Adafruit_INA219 &sensor) {
  auto I = sensor.getCurrent_mA(); // raw_value / (?variable?)
  return abs(I) < 0.25 ? std::numeric_limits<float>::quiet_NaN() : I;
}

SensorManager::SensorManager(std::initializer_list<uint8_t> sensors_addrs,
                             std::shared_ptr<ITimer> timer)
    : sensors(sensors_addrs.begin(), sensors_addrs.end()),
      curentValues(sensors_addrs.size()), timer(timer) {}

SensorManager::~SensorManager() {}

SensorManager &SensorManager::begin() {
  std::for_each(sensors.begin(), sensors.end(),
                [](Adafruit_INA219 &sensor) { sensor.begin(); });
  setCalibration_32V_2A();
  if (timer) {
    setUpdateInterval();
  }
  return *this;
}

SensorManager &SensorManager::setCalibration_32V_2A() {
  std::for_each(sensors.begin(), sensors.end(), [](Adafruit_INA219 &sensor) {
    sensor.setCalibration_32V_2A();
  });
  return *this;
}

SensorManager &SensorManager::setCalibration_32V_1A() {
  std::for_each(sensors.begin(), sensors.end(), [](Adafruit_INA219 &sensor) {
    sensor.setCalibration_32V_1A();
  });
  return *this;
}

SensorManager &SensorManager::setCalibration_16V_400mA() {
  std::for_each(sensors.begin(), sensors.end(), [](Adafruit_INA219 &sensor) {
    sensor.setCalibration_16V_400mA();
  });
  return *this;
}

float SensorManager::getBusVoltage_V() {
  return (timer && timer->isRunning()) ? busVolage : getBusVoltage_V_sync();
}

float SensorManager::getShuntVoltage_mV(uint8_t chanel) {
  return (chanel < curentValues.size())
             ? sensors[chanel].getShuntVoltage_mV()
             : std::numeric_limits<float>::quiet_NaN();
}

float SensorManager::getCurrent_mA(uint8_t chanel) {
  if (chanel < curentValues.size()) {
    auto current = (timer && timer->isRunning())
                       ? curentValues[chanel]
                       : getCurrent_W_check(sensors[chanel]);
    return invertCurrent ? -current : current;
  } else
    return std::numeric_limits<float>::quiet_NaN();
}

SensorManager &SensorManager::setUpdateInterval(uint32_t ms) {
  if (timer) {
    if (timer->isRunning())
      timer->stop();

    timer->initializeMs(ms, std::bind(&SensorManager::readSensors, this))
        .start();
  }
  return *this;
}

float SensorManager::getBusVoltage_V_sync() {
  for (auto sensor = sensors.begin(); sensor != sensors.end(); ++sensor) {
    auto v = getVoltage_W_check(*sensor);
    if (!std::isnan(v))
      return v;
  }
  return std::numeric_limits<float>::quiet_NaN();
}

void SensorManager::readSensors() {
  auto sensor = sensors.begin();
  auto value = curentValues.begin();
  busVolage = std::numeric_limits<float>::quiet_NaN();
  for (; sensor != sensors.end(); ++sensor, ++value) {
    *value = getCurrent_W_check(*sensor);
    if (std::isnan(busVolage)) {
      busVolage = getVoltage_W_check(*sensor);
    }
  }
}
