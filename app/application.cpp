#include <user_config.h>

#include "Adafruit_SSD1306.h"
#undef min
#undef max
#undef swap

#include "make_unique.h"
#include "memory"

#include "MDProtobufMessage.h"
#include "protocol.pb.h"

#include "Settings.h"
#include "webserver.h"

#include "MDProtobufProcessor.h"
#include "Monitor.h"
#include "Setup.h"
#include "linearfunction.h"
#include "protocolprocessor.h"
#include "shaduler.h"
#include "timerexecutor.h"
#include "udpconnectionlistener.h"
#include "wsconnectionlistener.h"

#include "dcdcadj.h"
#include "sensormanager.h"
#include "voltagecontroller.h"

static const String calibration_filename("dcdc.conf");

using Processor_t = MDProtobufProcessor<ru_sktbelpa_AmpermeterX4_Request,
                                        ru_sktbelpa_AmpermeterX4_Response>;

static UDPConnectionListener udplistener;
static WSConnectionListener wslistener;

static MDProtobufDeserialiser<Processor_t::Request_t>
    deserialiser((const char)ru_sktbelpa_AmpermeterX4_INFO_MAGICK,
                 ru_sktbelpa_AmpermeterX4_Request_fields);
static MDProtobufSerialiser<Processor_t::Response_t>
    serialiser((const char)ru_sktbelpa_AmpermeterX4_INFO_MAGICK,
               ru_sktbelpa_AmpermeterX4_Response_fields);

static DCDCAdj dcdc(2, -1);
static SensorManager sensors({0x40, 0x41, 0x44, 0x45},
                             std::shared_ptr<ITimer>(ITimer::makeTimer()));

static Shaduler shaduler(5);

static Adafruit_SSD1306 ssd1306(128, 64, &SPI, DISPLAY_DC, DISPLAY_RST, DISPLAY_CS);
static Monitor display(ssd1306, sensors);

static VoltageController voltageController;
static ProtocolProcessor protocolProcessor(voltageController, shaduler, sensors,
                                           display);
static Processor_t processor(deserialiser, serialiser, protocolProcessor);

static void sta_Connected(IpAddress ip, IpAddress gateway, IpAddress netmask) {
  Serial.println("\n=== WIFI Station connected ===");
  Serial.println(ip.toString());
  Serial.println("==============================\n");

  if (display.getMode() == Monitor::MODE::LOGO) {
    display.switchMode(Monitor::MODE::NORMAL);
  }
}

void onCalibrationFinished(VoltageController &controller) {
  Serial.printf("Calibration complead! (%s)\n"
                "Voltage range: %f - %f v\n",
                controller.getCalibrationFunctrion()->print().c_str(),
                controller.voltage_min(), controller.voltage_max());
  shaduler.shadule(SHADULABLE([&controller]() {
    controller.SaveCalibrationData(calibration_filename);
  }));
  controller.setVoltage(settings.s.output_voltage);

  display.switchMode(Monitor::MODE::NORMAL);
}

static void reportProgress(VoltageController &controller, int progress) {
  display.setCalibrationPercentProgressbar(progress);
  Serial.printf("Calibration in progress: %d %% done..\n", progress);
}

static void init_voltage_controller() {
  sensors.begin()
      .setInvertCurrent() // допущена ошибка в схеме, ток течет в обратном
                          // направлении
      .setCalibration_16V_400mA()
      .setUpdateInterval(settings.s.update_interval_ms);

  dcdc.begin();

  voltageController
      .setSensor(std::make_shared<VoltageSensor>(
          std::bind(&SensorManager::getBusVoltage_V_sync, &sensors)))
      .setActor(std::make_shared<VoltageActor>(
          std::bind(&DCDCAdj::setValue, &dcdc, std::placeholders::_1)))
      .setV2AExpression(std::make_shared<LinearFunction<float>>());

  voltageController.Reset();
  auto res = voltageController.LoadCalibrationData(calibration_filename);
  if (res == VoltageController::IOERROR) {
    voltageController.startCalibration(
        ITimer::makeTimer(),
        std::make_shared<CalibrationFinishedCB>(onCalibrationFinished),
        std::make_shared<CalibrationProgressUpdateCB>(reportProgress));
    display.switchMode(Monitor::MODE::CALIBRATING);
  } else {
    voltageController.setVoltage(settings.s.output_voltage);
  }
}

static void init_display() {
  ssd1306.begin(SSD1306_SWITCHCAPVCC, SSD1306_I2C_ADDRESS, false);
  ssd1306.clearDisplay();
  display.begin(ITimer::makeTimer());
}

static void ready() {
  Serial.println("\n=== READY! ===\n");

  shaduler.start();

  if (WifiAccessPoint.isEnabled()) {
    Serial.println("\n=== WIFI Access point enabled ===");
    Serial.println(WifiAccessPoint.getIP().toString());
    Serial.println("=================================\n");
  } else {
    Serial.println("Awaiting connect to access point");
  }

  auto server = startWebServer();

  udplistener.listen();
  wslistener.registerOnServer(server, "/wsSctb");
}

void user_init() {
  // Mount file system, in order to work with files
  if (!spiffs_mount()) {
    debug_e("failed to mount spiffs");
  } else {
    debug_i("spiffs mounted!");
  }

  Serial.begin(SERIAL_BAUD_RATE); // 115200 by default
  Serial.systemDebugOutput(true);
  Serial.println("\nStart");

  Wire.pins(SDA_PIN, SCL_PIN);

  settings.load();

  init_voltage_controller();
  init_display();

  udplistener.setProcessor(&processor);
  wslistener.setProcessor(&processor);

  ConfigureWIFI_AP();
  ConfigureWIFI_STA();

  WifiEvents.onStationGotIP(sta_Connected);
  if (!isSTAEnabled()) {
    runAfter(
        []() {
          if (display.getMode() == Monitor::MODE::LOGO) {
            display.switchMode(Monitor::MODE::NORMAL);
          }
        },
        5000);
  }

  System.onReady(ready);
}
