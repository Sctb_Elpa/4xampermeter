#include <stdarg.h>

#include "String.h"

void vsnprintf_wrap(char *buf, size_t bufsize, const char *fmt, ...) {
  va_list va;
  va_start(va, fmt);
  __vsnprintf(buf, bufsize, fmt, va);
  va_end(va);
}
