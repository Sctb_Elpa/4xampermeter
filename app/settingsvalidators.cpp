
#include "settingsvalidators.h"

namespace SettingsValidators {

constexpr CharRegion ESSIDValidator::avalable_symbols_regions[];

bool ESSIDValidator::validate(const _String &essid) const {

  if (!StringLengthValidator::validate(essid))
    return false;

  if (essid.length() == 0)
    return false;

  for (auto it = essid.begin(); it != essid.end(); ++it) {
    bool res = false;
    for (int region_n = 0;
         region_n < sizeof(avalable_symbols_regions) / sizeof(CharRegion);
         ++region_n) {
      res |= avalable_symbols_regions[region_n].charInside(*it);
    }
    if (!res)
      return false;
  }

  return true;
}

} // namespace SettingsValidators
