#include <user_config.h>

#include "Settings.h"

#include "Setup.h"

void ConfigureWIFI_AP() {
  WifiAccessPoint.enable(settings.s.isAccesPointEnabled);
  WifiAccessPoint.config(settings.s.AP_ESSID, settings.s.AP_password,
                         AUTH_OPEN);
  WifiAccessPoint.setIP(settings.s.AP_IP.IP_bytes);
}

void ConfigureWIFI_STA() {
  WifiStation.enable(settings.s.isStationEnabled);
  WifiStation.config(settings.s.STA_ESSID, settings.s.STA_password);
}

bool isAPEnabled() { return WifiAccessPoint.isEnabled(); }
bool isSTAEnabled() { return WifiStation.isEnabled(); }
bool isSTAConnected() { return WifiStation.isConnected(); }

IpAddress getAPIP() { return WifiAccessPoint.getIP(); }
IpAddress getSTAIP() { return WifiStation.getIP(); }
