#include "user_config.h"

#include <Network/Http/HttpRequest.h>
#include <Network/Http/HttpResponse.h>
#include <Network/HttpServer.h>

#include "webserver.h"

static HttpServer WebServer;
static String lastModified;

static bool checkLastModified(HttpRequest &request) {
  return lastModified.length() > 0 &&
         request.getHeader("If-Modified-Since").equals(lastModified);
}

static void addLastModified(HttpResponse &response) {
  if (lastModified.length() > 0)
    response.setHeader("Last-Modified", lastModified);
}

void onIndex(HttpRequest &request, HttpResponse &response) {
  if (checkLastModified(request)) {
    response.code = HTTP_STATUS_NOT_MODIFIED;
    return;
  }
  response.setHeader("Content-Type", "text/html");
  addLastModified(response);
  response.setCache(86400, true);
  response.sendFile("index.html");
}

void onFile(HttpRequest &request, HttpResponse &response) {
  if (checkLastModified(request)) {
    response.code = HTTP_STATUS_NOT_MODIFIED;
    return;
  }

  String file = request.uri.Path;
  if (file[0] == '/')
    file = file.substring(1);

  if (file[0] == '.')
    response.code = HTTP_STATUS_FORBIDDEN;
  else {
    addLastModified(response);
    response.setCache(
        86400, true); // It's important to use cache for better performance.
    response.sendFile(file);
  }
}

HttpServer *startWebServer(int port) {
  if (fileExist(".lastModified")) {
    lastModified = fileGetContent(".lastModified");
    lastModified.trim();
  }

  WebServer.listen(port);
  WebServer.paths.set("/", onIndex);
  WebServer.paths.setDefault(onFile);

  return &WebServer;
}
