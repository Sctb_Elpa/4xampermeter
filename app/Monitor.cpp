#include <cmath>

#include "Monitor.h"

#include "ITimer.h"
#include "Setup.h"
#include "sensormanager.h"

#include <Spiffs.h>
#include <IpAddress.h>

#include "Adafruit_SSD1306.h"
#include <functional>

#include "Fonts/FreeSerif12pt7b.h"
#include "Fonts/FreeSerif9pt7b.h"

#if defined(__TEST__) || 0
#define d_diag_msg(...)
#else
#include <SmingCore.h>
#define d_diag_msg(...) debugf(__VA_ARGS__)
#endif

#define FRAME_PERIOD 110

#define STA_AP_Rotation_period (5000 / (FRAME_PERIOD))

#define FONT_V_STR_OFFSET (16)

#define Text_H_offset 8

/// В рисовании все магические числа подобраны. Там где есть логика -
/// значения вычисляемые

static void print_disabled(Print &p) { p.print(" - Disabled -"); }
static void print_IP(Print &p, IpAddress ip) { p.print(ip.toString().c_str()); }

// xbm -> MONO converter
// https://www.online-utility.org/image/convert/to/MONO
static void drawLogo(Adafruit_GFX &display) {
  const uint16_t w = 43, h = 48, loffset = 0;

  FileStream* fileStream = new FileStream;
  if (fileStream && fileStream->open("logo.MONO", File::ReadOnly)) {
    uint8_t xbm_data[w * h];
    fileStream->readBytes(xbm_data, sizeof(xbm_data));
    fileStream->close();

    display.drawXBitmap(loffset, 4, xbm_data, w, h, WHITE);
  } else {
    d_diag_msg("Open logo failed!");
  }
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setFont(&FreeSerif9pt7b);
  display.setCursor(loffset + w - 10, 10);
  display.print("Ampermeter");
  display.setFont(&FreeSerif12pt7b);
  display.setCursor(loffset + w + 20, 32);
  display.print("X4");
  display.setFont(nullptr);
  display.setCursor(loffset + w + 20, 40);
  display.print("Startup...");
}

Monitor::~Monitor() {}

void Monitor::_begin() {
  d.setRotation(2);
  drawLogo(d);
  updateTimer->initializeMs(FRAME_PERIOD, std::bind(&Monitor::Update, this))
      .start();
  displayNetworkState = displayConfig::DISPLAY_AP_IP;
}

void Monitor::drawWifiState(const bool force) {
  auto timer_redraw = frame_counter % STA_AP_Rotation_period == 0;
  if (timer_redraw) {
    displayNetworkState = static_cast<displayConfig>(
        (((uint8_t)displayNetworkState) + 1) % (uint8_t)displayConfig::COUNT);
  }

  if (timer_redraw || force) {
    d.setTextSize(1);
    d.fillRect(0, d.height() - 16, d.width(), d.height(), WHITE);
    d.setTextColor(BLACK, WHITE);

    switch (displayNetworkState) {
    case displayConfig::DISPLAY_AP_IP:
      d.setCursor(5 + 6, d.height() - 16 + 5);
      d.print("AP: ");
      if (isAPEnabled()) {
        print_IP(d, getAPIP());
      } else {
        print_disabled(d);
      }
      break;
    case displayConfig::DISPLAY_STA_IP:
      d.setCursor(5, d.height() - 16 + 5);
      d.print("STA: ");
      if (isSTAEnabled()) {
        if (isSTAConnected()) {
          print_IP(d, getSTAIP());
        } else {
          d.print("Connecting...");
        }
      } else {
        print_disabled(d);
      }
      break;
    default:
      break;  
    }
  } else {
    d.drawFastHLine(0, 48, d.width(), WHITE); // repait first yellow line
  }
}

bool Monitor::drawCurrent() {
  enum Scroll_ages {
    START = 0,
    START_SCROLL = 2000 / FRAME_PERIOD,
    SCROLL_END = START_SCROLL + 3500 / FRAME_PERIOD,
    AWAIT_END = SCROLL_END + 2000 / FRAME_PERIOD,
  };

  static const uint16_t V_offset = 18 + 12;

  auto printer = [this](uint8_t start) {
    for (uint8_t i = 0; i < 2; ++i) {
      d.setCursor(Text_H_offset, V_offset + FONT_V_STR_OFFSET * i);
      print_currnet_NAN_detect(start + i + 1,
                               sensorManager.getCurrent_mA(start + i));
    }
  };

  auto scroll_pos = frame_counter % AWAIT_END;

  d.fillRect(0, 15, d.width(), 33, BLACK);
  if (scroll_pos < START_SCROLL) {
    printer(0);
  } else if (scroll_pos < SCROLL_END) {
    uint16_t minus = scroll_pos - START_SCROLL;
    if (minus < FONT_V_STR_OFFSET) {
      print_1_current(0, minus, V_offset);
    }
    if (minus < FONT_V_STR_OFFSET * 2) {
      print_1_current(1, minus, V_offset);
    }
    if (minus > 0) {
      print_1_current(2, minus, V_offset);
    }
    if (minus > FONT_V_STR_OFFSET) {
      print_1_current(3, minus, V_offset);
    }
    return true;
  } else {
    printer(2);
  }
  return false;
}

void Monitor::drawVoltage() {
  d.fillRect(0, 0, d.width(), 15, WHITE);
  d.setTextColor(BLACK, WHITE);
  d.setCursor(20 + Text_H_offset, 12);
  d.printf("U: %0.2f V", sensorManager.getBusVoltage_V());
}

bool Monitor::drawCalibrationProgressBar() {
  if (calibr_percent < 2) {
    d.fillRect(0, 0, d.width(), 48, BLACK);
    d.setTextSize(1);
    d.setFont(&FreeSerif9pt7b);
    d.setCursor(20, 23);
    d.setTextColor(WHITE, BLACK);
    d.print("Calibrating");
    d.setFont(nullptr);
  }

  if (calibr_percent < 100) {
    auto filled_width = (d.width() - 20) * calibr_percent / 100;
    d.fillRect(10, 15 + FONT_V_STR_OFFSET, filled_width, 10, WHITE);
    d.drawRect(10 + filled_width, 15 + FONT_V_STR_OFFSET,
               d.width() - 20 - filled_width, 10, WHITE);
  } else {
    d.fillRect(10, 15 + FONT_V_STR_OFFSET, d.width() - 20, 10, WHITE);
  }

  return false;
}

bool Monitor::drawVoltageAndCurrent() {
  d.setTextColor(WHITE, BLACK);
  d.setTextSize(1);
  d.setFont(&FreeSerif9pt7b);

  auto res = drawCurrent();
  drawVoltage();

  d.setFont(nullptr);

  return res;
}

void Monitor::print_1_current(const uint8_t chanel, const uint16_t minus,
                              const uint16_t V_offset) {
  d.setCursor(Text_H_offset, V_offset + FONT_V_STR_OFFSET * chanel - minus);
  print_currnet_NAN_detect(chanel + 1, sensorManager.getCurrent_mA(chanel));
}

void Monitor::print_currnet_NAN_detect(const uint8_t chanel, const float I) {
  d.printf("I%d: ", chanel);
  if (std::isnan(I)) {
    d.printf(" - N/C -");
  } else {
    d.printf("%0.1f mA", I);
  }
}

bool Monitor::drawBody() {
  switch (mode) {
  case MODE::NORMAL:
    return drawVoltageAndCurrent();
  case MODE::CALIBRATING:
    return drawCalibrationProgressBar();
  default: return false;
  }
}

void Monitor::begin(std::unique_ptr<ITimer> &&timer) {
  updateTimer = std::move(timer);
  _begin();
}

void Monitor::begin(std::unique_ptr<ITimer> &timer) {
  updateTimer = std::move(timer);
  _begin();
}

Monitor &Monitor::setCalibrationPercentProgressbar(uint8_t percent) {
  calibr_percent = percent;
  return *this;
}

void Monitor::Update() {
  auto force_draw_wifi = drawBody();
  drawWifiState(force_draw_wifi);

  frame_counter++;
  d.display();
}

Monitor &Monitor::switchMode(const Monitor::MODE newMode) {
  mode = newMode;
  return *this;
}
