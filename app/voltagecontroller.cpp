#include <cmath>
#include <numeric>

#include "calibration_summary.pb.h"
#include "halpers.pb.h"
#include "pb_encode.h"

#include "ITimer.h"
#include "myassert.h"
#include "spiffsprotobuffile.h"

#include "calibration_summary.pb.h"

#include "voltagecontroller.h"

#define ACTOR_MINIMUM_LIMIT 0.0f
#define ACTOR_MAXIMUM_LIMIT 1.0f

#ifndef __TEST__
#include <HardwareSerial.h>
#define vc_debugf(...) Serial.printf(__VA_ARGS__)
#else
#define vc_debugf(...) printf(__VA_ARGS__)
#endif

VoltageController::VoltageController()
    : m_expr(), m_voltage_min(0.0f), m_voltage_max(INFINITY) {}

VoltageController &
VoltageController::setActor(const std::shared_ptr<VoltageActor> &actor) {
  m_actor = actor;
  return *this;
}

VoltageController &
VoltageController::setSensor(const std::shared_ptr<VoltageSensor> &sensor) {
  m_sensor = sensor;
  return *this;
}

VoltageController &VoltageController::setV2AExpression(
    const std::shared_ptr<IMathFunc<float>> &expr) {
  m_expr = expr;
  return *this;
}

VoltageController::enStatus
VoltageController::setVoltage(const float outputVoltage) {
  enStatus s = status();
  if (s == OK) {
    if (!isVoltageInLimits(outputVoltage)) {
      return OUT_OF_LIMITS;
    }

    auto newCtorValue = (*m_expr)(outputVoltage);
    if (newCtorValue < ACTOR_MINIMUM_LIMIT ||
        newCtorValue > ACTOR_MAXIMUM_LIMIT) {
      return EXPR_OVERFLOW;
    }

    (*m_actor)(newCtorValue);
    m_cur_output = outputVoltage;
  }
  return s;
}

float VoltageController::actualVoltage() const {
  return status() == OK ? (*m_sensor)() : NAN;
}

VoltageController::enStatus VoltageController::status() const {
  if (m_actor == nullptr || m_expr == nullptr) {
    return CONFIG_INCOMPLEAD;
  }

  return OK;
}

void VoltageController::setVoltage_min(float voltage_min) {
  myassert(voltage_min >= 0.0f);
  myassert(voltage_min < m_voltage_max);

  if (voltage_min > m_cur_output)
    setVoltage(voltage_min);
  m_voltage_min = voltage_min;
}

void VoltageController::setVoltage_max(float voltage_max) {
  myassert(voltage_max > m_voltage_min);

  if (m_cur_output > voltage_max)
    setVoltage(voltage_max);
  m_voltage_max = voltage_max;
}

VoltageController::enStatus VoltageController::startCalibration(
    const std::shared_ptr<ITimer> &executor,
    const std::shared_ptr<CalibrationFinishedCB> &calibrationFinishedCB,
    const std::shared_ptr<CalibrationProgressUpdateCB> &calibrationInfoCB,
    const uint8_t points_Count, const uint32_t stable_delay) {
  if (status() != OK)
    return status();

  if (!m_sensor)
    return CONFIG_INCOMPLEAD;

  if (std::isnan((*m_sensor)()))
    return SENSOR_ERROR;

  this->calibrationFinishedCB = calibrationFinishedCB;
  this->calibrationInfoCB = calibrationInfoCB;

  calibrationData.assign(points_Count, Point());
  calibrationexecutor = executor;
  calibrationStage = CALIBRATION_PREPARE;

  calibrationexecutor = executor;

  calibrationexecutor
      ->initializeMs(
          stable_delay,
          std::bind(&VoltageController::calibrationTimerTick2way, this))
      .start();

  return OK;
}

void VoltageController::Reset() {
  if ((status() == OK) && m_actor) {
    m_cur_output = 0.0f;
    (*m_actor)(ACTOR_MINIMUM_LIMIT);
  }
}

VoltageController::enStatus
VoltageController::LoadCalibrationData(const _String &filename) {
  ru_sktbelpa_AmpermeterX4_CalibrationConfig config;
  SPIFFSProtobufFile f(filename);
  if (!f.Decode(ru_sktbelpa_AmpermeterX4_CalibrationConfig_fields, &config)) {
    return VoltageController::IOERROR;
  }
  m_expr->readCoeffsFrom(&config.coeffs, sizeof(config.coeffs));
  m_voltage_min = config.voltage_min;
  m_voltage_max = config.voltage_max;
  return VoltageController::OK;
}

VoltageController::enStatus
VoltageController::SaveCalibrationData(const _String &filename) const {
  ru_sktbelpa_AmpermeterX4_CalibrationConfig config{
      .coeffs = {}, .voltage_min = m_voltage_min, .voltage_max = m_voltage_max};
  m_expr->writeCoeffsTo(&config.coeffs, sizeof(config.coeffs));
  SPIFFSProtobufFile f(filename);
  return f.Encode(ru_sktbelpa_AmpermeterX4_CalibrationConfig_fields, &config)
             ? VoltageController::OK
             : VoltageController::IOERROR;
}

static void filterOverload(
    std::vector<VoltageController::Point>::const_iterator &filtred_begin,
    std::vector<VoltageController::Point>::const_iterator &filtred_end) {

  using iterator_t =
      std::vector<typename VoltageController::Point>::const_iterator;

  class rev_point_itherator : public iterator_t {
  public:
    explicit rev_point_itherator(const iterator_t &__i) _GLIBCXX_NOEXCEPT
        : iterator_t(__i) {}

    iterator_t::pointer operator->() const _GLIBCXX_NOEXCEPT {
      reversed_point.Y = iterator_t::operator->()->X;
      reversed_point.X = iterator_t::operator->()->Y;
      return &reversed_point;
    }

  private:
    mutable VoltageController::Point reversed_point;
  };

  struct comparer {
    constexpr comparer(float M)
        : M(M), threashhold(std::abs(M * 1e-3f)), sign_m(M > 0) {}

    bool operator()(const float v) const {
      auto v_m_M = v - M;
      bool res;
      if (std::abs(v_m_M) < threashhold)
        res = true;
      else
        res = ((sign_m && (v_m_M > 0.0f)) || (!sign_m && (v_m_M < 0.0f)));
#if 0
      vc_debugf("%d\n", (int)res);
#endif
      return res;
    }

  private:
    const float M, threashhold;
    const bool sign_m;
  };

  smsp::SmoothSpline<float> spline;
  spline.Update(rev_point_itherator(filtred_begin),
                rev_point_itherator(filtred_end));
  std::vector<float> dy(std::distance(filtred_begin, filtred_end));
  const auto src_begin = filtred_begin;

  auto dy_it = dy.begin();
  auto M_dy = std::accumulate(
      filtred_begin, filtred_end, 0.0f,
      [&dy_it, &spline](float summ, const VoltageController::Point &p) {
        auto dy = spline.dY(p.Y);
#if 0
        vc_debugf("P: %f;%f;%f\n", p.X, p.Y, dy);
#endif
        *dy_it = dy;
        dy_it++;
        return summ + dy;
      });
  M_dy /= dy.size();

  const comparer __comparer(M_dy);
  const auto _start_offset =
      std::find_if(dy.begin(), dy.end(), __comparer) - dy.begin();
  const auto _end_offset =
      std::find_if(dy.rbegin(), dy.rend(), __comparer) - dy.rbegin();

  if (_start_offset < dy.size() - _end_offset) {
    filtred_begin += _start_offset;
    filtred_end -= _end_offset;
  }
}

static void writeCoeffs(
    const std::shared_ptr<IMathFunc<float>> &expr,
    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs &forward_pass_summary) {
  expr->writeCoeffsTo(&forward_pass_summary,
                      sizeof(ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs));
}

void VoltageController::doCalibrationStep() {
  struct __actor_calculator {
    constexpr __actor_calculator(const size_t points_count,
                                 const bool isforward = true)
        : isforward(isforward), points_count(points_count) {}

    float operator()(uint32_t step) const {
      if (isforward)
        return (float)step / (points_count - 1);
      else
        return (float)(points_count - 1 - step) / (points_count - 1);
    }

  private:
    const size_t points_count;
    const bool isforward;
  };

  auto points_count = calibrationData.size();
  if (calibration_step < points_count) {
    __actor_calculator f_v(points_count,
                           calibrationStage == CALIBRATION_FORWARD);

    auto &point = calibrationData[calibration_step];
    point.X = (*m_sensor)();
    point.Y = f_v(calibration_step);

#if 0
    vc_debugf("Point %f;%f\n", point.X, point.Y);
#endif

    if (calibrationInfoCB) {
      (*calibrationInfoCB)(*this,
                           (calibrationStage == CALIBRATION_FORWARD)
                               ? (int)(f_v(calibration_step) * 50.0f)
                               : 100 - (int)(f_v(calibration_step) * 50.0f));
    }

    ++calibration_step;
    if (calibration_step < points_count) {
      auto newPwmVal = f_v(calibration_step);
      (*m_actor)(newPwmVal);
    }
  } else {
    calibrationStage = (enCalibrationStage)(calibrationStage + 1);
  }
}

void VoltageController::doCalibrationCalc(bool isBackward) {
  const char *direction[] = {"forward", "backward"};

  auto dir = direction[(int)isBackward];
  auto f_begin = calibrationData.cbegin();
  auto f_end = calibrationData.cend();

  filterOverload(f_begin, f_end);
  m_expr->getCalibrator()->Calibrate(f_begin, f_end);
  if (m_expr->isIncreasing(*f_begin) ^ isBackward) {
    m_voltage_min = f_begin->X;
  } else {
    m_voltage_max = f_begin->X;
  }

#if 1
  vc_debugf("> Done %s calibration: %s [filter : %d -> %d / %d]\n", dir,
            m_expr->print().c_str(), f_begin - calibrationData.cbegin(),
            calibrationData.cend() - f_end, calibrationData.size());
#endif

  writeCalibrationSummary(f_begin, f_end, _String(dir) + ".clb");
}

void VoltageController::writeGeneralSummary() {
  ru_sktbelpa_AmpermeterX4_GeneralCalibrationSummary generalSummry{
      .avarageCoefficients = {},
      .minimum = m_voltage_min,
      .maximum = m_voltage_max};
  writeCoeffs(m_expr, generalSummry.avarageCoefficients);

  const SPIFFSProtobufFile f("summary.clbs");
  f.Encode(ru_sktbelpa_AmpermeterX4_GeneralCalibrationSummary_fields,
           &generalSummry);
}

void VoltageController::calibrationTimerTick2way() {
  switch (calibrationStage) {
  case CALIBRATION_PREPARE:
    (*m_actor)(0.0f);
    calibration_step = 0;
    calibrationStage = CALIBRATION_FORWARD;
    break;
  case CALIBRATION_FORWARD:
  case CALIBRATION_BACKWARD:
    doCalibrationStep();
    break;
  case CALIBRATION_CALC:
    calibrationexecutor->pause();
    doCalibrationCalc(false);

    if (calibrationInfoCB) {
      (*calibrationInfoCB)(*this, 50);
    }

    (*m_actor)(1.0f);
    calibration_step = 0;
    calibrationStage = CALIBRATION_BACKWARD;
    calibrationexecutor->resume();
    break;
  case CALIBRATION_FINISH:
    calibrationexecutor->stop();
    if (calibrationInfoCB) {
      (*calibrationInfoCB)(*this, 100);
      calibrationInfoCB.reset();
    }

    {
      auto forward = m_expr->clone();
      doCalibrationCalc(true);
      calibrationData.clear();
      m_expr->avarege(*forward);
    }
    writeGeneralSummary();

    if (this->calibrationFinishedCB) {
      (*calibrationFinishedCB)(*this);
      calibrationFinishedCB.reset();
    }
    break;
  } /* end switch */
}

void VoltageController::writeCalibrationSummary(
    const VPoints_cIT &filtred_begin, const VPoints_cIT &filtred_end,
    const _String &filename) {

  struct points_encoder {
    constexpr points_encoder(const std::vector<Point> &points,
                             const VPoints_cIT &filtred_begin,
                             const VPoints_cIT &filtred_end)
        : points(points), filtred_begin(filtred_begin),
          filtred_end(filtred_end) {}

    static bool encode_fun(pb_ostream_t *stream, const pb_field_t *field,
                           void *const *arg) {
      auto _this = *((points_encoder **)arg);

      for (auto it = _this->points.cbegin(); it != _this->points.cend(); ++it) {
        ru_sktbelpa_AmpermeterX4_CalibrationPoint p{
            .x = it->X,
            .y = it->Y,
            .isSignificantPoint =
                (it >= _this->filtred_begin) && (it <= _this->filtred_end)};
        if (!pb_encode_tag_for_field(stream, field))
          return false;
        if (!pb_encode_submessage(
                stream, ru_sktbelpa_AmpermeterX4_CalibrationPoint_fields, &p))
          return false;
      }
      return true;
    }

  private:
    const std::vector<Point> &points;
    const VPoints_cIT &filtred_begin;
    const VPoints_cIT &filtred_end;
  };

  points_encoder encoder(calibrationData, filtred_begin, filtred_end);
  ru_sktbelpa_AmpermeterX4_calibration_pass_summary pass_summary{
      .result = {},
      .points = {.funcs = {.encode = encoder.encode_fun}, .arg = &encoder}};
  writeCoeffs(m_expr, pass_summary.result);

  const SPIFFSProtobufFile f(filename);
  f.Encode(ru_sktbelpa_AmpermeterX4_calibration_pass_summary_fields,
           &pass_summary);
}
