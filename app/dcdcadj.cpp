#include "user_config.h"

#include <Clock.h>
#include <Digital.h>
#include <HardwarePWM.h>

#include "dcdcadj.h"


/// Sming 5.1
/// default max_duty = 1000 -> pwmFreq = 1Khz
/// max_duty = 2000 -> pwmFreq = 500Hz

/// PWM base period = 1us (1Mhz) or 200ns (5Mhz) ?
/// max duty = 1000000 / pwmFreq

#define BASE_FREQ 1000000

DCDCAdj::DCDCAdj(uint8_t ctrl_pin, uint8_t oe, uint32_t pwmFreq)
    : oePin(oe), max_duty(BASE_FREQ / pwmFreq), pwm_pin(ctrl_pin),
      HW_pwm(new HardwarePWM(&this->pwm_pin, 1)), is_inverted(false) {
  assert(ctrl_pin != 16); // can't use pin 16 for PWM
  HW_pwm->setPeriod(max_duty);
}

DCDCAdj::~DCDCAdj() { delete HW_pwm; }

void DCDCAdj::begin() {
  pinMode(oePin, OUTPUT);
  enable(false); // disable dc-dc
  setValue(0.0f);
  delayMilliseconds(10);
  enable(true); // enable dc-dc
}

bool DCDCAdj::isEnabled() const { return !!digitalRead(oePin); }

void DCDCAdj::enable(bool enable) { digitalWrite(oePin, (uint8_t)enable); }

void DCDCAdj::setValue(float value) {
  assert(value >= 0.0f);
  assert(value <= 1.0f);

  if (isInverted())
    value = 1.0 - value;

  // Я не знаю откуда берется это магическое 25, но с ним работает!
  auto duty = (uint32_t)(max_duty * value * 25.0);
  HW_pwm->setDuty(pwm_pin, duty);
}

void DCDCAdj::setInversion(bool invert) {
  is_inverted = invert;
  if (isEnabled())
    HW_pwm->setDuty(pwm_pin, max_duty - HW_pwm->getDuty(pwm_pin));
}
