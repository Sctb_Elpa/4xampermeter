COMPONENT_SOC := esp8266
HWCONFIG = 4xampermeter

MODULES += app test/onhw
ARDUINO_LIBRARIES += Adafruit_SSD1306
EXTRA_INCDIR += include easyunit test/onhw

COMPONENT_SEARCH_DIRS += ./Components

COMPONENT_DEPENDS += my_nanopb smoothspline INA219

# uncomment to build web sata
#include ./www/www-halper.mk

SPIFF_FILES = www/fs

RUN_TESTS ?= 0
