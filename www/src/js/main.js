/*jslint indent: 4 */
/*global require global*/

var $ = require('jquery');
global.jQuery = $;
global.bs = require('bootstrap');
var Slider = require('bootstrap-slider');

var model = require('./mvc/model');
var view = require('./mvc/view');
var view_AP = require('./mvc/view-AP');
var controller = require('./mvc/controller');
var controller_AP = require('./mvc/controller-AP');


$().ready(function () {
    'use struct';

    var startCalibration_btn = $('#StartCalibration');
    var controls = {
        history_char_canvas: $('#mc_canvas'),
        vcalibration_button: startCalibration_btn,
        voltage_slider: new Slider('#voltageSlider', {
            formatter: function (value) {
                return value + ' В';
            }
        }),
        minVoltageBtn: $('#min_voltage'),
        maxVoltageBtn: $('#max_voltage'),
        voltageChart: $('#voltage_monitor')
    };

    var m = new model();
    var v = new view(m, controls);
    var v_ap = new view_AP(m, $('#AP_form'));
    var c = new controller(m, v);
    var c_ap = new controller_AP(m, v_ap);

    $('#modal-chart')
        .on('shown.bs.modal', c.onShowHistoryChart)
        .on('hidden.bs.modal', c.onHideNistoryChart);
    $('#modal-voltage-setup')
        .on('shown.bs.modal', c.onShowVoltageControl)
        .on('hidden.bs.modal', c.onHideVoltageControl);
    $('#modal-AP-setup').on('shown.bs.modal', c_ap.onShowAPSettings);

    $('.chartjs-render-monitor.live-current-monitor').each(v.registerChartOn);

    ['minVoltageBtn', 'maxVoltageBtn'].forEach(function (key) {
        controls[key].on('click', c.setOutputVoltage)
    });

    $('#voltageSlider').on('slideStop', c.setOutputVoltage);

    startCalibration_btn.on('click', c.startCalibration);



    m.on('calibration-finished', c.calibrationFinished);
    m.on('data-ready', v.update_live_charts);
    m.startUpdating(200);
});
