/*jslint indent: 4 */
/*global $*/

function VoltageController() {
    var self = this;
    this.callbackmap = {};
    this.monitor = null;
    this.currentVoltage = 0.0;
    this._voltageSetup = 0;

    this.VoltageLimits = function () {
        return [3.5, 11.7];
    }
    this.StartCalibration = function () {
        setTimeout(function () {
            self._trigger('calibrationFinished', 'ok')
        }, 5000);
    }
    this.voltage = function (v) {
        if (v == undefined)
            return self.VoltageLimits()[0] + 1.1;
        else
            self._voltageSetup = v;
    }
    this.on = function (event, callback) {
        if (!self.callbackmap[event])
            self.callbackmap[event] = [];
        self.callbackmap[event].push(callback);
        return self;
    }
    this._trigger = function (evt, val) {
        val = (val || val === 0) ? val : undefined;

        var callbackFnArray = self.callbackmap[evt];
        if (callbackFnArray && callbackFnArray.length) {
            for (var i = 0; i < callbackFnArray.length; i++) {
                var callbackFn = callbackFnArray[i];
                callbackFn(val);
            }
        }
    }
    this.monitoring = function (enabled) {
        if (enabled == undefined) {
            enabled = true;
        }
        if ((self.monitor != null) == enabled) {
            return;
        }

        if (enabled) {
            self.monitor = setInterval(function () {
                self.currentVoltage = self._readVoltage();
                self._trigger('update', self.currentVoltage);
            }, 1000);
        } else {
            clearInterval(self.monitor);
        }
    }
    this._readVoltage = function () {
        return {
            x: Date.now(),
            y: self._voltageSetup + Math.random()
        };
    }
}
