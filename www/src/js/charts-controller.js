/*jslint indent: 4 */
/*global $, Chart */

// https://stackoverflow.com/a/14696535
function groupBy(arr, property) {
    return arr.reduce(function (acumulator, element) {
        acumulator.push(element[property]);
        return acumulator;
    }, []);
}

function LiveChartController(canvas, options) {
    var self = this;
    this.chart = new Chart(canvas, options);

    this.addData = function (data, max_points) {
        while (self.chart.data.labels.length >= max_points) {
            self.chart.data.labels.shift();
            self.chart.data.datasets[0].data.shift();
        }
        self.chart.data.labels.push(data.x);
        self.chart.data.datasets[0].data.push(data.y);
        self.chart.update(0);
    }
    this.setYLimites = function (limits) {
        var ticks = self.chart.options.scales.yAxes[0].ticks;
        ticks.suggestedMin = limits[0];
        ticks.suggestedMax = limits[1];
        self.chart.update(0);
    }
}

function HistoryChart(selector) {
    var self = this;
    this.context = $(selector)[0];
    this.chart = null;

    this.display = function (config) {
        self.destroy();
        self.chart = new Chart(self.context, config);
    }
    this.destroy = function () {
        if (self.chart) {
            self.chart.destroy();
            self.chart = null;
        }
    }
}

var chartsglobalConfig = {
    type: 'line',
    data: {
        datasets: [{
            backgroundColor: 'rgb(240, 240, 240)',
            borderColor: 'rgb(242, 36, 36)',
            fill: false,
                }],
    },
    options: {
        animation: false,
        responsive: true,
        legend: {
            display: false
        },
        title: {
            display: true,
        },
        tooltips: {
            enabled: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                type: 'time',
                display: true,
                time: {
                    displayFormats: {
                        'second': 'mm:ss',
                        'minute': 'mm:ss',
                        'hour': 'hh:mm:ss',
                        'day': 'DD hh:mm',
                        'week': 'DD hh:mm',
                        'month': 'MM.DD',
                        'quarter': 'MM.DD',
                        'year': 'YYYY.MM.DD',
                    }
                }
            }],
            yAxes: [{
                display: true,
            }]
        },
        elements: {
            line: {
                tension: 0, // disables bezier curves
            }
        }
    }
};

function ChartController() {
    var self = this;
    this.liveCurrentCharts = [];
    this.maxPointsLive = 50;
    this.historyChart = new HistoryChart($('#mc_canvas')[0]);
    this.id2chartMap = [];

    // configure current charts
    $(".live-current-monitor").each(function (number, element) {
        self.id2chartMap[number] = element;
        var local_options = $.extend(true, {}, chartsglobalConfig);
        local_options.data.labels = [];
        local_options.data.datasets[0].data = [];
        local_options.options.title.text = 'Сила тока в канале ' + (number + 1);
        self.liveCurrentCharts[number] = new LiveChartController(element, local_options)
    });

    this.addData = function (chart_id, point) {
        self.liveCurrentCharts[chart_id].addData(point, self.maxPointsLive);
    }

    this.display_history_chart = function (I_points, V_points) {
        var I_data = I_points.slice();
        var local_options = $.extend(true, {}, chartsglobalConfig);

        local_options.options.scales.yAxes = [
            $.extend(true, {
                id: 'I',
                type: 'linear',
                position: 'left'
            }, local_options.options.scales.yAxes[0]),
            $.extend(true, {
                    id: 'V',
                    type: 'linear',
                    position: 'right',
                    gridLines: {
                        display: false
                    }
                },
                local_options.options.scales.yAxes[0])
        ];

        local_options.data.labels = groupBy(I_data, "x");
        local_options.data.datasets = [{
                data: I_data,
                yAxisID: 'I',
                backgroundColor: 'rgb(240, 240, 240)',
                borderColor: 'rgb(242, 36, 36)',
                fill: false
            },
            {
                data: V_points.slice(),
                yAxisID: 'V',
                backgroundColor: 'rgba(87, 213, 224, 0.35)',
                borderColor: 'rgba(119, 190, 216, 0.8)',
                fill: true
            }];
        local_options.options.tooltips = {
            enabled: true,
            mode: 'x',
            intersect: false,
            callbacks: {
                title: function (tooltipItem, data) {
                    var d = tooltipItem[0].xLabel;
                    return "Время: " + d.format('dd.mm hh:MM:ss');
                },
                label: function (tooltipItem, data) {
                    var units = ['мА', 'В'];
                    return tooltipItem.yLabel + " " + units[tooltipItem.datasetIndex];
                }
            }
        };
        local_options.options.downsample = {
            enabled: true,
            threshold: 250 // max number of points to display per dataset
        }

        self.historyChart.display(local_options);
    }

    this.destroy_history_chart = function () {
        self.historyChart.destroy();
        $('.modal-body').remove('.chartjs-size-monitor');
    }

    this.getChartId = function (invoker) {
        return self.id2chartMap.findIndex(function (element) {
            return element === invoker;
        });
    }
}
