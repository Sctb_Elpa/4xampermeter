/*jslint indent: 4 */
/*global $ VoltageController, chartsglobalConfig LiveChartController*/


function VoltageGUI() {
    var self = this;
    this.min_voltage_label = $('#min_voltage');
    this.min_voltage_label.on('click', function () {
        var min_voltage = self.voltage_controller.VoltageLimits()[0];
        self.slider.slider('setValue', min_voltage);
        self._setVoltage(min_voltage);
        return false;
    });
    this.max_voltage_label = $('#max_voltage');
    this.max_voltage_label.on('click', function () {
        var max_voltage = self.voltage_controller.VoltageLimits()[1];
        self.slider.slider('setValue', max_voltage);
        self._setVoltage(max_voltage);
        return false;
    });
    this.calibrate_btn = $('#StartCalibration');
    this.slider = $('#ex1').slider({
        formatter: function (value) {
            return value + ' В';
        }
    }).on('slideStop', function (event) {
        self._setVoltage(event.value);
    });
    var local_options = $.extend(true, {}, chartsglobalConfig);
    local_options.data.labels = [];
    local_options.data.datasets[0].data = [];
    local_options.options.title.text = 'Напряжение';
    this.voltage_plot = new LiveChartController($('#voltage_monitor'), local_options)

    this.voltage_controller = new VoltageController();

    this._setVoltage = function (v) {
        self.voltage_controller.voltage(v);
    }

    this.voltage_controller
        .on('update', function (point) {
            self.voltage_plot.addData(point, 50);
        })
        .on('calibrationFinished', function (result) {
            self.LoadState();
            self.slider.slider('enable');
            self._resetCalibrateBtn();
        });

    $('#StartCalibration').on('click', function (event) {
        self.slider.slider('disable');
        self.calibrate_btn.attr('disabled', true);
        self.calibrate_btn.html('<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i> Идет калибровка')
        self.voltage_controller.StartCalibration();
    });

    this._resetCalibrateBtn = function () {
        self.calibrate_btn.removeAttr('disabled');
        self.calibrate_btn.html('<i class="fa fa-cog" aria-hidden="true"></i> Начать калибровку')
    }

    this.LoadState = function () {
        var limits = self.voltage_controller.VoltageLimits()
        self.min_voltage_label.html(limits[0]);
        self.max_voltage_label.html(limits[1]);
        self.voltage_plot.setYLimites(limits);
        self.slider.slider('setValue', self.voltage_controller.voltage());
        self._resetCalibrateBtn();
    }
    this.LoadState();
    this.voltage_controller.monitoring(true);
}

function AP_controller() {
    var self = this;
    this.enable_checkbox = $('#enable_AP');
    this.form = $('#AP_form');
    this.form.formValidation({
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            AP_SSID: {
                validators: {
                    stringLength: {
                        min: 3,
                    },
                    notEmpty: {
                        message: 'Введите название для сети.'
                    }
                }
            },
            AP_IP: {
                validators: {
                    ip: {
                        ipv4: true,
                        ipv6: false,
                        message: "Введите IP адрес точки доступа."
                    },
                    notEmpty: {
                        message: 'IP-адрес не может быть пустым.'
                    }
                }
            }
        }
    }).on('success.form.fv', function () {
        return false;
    });

    this.enable_checkbox.change(function () {
        var is_ap_enabled = $(this).is(":checked");
        if (!is_ap_enabled)
            self._load();
        var ro = 'readonly';
        $('input[name^=AP_]').each(function (n, element) {
            is_ap_enabled ? $(element).removeAttr(ro) : $(element).attr(ro, ro);
        });
        self._enableSubmitBtn(true);
    });

    this._enableSubmitBtn = function (en) {
        self.form.formValidation('disableSubmitButtons', !en);
    }

    $('#AP_reset_settings').on('click', function () {
        self._reset();
        return false;
    });

    this._load = function () {
        this.enable_checkbox.attr('checked', true);
        $('input[name=AP_SSID]').attr('value', 'test1');
        $('input[name=AP_IP]').attr('value', '192.168.4.1');
        self._enableSubmitBtn(false);
    }
    this._reset = function () {
        this.enable_checkbox.attr('checked', true);
        $('input[name=AP_SSID]').attr('value', 'AmpermeterX4');
        $('input[name=AP_IP]').attr('value', '192.168.125.1');
        self._enableSubmitBtn(true);
    }
    this._load();
}

function wifi_network(ssid, is_encrepted, signal_level) {
    var self = this;
    this.ssid = ssid;
    this.is_encrepted = is_encrepted;
    this.signal_level = signal_level;
    this.node = null;
    this.callbackmap = {};

    this.selector = function () {
        return '[network="' + self.ssid + '"]';
    }

    this.appendToList = function (parent) {
        var signal = self.signal_level > 33 ? (self.signal_level > 66 ? 'good' : 'better') : 'bad';
        var lock = self.is_encrepted ? 'lock' : 'unlock';
        var pass_hiden = !self.is_encrepted ? 'hide' : '';
        parent.append('<a href="#" class="list-group-item" network="${self.ssid}">\
                                    <i class="fa fa-${lock}" aria-hidden="true"></i><span class="list-item-text">${self.ssid}</span><span \class="badge"><i class="fa fa-signal ${signal}-signal" aria-hidden="true"></i></span>\
                                    <div class="row vert-offset-top-1 pass-container hide">\
                                        <div class="col-xs-8 ${pass_hiden}">\
                                            <input name="sta_pass" placeholder="Пароль" class="form-control" type="text">\
                                        </div>\
                                        <div class="col-xs-2">\
                                            <button type="submit" class="btn btn-success  id="AP_apply">Подключить <i class="fa fa-arrow-right" \aria-hidden="true"></i></button>\
                                        </div>\
                                    </div>\
                            </a>');
        $(self.selector()).on('click', function () {
            $(self.selector() + '>div.pass-container').removeClass('hide');
            self._trigger('rised', self);
            return false;
        });
    };
    this.destroy = function () {
        $(self.selector()).remove();
    };
    this.collapse = function () {
        $(self.selector() + '>div.pass-container').addClass('hide');
    };
    this.on = function (event, callback) {
        if (!self.callbackmap[event])
            self.callbackmap[event] = [];
        self.callbackmap[event].push(callback);
        return self;
    };
    this._trigger = function (evt, val) {
        val = (val || val === 0) ? val : undefined;

        var callbackFnArray = self.callbackmap[evt];
        if (callbackFnArray && callbackFnArray.length) {
            for (var i = 0; i < callbackFnArray.length; i++) {
                var callbackFn = callbackFnArray[i];
                callbackFn(val);
            }
        }
    }
}

function STA_controller() {
    var self = this;
    this.enable_checkbox = $('#enable_STA');
    this.list = $('#net_list');
    this.rescan_btn = $('#STA_rescan');
    this.netlist = null;

    this.rescan_btn.on('click', function () {
        self.clerNetList();
        self.startNetworkScan();
        return false;
    });

    this.startNetworkScan = function () {
        self.rescan_btn.attr('disabled', 'disabled');
        setTimeout(function () {
            self.rescan_btn.removeAttr('disabled');
            self.loadNetlist();
        }, 500);
    };

    this.clerNetList = function () {
        self.netlist.forEach(function (item) {
            item.destroy()
        });
        self.netlist = null;
    };

    this.getNetlist = function () {
        var netlist = [];
        for (var i = 0; i < 5; i++) {
            netlist.push(new wifi_network('Network ' + i, Math.random() > 0.5, Math.random() * 100));
        }
        var colapse_all_but = function (net) {
            netlist.forEach(function (n) {
                if (n != net)
                    n.collapse();
            });
        };

        netlist.forEach(function (net) {
            net.on('rised', colapse_all_but)
        });
        return netlist;
    }

    this.loadNetlist = function () {
        self.netlist = self.getNetlist();
        self.netlist.forEach(function (item) {
            item.appendToList(self.list)
        });
    };

    this.load = function () {
        this.enable_checkbox.attr('checked', true);
        self.loadNetlist();
    };
    this.load();
}

$().ready(function () {
    var voltagegui = new VoltageGUI();
    var ap_control = new AP_controller();
    var sta_controller = new STA_controller();
});
