/*jslint indent: 4 */
/*global $, window, Chart, overview_charts_prepare destroy_history_chart overview_chart_add_data ChartController*/

var history = new Array();
var v_history = new Array();
var controller = null;
var i = 0;

function generateRandom_data() {
    for (var chart = 0; chart < 4; chart++) {
        var v = Math.random() + chart;
        var x = new Date(Date.now() + (i * 1000));
        var point = {
            x: x,
            y: v
        }
        controller.addData(chart, point);
        history[chart].push(point);
    }
    v_history.push({
        x: x,
        y: Math.random()
    });
    i++;
}

// on load complead
$().ready(function () {
    for (var i = 0; i < 4; i++)
        history[i] = new Array();
    controller = new ChartController();
    setInterval(generateRandom_data, 200);

    // https://codepen.io/codeschubser/pen/PwajYp
    $('#modal-chart').on('shown.bs.modal', function (event) {
        var chart_id = controller.getChartId(event.relatedTarget);
        $('#exampleModalLabel').html('История изменения силы тока в канале ' + (chart_id + 1))
        controller.display_history_chart(history[chart_id], v_history);
    }).on('hidden.bs.modal', controller.destroy_history_chart);
});
