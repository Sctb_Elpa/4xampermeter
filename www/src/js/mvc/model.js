/*jslint indent: 4 */
/*global module clearInterval setInterval setTimeout*/

function HistoryController() {
    'use struct';
    var self = this;
    this.history = {};

    this.pushData = function (data) {
        for (var key in data) {
            if (self.history[key] == undefined)
                self.history[key] = [data[key]];
            else
                self.history[key].push(data[key]);
        }
    }

    this.getChanelData = function (chanel) {
        return self.history[chanel];
    }
}

function Model() {
    var self = this;
    this.update_timer = null;
    this.callbackmap = {};

    this.history = new HistoryController();

    this.on = function (event, callback) {
        if (!self.callbackmap[event])
            self.callbackmap[event] = [];
        self.callbackmap[event].push(callback);
        return self;
    }

    this._trigger = function (evt, val) {
        val = (val || val === 0) ? val : undefined;

        var callbackFnArray = self.callbackmap[evt];
        if (callbackFnArray && callbackFnArray.length) {
            for (var i = 0; i < callbackFnArray.length; i++) {
                var callbackFn = callbackFnArray[i];
                callbackFn(val);
            }
        }
    }

    this.startUpdating = function (interval) {
        if (self.update_timer)
            self.stopUpdating();
        self.update_timer = setInterval(self.generateRandom_data, interval);
    }

    this.stopUpdating = function () {
        clearInterval(self.update_timer);
        self.update_timer = null;
    }

    this.generateRandom_data = function () {
        var res = {};
        var date = Date.now();
        ['0', '1', '2', '3', 'v'].forEach(function (item) {
            res[item] = {
                y: Math.random(),
                x: date
            };
        })

        self.history.pushData(res);
        self._trigger('data-ready');
    }

    this.getData = function (chanel, maxpoints) {
        var d = self.history.getChanelData(chanel);
        if (maxpoints == undefined)
            maxpoints = d.length - 1;
        var start = d.length - 1 - maxpoints;
        return d.slice(start < 0 ? 0 : start, start + maxpoints);
    }

    this.startCalibration = function () {
        setTimeout(function () {
            self._trigger('calibration-finished');
        }, 3000);
    }

    this.getVltageControl = function () {
        return {
            minVoltage: 3.2,
            maxVoltage: 11.9,
            Voltage: 7.2
        }
    }
    this.setOutputVoltage = function (value) {
        console.log('Set output voltage: ' + value + ' v');
    }

    this.getAPConfig = function () {
        return {
            enabled: true,
            SSID: 'test',
            IP: '192.168.49.1'
        }
    }
    this.submitAPSettings = function (settings) {
        console.log('submitAPSettings');
    }

    this.resetAPSettings = function () {
        self._trigger('AP-settings-changed');
    }
}

module.exports = Model;
