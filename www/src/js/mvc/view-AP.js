/*jslint indent: 4 */
/*global module */

function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    unindexed_array.map(function (obj) {
        indexed_array[obj['name']] = obj['value'];
    });

    return indexed_array;
}

function view_AP(model, form) {
    var self = this;
    this.model = model;
    this.form = form;
    this.enableCheckbox = this.form.find('input.enabler');
    this.applyButton = this.form.find('button.btn-success');
    this.resetButton = this.form.find('button.btn-danger');
    this.form.bootstrapValidator({
        submitButtons: 'button[type="submit"]',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            AP_SSID: {
                validators: {
                    stringLength: {
                        min: 3,
                    },
                    notEmpty: {
                        message: 'Введите название для сети.'
                    }
                }
            },
            AP_IP: {
                validators: {
                    ip: {
                        ipv4: true,
                        ipv6: false,
                        message: "Введите IP адрес точки доступа."
                    },
                    notEmpty: {
                        message: 'IP-адрес не может быть пустым.'
                    }
                }
            }
        }
    });

    this.enableSubmitBtn = function (en) {
        self.form.bootstrapValidator('disableSubmitButtons', !en);
    }

    this.enableForm = function (enable) {
        enable = typeof enable == 'string' ? enable == 'checked' : enable;
        var ro = 'readonly';
        var fields = self.form.find('input[name^=AP_]');
        if (enable)
            fields.removeAttr(ro);
        else
            fields.attr(ro, ro);
    }

    this.loadConfig = function () {
        var AP_config = self.model.getAPConfig();
        self.enableCheckbox.prop('checked', AP_config.enabled);
        //self.enableForm(AP_config.enabled)
        self.form.find('input[name=AP_SSID]').val(AP_config.SSID);
        self.form.find('input[name=AP_IP]').val(AP_config.IP);
        self.enableSubmitBtn(false);
    }

    this.isFormEnabled = function () {
        return self.enableCheckbox.is(":checked");
    }

    this.getConfig = function () {
        return {};
    }

    this.getFormData = function () {
        return getFormData(self.form);
    }
    /*
        this.enable_checkbox.change(function () {
            var is_ap_enabled = $(this).is(":checked");
            if (!is_ap_enabled)
                self._load();
            var ro = 'readonly';
            $('input[name^=AP_]').each(function (n, element) {
                is_ap_enabled ? $(element).removeAttr(ro) : $(element).attr(ro, ro);
            });
            self._enableSubmitBtn(true);
        });

        
        $('#AP_reset_settings').on('click', function () {
            self._reset();
            return false;
        });

        this._load = function () {
            this.enable_checkbox.attr('checked', true);
            $('input[name=AP_SSID]').attr('value', 'test1');
            $('input[name=AP_IP]').attr('value', '192.168.4.1');
            self._enableSubmitBtn(false);
        }
        this._reset = function () {
            this.enable_checkbox.attr('checked', true);
            $('input[name=AP_SSID]').attr('value', 'AmpermeterX4');
            $('input[name=AP_IP]').attr('value', '192.168.125.1');
            self._enableSubmitBtn(true);
        }
        this._load();
        */
}

module.exports = view_AP;
