/*jslint indent: 4 */
/*global module*/

function Controller_AP(model, ap_view) {
    var self = this;
    this.model = model;
    this.view = ap_view;

    this.submit = function (e) {
        e.preventDefault();
        self.model.submitAPSettings(self.view.getFormData());
    }
    this.reset = function () {
        self.model.resetAPSettings();
    }
    this.onShowAPSettings = function () {
        self.view.loadConfig();
    }

    this.view.form.on('success.form.bv', this.submit);
    this.view.resetButton.on('click', this.reset);
    this.model.on('AP-settings-changed', this.onShowAPSettings);
    this.view.enableCheckbox.change(function () {
        self.view.enableForm(self.view.isFormEnabled());
        self.view.enableSubmitBtn(true);
    });
}

module.exports = Controller_AP;
