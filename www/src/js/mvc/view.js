/*jslint indent: 4 */
/*global module require*/

var $ = require('jquery');
var Chart = require('chart.js');
var dateformat = require('dateformat');

function groupBy(arr, property) {
    return arr.reduce(function (acumulator, element) {
        acumulator.push(element[property]);
        return acumulator;
    }, []);
}

function LiveChartController(canvas, options) {
    var self = this;
    this.chart = new Chart(canvas, options);

    this.addData = function (data, max_points) {
        while (self.chart.data.labels.length >= max_points) {
            self.chart.data.labels.shift();
            self.chart.data.datasets[0].data.shift();
        }
        self.chart.data.labels.push(data.x);
        self.chart.data.datasets[0].data.push(data.y);
        self.chart.update(0);
    }
    this.setYLimites = function (limits) {
        var ticks = self.chart.options.scales.yAxes[0].ticks;
        ticks.suggestedMin = limits[0];
        ticks.suggestedMax = limits[1];
        self.chart.update(0);
    }
    this.drawData = function (data) {
        self.chart.data.labels = groupBy(data, 'x');
        self.chart.data.datasets[0].data = data;
        self.chart.update(0);
    }
    this.destroy = function () {
        self.chart.destroy();
        self.chart = null;
        self = null;
    }
}

function View(model, controls) {
    var self = this;

    this.histor_max_points = 250;
    this.overview_max_points = 50;

    this.chartsglobalConfig = {
        type: 'line',
        data: {
            datasets: [{
                backgroundColor: 'rgb(240, 240, 240)',
                borderColor: 'rgb(242, 36, 36)',
                fill: false,
                }],
        },
        options: {
            animation: false,
            responsive: true,
            legend: {
                display: false
            },
            title: {
                display: true,
            },
            tooltips: {
                enabled: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    display: true,
                    ticks: {
                        autoSkip: true,
                        maxRotation: 0,
                        minRotation: 0
                    },
                    time: {
                        displayFormats: {
                            'second': 'mm:ss',
                            'minute': 'mm:ss',
                            'hour': 'hh:mm:ss',
                            'day': 'DD hh:mm',
                            'week': 'DD hh:mm',
                            'month': 'MM.DD',
                            'quarter': 'MM.DD',
                            'year': 'YYYY.MM.DD',
                        }
                    }
            }],
                yAxes: [{
                    display: true,
            }]
            },
            elements: {
                line: {
                    tension: 0, // disables bezier curves
                }
            }
        }
    };
    this.id2chartMap = {};
    this.history_chart = null;

    this.model = model;
    this.controls = controls;

    this.setVoltageCalibrationStatus = function (status) {
        if (status == 'idle') {
            self.controls.voltage_slider.enable();
            self.controls.vcalibration_button
                .removeAttr('disabled')
                .html('<i class="fa fa-cog" aria-hidden="true"></i> Начать калибровку')
        } else if (status == 'calibrationg') {
            self.controls.voltage_slider.disable();
            self.controls.vcalibration_button
                .attr('disabled', true)
                .html('<i class="fa fa-spinner fa-pulse" aria-hidden="true"></i> Идет калибровка');
        }
    }

    this._getBasicChartConfig = function () {
        var local_options = $.extend(true, {}, self.chartsglobalConfig);
        local_options.data.labels = [];
        local_options.data.datasets[0].data = [];
        return local_options;
    }

    this.registerChartOn = function (id, canvas) {
        var local_options = self._getBasicChartConfig();
        local_options.options.title.text = 'Сила тока в канале ' + (id + 1);

        self.id2chartMap[id] = {
            chart: new LiveChartController(canvas, local_options),
            canvas: canvas
        };
    }

    this._get_history_chart_config = function (I_data, V_data) {
        var local_options = $.extend(true, {}, self.chartsglobalConfig);

        local_options.options.scales.yAxes = [
            $.extend(true, {
                id: 'I',
                type: 'linear',
                position: 'left'
            }, local_options.options.scales.yAxes[0]),
            $.extend(true, {
                id: 'V',
                type: 'linear',
                position: 'right',
                gridLines: {
                    display: false
                }
            }, local_options.options.scales.yAxes[0])
        ];

        local_options.data.labels = groupBy(I_data, "x");
        local_options.data.datasets = [{
                data: I_data,
                yAxisID: 'I',
                backgroundColor: 'rgb(240, 240, 240)',
                borderColor: 'rgb(242, 36, 36)',
                fill: false
            },
            {
                data: V_data,
                yAxisID: 'V',
                backgroundColor: 'rgba(87, 213, 224, 0.35)',
                borderColor: 'rgba(119, 190, 216, 0.8)',
                fill: true
            }];
        local_options.options.tooltips = {
            position: 'nearest',
            enabled: true,
            mode: 'x',
            intersect: false,
            callbacks: {
                title: function (tooltipItem) {
                    return "Время: " + dateformat(tooltipItem[0].xLabel, 'dd.mm hh:MM:ss');
                },
                label: function (tooltipItem) {
                    var units = ['мА', 'В'];
                    return tooltipItem.yLabel.toFixed(2) + " " + units[tooltipItem.datasetIndex];
                }
            }
        };
        local_options.options.downsample = {
            enabled: true,
            threshold: self.histor_max_points // max number of points to display per dataset
        }
        return local_options;
    }

    this.renderHistory = function (chanel) {
        self.destroyHistory();
        self.history_chart = new Chart(self.controls.history_char_canvas, self._get_history_chart_config(
            self.model.getData(chanel), self.model.getData('v')
        ));
    }

    this.destroyHistory = function () {
        if (self.history_chart) {
            self.history_chart.destroy();
            self.history_chart = null;
        }
    }

    this.update_live_charts = function () {
        Object.keys(self.id2chartMap).forEach(function (key) {
            self.id2chartMap[key].chart.drawData(self.model.getData(key, self.overview_max_points));
        })
    }

    this.mapSender2ChNum = function (canvas) {
        return Object.keys(self.id2chartMap).find(function (key) {
            return self.id2chartMap[key].canvas == canvas;
        });
    }

    this.updateVoltageControl = function () {
        var voltageSetup = self.model.getVltageControl();
        self.controls.minVoltageBtn.html(voltageSetup['minVoltage']);
        self.controls.maxVoltageBtn.html(voltageSetup['maxVoltage']);
        self.controls.voltage_slider.setValue(voltageSetup['Voltage']);
    }

    this.mapValueFromTarget = function (obj) {
        switch (obj.type) {
            case 'slideStop':
                return obj.value;
            case 'click':
                var res;
                if (Object.keys(self.controls).filter(function (key) {
                        return ['minVoltageBtn', 'maxVoltageBtn'].includes(key);
                    }).some(function (key) {
                        var c = self.controls[key];
                        if (self.controls[key].is(obj.currentTarget)) {
                            res = c.html();
                            return true;
                        }
                    }))
                    return res;
        }
    }
    this.setOutputVoltageValue = function (value) {
        self.controls.voltage_slider.setValue(value);
    }

    this.initVoltageChart = function () {
        var local_options = self._getBasicChartConfig();
        local_options.options.title.text = 'Монитор напряжения';
        self.destroyVoltageChart();
        self.id2chartMap['v'] = {
            chart: new LiveChartController(self.controls.voltageChart, local_options),
            canvas: self.controls.voltageChart
        };
    }

    this.destroyVoltageChart = function () {
        var c = self.id2chartMap['v'];
        delete self.id2chartMap['v'];
        if (c && c.chart) {
            c.chart.destroy();
            c.chart = null;
        }
    }

    this.setVoltageCalibrationStatus('idle');
}

module.exports = View;
