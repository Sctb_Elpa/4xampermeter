/*jslint indent: 4 */
/*global module */


function Controller(model, view) {
    var self = this;
    this.model = model;
    this.view = view;

    this.onShowHistoryChart = function (event) {
        var chartid = self.view.mapSender2ChNum(event.relatedTarget);
        self.view.renderHistory(chartid);
    }

    this.onHideNistoryChart = function () {
        self.view.destroyHistory();
    }

    this.onShowVoltageControl = function () {
        self.view.updateVoltageControl();
        self.view.initVoltageChart();
    }

    this.onHideVoltageControl = function () {
        self.view.destroyVoltageChart();
    }

    this.startCalibration = function () {
        self.view.setVoltageCalibrationStatus('calibrationg');
        self.model.startCalibration();
    }

    this.calibrationFinished = function () {
        self.view.setVoltageCalibrationStatus('idle');
    }

    this.setOutputVoltage = function (arg) {
        var value = (typeof arg == 'object') ? self.view.mapValueFromTarget(arg) : arg;
        self.view.setOutputVoltageValue(value);
        self.model.setOutputVoltage(value);
    }
}

module.exports = Controller;
