/*jslint indent: 4 */
/*global $ require */

global.$ = require('jquery');
global.jQuery = $;
global.bs = require('bootstrap');
var Chart = require('chart.js');

global.chartsglobalConfig = {
    type: 'line',
    data: {
        datasets: [{
            backgroundColor: 'rgb(240, 240, 240)',
            borderColor: 'rgb(242, 36, 36)',
            fill: false,
                }],
    },
    options: {
        animation: false,
        responsive: true,
        legend: {
            display: false
        },
        title: {
            display: true,
        },
        tooltips: {
            enabled: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                type: 'time',
                display: true,
                time: {
                    displayFormats: {
                        'second': 'mm:ss',
                        'minute': 'mm:ss',
                        'hour': 'hh:mm:ss',
                        'day': 'DD hh:mm',
                        'week': 'DD hh:mm',
                        'month': 'MM.DD',
                        'quarter': 'MM.DD',
                        'year': 'YYYY.MM.DD',
                    }
                }
            }],
            yAxes: [{
                display: true,
            }]
        },
        elements: {
            line: {
                tension: 0, // disables bezier curves
            }
        }
    }
};

global.LiveChartController = function (canvas, options) {
    var self = this;
    this.chart = new Chart(canvas, options);

    this.addData = function (data, max_points) {
        while (self.chart.data.labels.length >= max_points) {
            self.chart.data.labels.shift();
            self.chart.data.datasets[0].data.shift();
        }
        self.chart.data.labels.push(data.x);
        self.chart.data.datasets[0].data.push(data.y);
        self.chart.update(0);
    }
    this.setYLimites = function (limits) {
        var ticks = self.chart.options.scales.yAxes[0].ticks;
        ticks.suggestedMin = limits[0];
        ticks.suggestedMax = limits[1];
        self.chart.update(0);
    }
}
