#######################################################################
www_mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
www_dir := $(notdir $(patsubst %/,%,$(dir $(www_mkfile_path))))

SPIFF_FILES = $(www_dir)/fs
LASTMODIFIED_FILE = $(SPIFF_FILES)/.lastModified

NODE_MODULES = $(www_dir)/node_modules

$(SPIFF_FILES):
	$(Q) mkdir -p $@

$(NODE_MODULES):
	$(Q) cd $(www_dir) && npm install

compress_www: $(NODE_MODULES)
	$(Q) cd $(www_dir) && node_modules/.bin/gulp make-release

$(LASTMODIFIED_FILE):
	LANG=C date +'%a, %d %b %Y %H:%M:%S GMT' -u > $@

common: $(SPIFF_FILES) compress_www $(LASTMODIFIED_FILE) #spiff_clean


ifeq "$(RUN_TESTS)" "1"
deploy: proto_gen refrash_tests common flash
else
deploy: proto_gen common flash
endif

#######################################################################
