/*jslint indent: 4 */

'use struct';

// https://habrahabr.ru/post/250569/
// длинное имя у файла glyphicons-halflings-regular.woff

import gulp from 'gulp';
import rigger from 'gulp-rigger';
import gzip from 'gulp-gzip';
import { reload } from "browser-sync";
import htmlmin from 'gulp-htmlmin';
import uglify from 'gulp-uglify';
import sourcemaps from 'gulp-sourcemaps';
import gutil from 'gulp-util';
import concat from 'gulp-concat';
import cssmin from 'gulp-cssmin';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import rimraf from 'gulp-rimraf';
import watch from 'gulp-watch';
import replace from 'gulp-just-replace';
import rename from "gulp-rename";
import Path from 'path';
import log from 'fancy-log';
import changed from 'gulp-changed';

var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        develop: 'dev',
        release: 'fs'
    },
    src: { //Пути откуда брать исходники
        html: 'src/html/*.html',
        js_main: 'src/js/main.js',
        style: 'src/css/*.css',
        resources: {
            'node_modules/bootstrapvalidator/dist/js/bootstrapValidator.min.js': 'bots_val'
        },
        MONO: 'img/*.MONO'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        html: 'src/html/**/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
        js: 'src/js/**/*.js', //В стилях и скриптах нам понадобятся только main файлы
        style: 'src/css/*.css',
        get fonts() {
            return [libs.bootstrap.fonts.orig, libs.fontawesome.fonts];
        }
    },
    get clean() {
        return [this.build.develop, this.build.release.fonts];
    }
};

var libs = {
    bootstrap: {
        css: 'node_modules/bootstrap/dist/css/bootstrap.css',
        fonts: {
            orig: 'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.woff',
            rename: 'g-h-r.woff'
        }
    },
    bootstrap_slider: {
        css: 'node_modules/bootstrap-slider/dist/css/bootstrap-slider.css',
    },
    fontawesome: {
        css: 'node_modules/font-awesome/css/font-awesome.css',
        fonts: 'node_modules/font-awesome/fonts/fontawesome-webfont.woff',
    },
};

var server_cfg = {
    server: {
        baseDir: './' + path.build.develop
    },
    //tunnel: true,
    host: 'localhost',
    port: 9009,
    logPrefix: "Dev Server"
};

var replace_map = [
    {
        search: "src: url('../fonts/fontawesome-webfont.eot?v=4.7.0');",
        replacement: ''
    }, {
        search: /src:.*fonts\/([A-Za-z-]*\.woff)\?.*format\('svg'\);/g,
        replacement: "src: url('$1') format('woff');"
    }, {
        search: "src: url('../fonts/glyphicons-halflings-regular.eot');",
        replacement: ''
    }, {
        search: /src:.*fonts.*\/([A-Za-z-]*\.woff)'.*format\('svg'\);/g,
        replacement: "src: url('" + libs.bootstrap.fonts.rename + "') format('woff');"
    },
];

function getlibfiles(type) {
    var res = [];
    for (var lib in libs) {
        var value = libs[lib][type];
        if (value != undefined)
            res.push(value);
    }

    return res;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('html:build:dev', function () {
    return gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(changed(path.build.develop))
        .pipe(gulp.dest(path.build.develop)) //Выплюнем их в папку build
        .pipe(reload({
            stream: true
        })); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build:dev', function () {
    var b = browserify({
        entries: path.src.js_main,
        debug: true
    });

    return b.bundle()
        .pipe(source('script.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({
            loadMaps: true
        }))
        .pipe(uglify())
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })
        .pipe(changed(path.build.develop))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.develop))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('style:build:dev', function () {
    return gulp.src(getlibfiles('css').concat(path.src.style))
        .pipe(replace(replace_map))
        .pipe(cssmin()) //Сожмем
        .pipe(concat('style.css'))
        .pipe(changed(path.build.develop))
        .pipe(gulp.dest(path.build.develop))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('fonts:dev', function (cb) {
    gulp.src(libs.bootstrap.fonts.orig)
        .pipe(rename(libs.bootstrap.fonts.rename))
        .pipe(gulp.dest(path.build.develop));

    gulp.src(libs.fontawesome.fonts)
        .pipe(changed(path.build.develop))
        .pipe(gulp.dest(path.build.develop));
    cb();
});

gulp.task('resources:dev', function () {
    return gulp.src(Object.keys(path.src.resources))
        .pipe(rename(function (p) {
            var key = path.src.resources.find_file(
                Path.join(p.dirname, p.basename + p.extname));
            if (path.src.resources[key])
                p.basename = path.src.resources[key];
            }))
        .pipe(changed(path.build.develop))
        .pipe(gulp.dest(path.build.develop));
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('html:build:release', function () {
    return gulp.src(path.src.html) //Выберем файлы по нужному пути
        .pipe(rigger()) //Прогоним через rigger
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(path.build.develop)) //Выплюнем их в папку build
        .pipe(gzip({
            threshold: 1024,
            gzipOptions: {
                level: 9
            }
        }))
        .pipe(changed(path.build.release))
        .pipe(gulp.dest(path.build.release));
});

gulp.task('js:build:release', function () {
    var b = browserify({
        entries: path.src.js_main,
        debug: true
    });

    return b.bundle()
        .pipe(source('script.js'))
        .pipe(buffer())
        .pipe(uglify())
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })
        .pipe(gzip({
            threshold: 1024,
            gzipOptions: {
                level: 9
            }
        }))
        .pipe(changed(path.build.release))
        .pipe(gulp.dest(path.build.release));
});

gulp.task('style:build:release', function () {
    return gulp.src(getlibfiles('css').concat(path.src.style))
        .pipe(replace(replace_map))
        .pipe(cssmin()) //Сожмем
        .pipe(concat('style.css')) // сгруппируем
        .pipe(gzip({
            threshold: 1024,
            gzipOptions: {
                level: 9
            }
        }))
        .pipe(changed(path.build.release))
        .pipe(gulp.dest(path.build.release)); //Выплюнем их в папку fs
});

gulp.task('fonts:compress', function (cb) {
    // gzip has no effect there
    gulp.src(libs.bootstrap.fonts.orig)
        .pipe(rename(libs.bootstrap.fonts.rename))
        .pipe(changed(path.build.release))
        .pipe(gulp.dest(path.build.release));

    gulp.src(libs.fontawesome.fonts)
        .pipe(changed(path.build.release))
        .pipe(gulp.dest(path.build.release));
    cb();
});

gulp.task('resources:compress', function () {
    // gzip has no effect there
    return gulp.src(Object.keys(path.src.resources))
        .pipe(rename(function (p, file) {

            function find_file(name) {
                for (var key in path.src.resources) {
                    if (Path.basename(key, '.js') + '.js' == name)
                        return key;
                }
                return undefined;
            }

            var key = find_file(Path.join(p.dirname, p.basename + p.extname));
            if (path.src.resources[key])
                p.basename = path.src.resources[key];
            }))
        .pipe(gzip({
            threshold: 1024,
            gzipOptions: {
                level: 9
            }
        }))
        .pipe(changed(path.build.release))
        .pipe(gulp.dest(path.build.release));
});

gulp.task('resources:MONO', function () {
    return gulp.src(path.src.MONO)
        .pipe(changed(path.build.release))
        .pipe(gulp.dest(path.build.release));
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('make-release', gulp.series('resources:MONO', 'html:build:release', 'js:build:release', 'style:build:release', 'fonts:compress', 'resources:compress'));

gulp.task('make-dev', gulp.series('html:build:dev', 'js:build:dev', 'style:build:dev', 'fonts:dev', 'resources:dev'));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('watch', function () {
    watch([path.watch.html], function (event, cb) {
        gulp.start('html:build:dev');
    });
    watch([path.watch.style], function (event, cb) {
        gulp.start('style:build:dev');
    });
    watch([path.watch.js], function (event, cb) {
        gulp.start('js:build:dev');
    });
    watch(path.watch.fonts, function (event, cb) {
        gulp.start('fonts:dev');
    });
});

gulp.task('webserver', function () {
    browserSync(server_cfg);
});

gulp.task('clean', function () {
    return gulp.src(path.clean, {
            read: false
        })
        .pipe(rimraf({
            force: true
        }));
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

gulp.task('default', gulp.series('make-release'));

gulp.task('dev', gulp.series('make-dev', 'webserver', 'watch'));
