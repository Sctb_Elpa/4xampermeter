# 4х канальный амперметр на INA219

## Dependencies

* [Sming 5.1](https://github.com/SmingHub/Sming/tree/5.1.0) - фреймворк Sming
* [arduino-esp8266-core-git](https://aur.archlinux.org/packages/arduino-esp8266-core-git) - Компилятор и SDK для ESP8266.

## Build

```bash
make SMING_HOME=/opt/Sming/Sming ESP_HOME=/usr/share/arduino/hardware/archlinux-esp8266/esp8266/tools SMING_ARCH=Esp8266 COM_SPEED_SERIAL=115200 flash
```

## Известные проблемы

1. При обновление со старой версии Sming сильно изменилась система сборки и автоматическая генерация WEB-компонентов сломана. Поэтому требуется вручную из собрать раскоментировав сточку `include ./www/www-halper.mk` в `component.mk` и выполнить `make ... deploy`.
2. Тестирование не работает.
