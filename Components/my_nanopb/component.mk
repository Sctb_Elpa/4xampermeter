COMPONENT_LIBNAME = my_nanopb

COMPONENT_PREREQUISITES = proto_gen

#######################################################################
protocol_mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
protocol_dir_abs := $(patsubst %/,%,$(dir $(protocol_mkfile_path)))
protocol_dir := $(notdir $(protocol_dir_abs))
protocol_dir_name = nanopb

# name of system prptobuf compiler executable
PROTOC ?= protoc

NANOPB_PLUGIN = protoc-gen-nanopb
NANOPB_PY_DIR = proto

NANOPB_DIST_DIR = nanopb-dist
# path to protoc plugin for nanopb
NANOPB_PLUGIN_DIR = $(NANOPB_DIST_DIR)/generator

# name of protocol files
PROTO_FILE  =   protocol.proto

PROTO_FILES =   $(PROTO_FILE) \
                halpers.proto \
                calibration_summary.proto

# path to store output files
NANOPB_PROTOCOL_SRC_DIR = gen

NANOPB_PROTO_LIB_NAME = nanopb_proto


#######################################################################

NANOPB_PLUGIN_ABS := $(protocol_dir_abs)/$(NANOPB_PLUGIN_DIR)/$(NANOPB_PLUGIN)
NANOPB_PY_DIR_ABS := $(protocol_dir_abs)/$(NANOPB_PLUGIN_DIR)/$(NANOPB_PY_DIR)
NANOPB_PROTOCOL_SRC_DIR_ABS := $(protocol_dir_abs)/$(NANOPB_PROTOCOL_SRC_DIR)

PROTO_FILE_ABS    := $(addprefix $(protocol_dir)/,$(PROTO_FILE))

PROTO_FILES := $(addprefix $(protocol_dir)/, $(PROTO_FILES))
PROTO_FILES_SRC := $(PROTO_FILES:$(protocol_dir)/%.proto=$(NANOPB_PROTOCOL_SRC_DIR_ABS)/%.pb.c)

#######################################################################

$(NANOPB_PROTOCOL_SRC_DIR_ABS):
	$(Q) mkdir -p $@

prepare_pb_lib:
	$(Q) $(MAKE) -C $(NANOPB_PY_DIR_ABS)

$(PROTO_FILES_SRC): $(NANOPB_PROTOCOL_SRC_DIR_ABS)/%.pb.c: $(protocol_dir_abs)/%.proto
	$(Q) $(PROTOC) --plugin=protoc-gen-nanopb=$(NANOPB_PLUGIN_ABS) -I$(protocol_dir_abs) --nanopb_out=-I$(protocol_dir_abs):$(NANOPB_PROTOCOL_SRC_DIR_ABS) $^


proto_gen: $(NANOPB_PROTOCOL_SRC_DIR_ABS) prepare_pb_lib $(PROTO_FILES_SRC)

#######################################################################

COMPONENT_INCDIRS = $(NANOPB_PROTOCOL_SRC_DIR_ABS) $(protocol_dir_abs)/$(NANOPB_DIST_DIR)

COMPONENT_SRCDIRS = $(NANOPB_PROTOCOL_SRC_DIR_ABS) $(protocol_dir_abs)/$(NANOPB_DIST_DIR)
