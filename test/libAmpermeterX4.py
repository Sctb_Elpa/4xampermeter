# -*- coding: utf-8 -*-

import protocol_pb2
import socket
import select
import threading
import random

from io import BytesIO
from google.protobuf.internal.decoder import _DecodeSignedVarint as DecodeSignedVarint
from google.protobuf.internal.encoder import _EncodeSignedVarint as EncodeSignedVarint


class TimeoutError(RuntimeError):
    pass


class Request:
    def __init__(self, magick):
        self.__dict__['req'] = protocol_pb2.Request()
        self.__dict__['magick'] = magick.to_bytes(1, 'little')

    def serialiseMDToString(self):
        out = BytesIO()
        out.write(self.magick)
        EncodeSignedVarint(out.write, self.req.ByteSize(), False)
        out.write(self.req.SerializeToString())
        return out.getvalue()

    def __setattr__(self, key, value):
        req = self.__dict__['req']
        if hasattr(req, key):
            setattr(req, key, value)
            return value
        raise AttributeError("No such attribute: {}".format(key))

    def __getattr__(self, name):
        req = self.__dict__['req']
        if hasattr(req, name):
            return getattr(req, name)
        raise AttributeError("No such attribute: {}".format(name))



class Response:
    def __init__(self, magick):
        self.resp = protocol_pb2.Response()
        self.magick = magick.to_bytes(1, 'little')

    def parceMD(self, data):
        magick = data[0].to_bytes(1, 'little')
        if magick != self.__dict__['magick']:
            raise ValueError("Invalid magick ({:02X})".format(magick))
        length, offset = DecodeSignedVarint(data, 1)
        if length < 0 or length > 1500 or length > len(data) - offset:
            raise ValueError("length of message seems incorrect")
        self.__dict__['resp'].ParseFromString(data[offset:offset + length])

    def __getattr__(self, name):
        if hasattr(self.resp, name):
            return getattr(self.resp, name)
        raise AttributeError("No such attribute: {}".format(name))


class AmpermeterX4_requestBuilder:
    def __init__(self, magick=protocol_pb2.INFO.Value('MAGICK')):
        self.magick = magick

    def build_request(self):
        """
        Создаёт заготовку запроса

        :return: объект типа protocol_pb2.Request c заполнениыми полями id и version
        """
        req = Request(self.magick)
        req.id = random.randrange(0xffffffff)
        req.protocolVersion = protocol_pb2.INFO.Value('PROTOCOL_VERSION')
        req.deviceID = protocol_pb2.INFO.Value('ID_DISCOVER')
        return req

    def build_ping_request(self):
        """
        Создаёт запрос проверки соединения

        :return: объект типа protocol_pb2.Request
        """
        return self.build_request()

    def build_settings_request(self):
        """
        Создаёт запрос чтения настроек

        :return: объект типа protocol_pb2.Request
        """
        sr = self.build_request()
        sr.setSettings.CopyFrom(protocol_pb2.SetSettings())
        return sr

    def build_measureValues_request(self):
        mr = self.build_request()
        mr.measureRequest.CopyFrom(protocol_pb2.MeasureRequest())
        return mr

    def build_voltage_ctl_request(self):
        cr = self.build_request()
        cr.outputVoltageCtrl.CopyFrom(protocol_pb2.OutputVoltageCtrl())
        return cr

    def build_reboot_request(self):
        rr = self.build_request()
        rr.rebootRequest.CopyFrom(protocol_pb2.RebootRequest())
        return rr

    def build_network_list_request(self):
        nlr = self.build_request()
        nlr.wifiScanRequest.CopyFrom(protocol_pb2.WifiScanRequest())
        return nlr


class AmpermeterX4_io:
    """Класс для простого доступа к РЧ-24 v2 по ротоколу UDP с использованием google protocol buffers"""
    def __init__(self, address, port=9178, magick=protocol_pb2.INFO.Value('MAGICK')):
        """
        Конструктор

        :param address: Адрес к которому будет произведено подключение, например '192.168.0.1', device.example.com
        :param port: Порт UDP к которому будет произведено подключение
        """
        self.address = address
        self.port = port
        self.magick = magick
        self.base_timeout = 2  # сек
        self.isConnected = False
        self.udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp_socket.setblocking(0)

    def __str__(self):
        """
        Выводит краткую информацию о состоянии драйвера

        :return: Строка с краткой информацией ос состоянии устройства и соединения
        """
        return 'РЧ-24 v2 по адрсесу "{}"'.format(self.address)

    def connect(self):
        """
        Инициирует подключение к устройству

        :return: None
        """
        if self.isConnected:
            raise RuntimeError('Already connected')

        self.udp_socket.bind(('', 0))

        self.isConnected = True

    def disconnect(self):
        """
        Инициирует отключение от устройства

        :return: None
        """
        if not self.isConnected:
            return

        self.udp_socket.close()

    def process_request_sync(self, request, timeout_sec=1):
        """
        Синхронный обработчик запроса (блокирет вызвавший поток до получения ответа или до истечения таймаута)

        :param request: объект типа protocol_pb2.Request
        :param timeout_sec: Таймаут ожидания ответа от устройства
        :return:
        """
        if not (type(request) is Request):
            raise TypeError('"request" mast be instance of "protocol_pb2.Request"')

        return self.process_request_common(request, timeout_sec)

    def process_request_common(self, request, timeout_sec):
        self.udp_socket.sendto(request.serialiseMDToString(), (self.address, self.port))

        encode_failed = False

        response = Response(self.magick)
        while timeout_sec > 0:
            ready = select.select([self.udp_socket], [], [], self.base_timeout)
            if ready[0]:
                conn, adr = self.udp_socket.recvfrom(4096)
                try:
                    response.parceMD(conn)
                except Exception as e:
                    print('--->Encoding failed<---')
                    encode_failed = e
                    continue

                if (response.id == request.id) and (adr[0] == self.address)\
                        and response.protocolVersion <= protocol_pb2.INFO.Value('PROTOCOL_VERSION')\
                        and response.deviceID == protocol_pb2.INFO.Value('AMPERMETERX4_ID'):
                    return response  # ok
            timeout_sec -= self.base_timeout

        if timeout_sec <= 0:
            if encode_failed:
                raise encode_failed
            else:
                raise TimeoutError('Timeout')

    def async_listener(self, request, callback, timeout_sec):
        try:
            result = self.process_request_common(request, timeout_sec)
        except TimeoutError:
            callback(None)
            return
        callback(result)

    def process_request_async(self, request, callback=None, timeout_sec=1):
        """
        Асинхронный обработчик запроса (вызывает callback, по заверешнию)

        :param request:  объект типа protocol_pb2.Request
        :param callback: функция типа foo(response), которая будет вызвана после получения ответа или истечения таймаута
        :param timeout_sec: Таймаут ожидания ответа от устройства
        :return:
        """
        if not (type(request) is protocol_pb2.Request):
            raise TypeError('"request" mast be instance of "protocol_pb2.Request"')
        self.udp_socket.sendto(request.SerializeToString(), (self.address, self.port))

        thread = threading.Thread(target=self.async_listener, args=(self, request, callback, timeout_sec))
        thread.start()

