#######################################################################
test_mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
test_dir := $(notdir $(patsubst %/,%,$(dir $(test_mkfile_path))))

PYTEST = py.test

UDP_TEST_FILE = ./test_AmpermeterX4.py
WS_TEST_FILE = ./test_WSAmpermeterX4.py

CXXTESTGEN = cxxtestgen
CXXTEST_DIR = cxxtest
CXXTEST_INC = /usr/include/cxxtest

CXX_BINARY_NAME = $(CXXTEST_DIR_GEN_ABS)/cxxTest

APP_DIR = app

_CXX_SRC = app/settingsvalidators.cpp \
           app/String.cpp \
           app/spiffsprotobuffile.cpp \
           app/shaduler.cpp \
           app/ITimer.cpp \
           app/utils.cpp \
           app/voltagecontroller.cpp

_C_SRC = app/crc32.c

_NANOPB_SRC = nanopb/pb_common.c \
            nanopb/pb_decode.c \
            nanopb/pb_encode.c

ONHW_TEST_EXECUTOR = onhw/tests.cpp

TEST_DEFINES = -D__TEST__


#######################################################################

UDP_TEST_FILE_ABS := $(test_dir)/$(UDP_TEST_FILE)
WS_TEST_FILE_ABS  := $(test_dir)/$(WS_TEST_FILE)
CXXTEST_DIR_ABS   := $(test_dir)/$(CXXTEST_DIR)
CXXTEST_DIR_GEN_ABS := $(test_dir)/$(CXXTEST_DIR)/gen
PROTO_FILES_PY := $(PROTO_FILES:$(protocol_dir)/%.proto=$(test_dir)/%_pb2.py)

CXX_TEST_INCLUDE_DIRS := $(INCDIR) $(MODULE_INCDIR) $(EXTRA_INCDIR) \
                            $(SDK_INCDIR) -I$(CXXTEST_INC)

_OBJS_C := $(_C_SRC:$(APP_DIR)/%.c=$(CXXTEST_DIR_GEN_ABS)/%.o)
_OBJS_CXX := $(_CXX_SRC:$(APP_DIR)/%.cpp=$(CXXTEST_DIR_GEN_ABS)/%.o)
_OBJS_NANOPB_C := $(_NANOPB_SRC:$(protocol_dir)/%.c=$(CXXTEST_DIR_GEN_ABS)/%.o)
_OBJS_SRC_PROTO := $(PROTO_FILES_SRC:$(NANOPB_PROTOCOL_SRC_DIR_ABS)/%.pb.c=$(CXXTEST_DIR_GEN_ABS)/%.o)

TESTS_FILES := $(wildcard $(CXXTEST_DIR_ABS)/test.*.h)
TEST_SRC = $(CXXTEST_DIR_GEN_ABS)/cxxTestc.cpp

#######################################################################

TEST_DEP_CXX = $(CXX_BINARY_NAME:%=%.d)

-include $(TEST_DEP_CXX)

#######################################################################

$(PROTO_FILES_PY): $(test_dir)/%_pb2.py : $(protocol_dir)/%.proto
	$(PROTOC) -I$(protocol_dir) --python_out=$(test_dir) $^

pb_pytest.run: $(PROTO_FILES_PY)
	$(Q) $(PYTEST) $(UDP_TEST_FILE_ABS)

pbws_pytest.run: $(PROTO_FILES_PY)
	$(Q) $(PYTEST) $(WS_TEST_FILE_ABS)

speed_test.run: $(PROTO_FILES_PY)
	$(Q) $(PYTEST) -k test_ping --count=1000 $(UDP_TEST_FILE_ABS)


##########################################################################

$(_OBJS_C): $(CXXTEST_DIR_GEN_ABS)/%.o: $(APP_DIR)/%.c
	$(vecho) "CC $<"
	$(Q) gcc -g $(CXX_TEST_INCLUDE_DIRS) $(TEST_DEFINES) -c $^ -o $@

$(_OBJS_CXX): $(CXXTEST_DIR_GEN_ABS)/%.o: $(APP_DIR)/%.cpp
	$(vecho) "C+ $<"
	$(Q) g++ -g $(CXX_TEST_INCLUDE_DIRS) $(TEST_DEFINES) -c -std=c++11 $^ -o $@

$(_OBJS_NANOPB_C): $(CXXTEST_DIR_GEN_ABS)/%.o: $(protocol_dir)/%.c
	$(vecho) "C+ $<"
	$(Q) g++ -g $(CXX_TEST_INCLUDE_DIRS) $(TEST_DEFINES) -c $^ -o $@

$(_OBJS_SRC_PROTO): $(CXXTEST_DIR_GEN_ABS)/%.o: $(NANOPB_PROTOCOL_SRC_DIR_ABS)/%.pb.c
	$(vecho) "CC $<"
	$(Q) g++ -g $(CXX_TEST_INCLUDE_DIRS) $(TEST_DEFINES) -c $^ -o $@

$(CXXTEST_DIR_GEN_ABS)/libapp.host.a: proto_gen $(_OBJS_NANOPB_C) $(_OBJS_C) $(_OBJS_CXX) $(_OBJS_SRC_PROTO)
	$(vecho) "AR $<"
	$(Q) ar -qsc $@ $(_OBJS_NANOPB_C) $(_OBJS_C) $(_OBJS_CXX) $(_OBJS_SRC_PROTO)

$(CXXTEST_DIR_GEN_ABS):
	$(Q) mkdir -p $@

$(TEST_SRC): $(TESTS_FILES)
	$(vecho) "TG $<"
	$(Q) $(CXXTESTGEN) --error-printer -o $@ $^

$(CXX_BINARY_NAME): $(CXXTEST_DIR_GEN_ABS) $(CXXTEST_DIR_GEN_ABS)/libapp.host.a $(TEST_SRC)
	$(vecho) "LD $<"
	$(Q) g++ -g -pthread -std=c++11 -o $@ $(TEST_DEFINES) $(CXX_TEST_INCLUDE_DIRS) -MMD $(TEST_SRC) $(CXXTEST_DIR_GEN_ABS)/libapp.host.a

cxxTest: $(CXX_BINARY_NAME)
	$(Q) $^

refrash_tests:
	$(Q) touch $(test_dir)/$(ONHW_TEST_EXECUTOR)

##########################################################################

clean_tests:
	$(Q) rm -rf $(CXXTEST_DIR_GEN_ABS)
	$(Q) rm -rf $(PROTO_FILES_PY)

clean: clean_tests

##########################################################################

PASS_DECODER = python $(test_dir)/calibration_pass_parcer.py
DECODE_RESULT_CMD =

calibr_summary: $(PROTO_FILES_PY)
	$(Q) echo Target $$TEST_IP
	$(Q) echo "Downloading forward pass summary..."
	$(Q) curl --silent http://$$TEST_IP/forward.clb | $(PASS_DECODER)
	$(Q) echo
	$(Q) echo "Downloading backward pass summary..."
	$(Q) curl --silent http://$$TEST_IP/backward.clb | $(PASS_DECODER)
        #$(Q) echo
        #$(Q) echo "Downloading summary..."
        #$(Q) curl --silent http://$$TEST_IP/summary.clbs | $(DECODE_RESULT_CMD)
