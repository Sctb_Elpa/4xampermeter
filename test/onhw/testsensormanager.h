#ifndef TEST_SENSORMANAGER_H
#define TEST_SENSORMANAGER_H

#include <SmingCore/SmingCore.h>

#include "itestsuite.h"

#include "sensormanager.h"

TEST_SUITE_NAME(TestSensorManager)
public:
static void testManagerCreate() { SensorManager sm({0x41}); }

static void testNoTimer() {
  SensorManager sm({0x41});
  sm.begin();

  ASSERT("Sensor V more then 0", sm.getBusVoltage_V() > 0.0f);
  ASSERT("Sensor I more then 0", sm.getCurrent_mA() > 0.0f);
  ASSERT("Sensor sV more then 0", sm.getShuntVoltage_mV() > 0.0f);
}

static void testTimer() {
  auto timer = std::shared_ptr<ITimer>(ITimer::makeTimer());
  SensorManager sm({0x41}, timer);
  sm.begin();
  sm.setUpdateInterval(10);

  delay(30);

  ASSERT("Sensor V more then 0", sm.getBusVoltage_V() > 0.0f);
  ASSERT("Sensor I more then 0", sm.getCurrent_mA() > 0.0f);
  ASSERT("Sensor sV more then 0", sm.getShuntVoltage_mV() > 0.0f);
}

void run() override { RUN(testManagerCreate); }
}
;

#endif // TEST_SENSORMANAGER_H
