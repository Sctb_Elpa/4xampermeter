#include "itestsuite.h"
#include <SmingCore/Clock.h>
#include <SmingCore/Platform/AccessPoint.h>
#include <SmingCore/Platform/Station.h>
#include <SmingCore/Wire.h>

// Вот эти инклюды менять местами нельзя - все сломается!

#include "testlinearfunction.h"

#include "testvoltmeter.h"

#include "testsensormanager.h"

#include "testvoltagecontroller.h"

#include "testlinearfuncclibrator.h"

#include "testMDProtocolPerformance.h"

#include "testdisplay.h"

#include "testpwmctrl.h"

#include "testvectoritherator.h"

void Prepere_tests() {
  Serial.begin(SERIAL_BAUD_RATE); // 115200 by default
  Serial.systemDebugOutput(false);

  WifiAccessPoint.enable(false);
  WifiStation.enable(false);

  Wire.pins(SDA_PIN, SCL_PIN);

  Serial.println("\n\n>>>Runnting tests...\n");
  delayMilliseconds(500);
}

void run_tests() {
  Prepere_tests();

  TestLinearFunction::instance()->start();
  TestVectorItherator::instance()->start();
  TestVoltmeter::instance()->start();
  TestSensorManager::instance()->start();
  TestLinearFunctionCalibrator::instance()->start();
  TestPWMCtrl::instance()->start();
  TestVoltageController::instance()->start();
  TestMDProtocolPerformance::instance()->start();
  TestDisplay::instance()->start();

  TEST_REPORT();
}
