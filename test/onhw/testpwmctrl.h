#ifndef TESTPWMCTRL_H
#define TESTPWMCTRL_H

#include "dcdcadj.h"
#include <SmingCore/HardwarePWM.h>

#include "itestsuite.h"

TEST_SUITE_NAME(TestPWMCtrl)
public:
// DCDCAdj actor(2, -1);
static void testPWMBasic() {
  uint8_t pin = 2;
  HardwarePWM HW_pwm(&pin, 1);

  HW_pwm.setPeriod(20000);
  HW_pwm.analogWrite(pin, (uint32)(HW_pwm.getPeriod() / 25.0f * 0.5f));
}

static void testDCDCAdjsetSet2k_20percent() {
  DCDCAdj dcdc(2);
  dcdc.begin();
  dcdc.setValue(0.5f);
}

void run() override {
  RUN(testPWMBasic);
  RUN(testDCDCAdjsetSet2k_20percent);
}
}
;

#endif // TESTPWMCTRL_H
