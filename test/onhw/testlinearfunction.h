#ifndef TESTLINEARFUNCTION_H
#define TESTLINEARFUNCTION_H

#include "itestsuite.h"
#include "linearfunction.h"

TEST_SUITE_NAME(TestLinearFunction)
private:
TestLinearFunction() {}

public:
static void testZero(void) {
  auto f = make_linear_func();
  ASSERT_EQUALS((*f)(0), 0);
}

static void testINF() {
  auto f = make_linear_func();
  ASSERT("Inf\n", std::isinf((*f)(INFINITY)));
  ASSERT("-Inf\n", std::isinf(-(*f)(-INFINITY)));
}

static void testNAN() {
  auto f = make_linear_func();
  ASSERT("NaN\n", std::isnan((*f)(NAN)));
}

static void testSetK() {
  auto f = make_linear_func();
  const float newK = 10.0;
  f->setK(newK);
  ASSERT_EQUALS(f->getK(), newK);
  ASSERT_EQUALS((*f)(1.0f), 1.0f * newK);
}

static void testSetB() {
  auto f = make_linear_func();
  const float newB = 10.0;
  f->setB(newB);
  ASSERT_EQUALS(f->getB(), newB);
  ASSERT_EQUALS((*f)(1.0f), 1.0f + newB);
}

static void testIsIncreasing() {
  std::pair<float, float> p(0, 0);
  auto f = make_linear_func();
  f->setK(1.0f);
  ASSERT_EQUALS(f->isIncreasing(p), true);

  f->setK(-1.0f);
  ASSERT_EQUALS(f->isIncreasing(p), false);

  f->setK(0.0f);
  ASSERT_EQUALS(f->isIncreasing(p), false);
}

void run() override {
  RUN(testZero);
  RUN(testINF);
  RUN(testNAN);
  RUN(testSetK);
  RUN(testSetB);
  RUN(testIsIncreasing);
}

private:
static std::unique_ptr<LinearFunction<float>> make_linear_func() {
  return std::make_unique<LinearFunction<float>>();
}
}
;

#endif // TESTLINEARFUNCTION_H
