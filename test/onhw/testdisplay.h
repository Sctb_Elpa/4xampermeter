#ifndef TESTDISPLAY_H
#define TESTDISPLAY_H

#include "itestsuite.h"
#include <Libraries/Adafruit_SSD1306/Adafruit_SSD1306.h>

TEST_SUITE_NAME(TestDisplay)
private:
TestDisplay() {}

public:
static void testClear() {
  display->fillScreen(WHITE);
  display->display();
  delayMilliseconds(1000);
  display->fillScreen(BLACK);
  display->display();
  delayMilliseconds(1000);
}

static void testDraw() {
  display->fillCircle(display->width() / 2, display->height() / 2, 10, WHITE);
  display->display();
  display->drawCircle(display->width() / 2, display->height() / 2, 15, WHITE);
  display->display();
  delayMilliseconds(1000);
}

static void testText() {
  display->fillScreen(BLACK);
  display->setTextSize(1);
  display->setTextColor(WHITE);
  display->setCursor(0, 0);
  display->println("Text size 1");
  display->setTextColor(BLACK, WHITE); // 'inverted' text
  display->setCursor(0, 10);
  display->setTextSize(2);
  display->println("Size 2");
  display->setTextColor(WHITE);
  display->setTextSize(3);
  display->setCursor(0, 30);
  display->print("Size 3");
  display->display();
}

void run() override {
  display = new Adafruit_SSD1306(DISPLAY_DC, DISPLAY_RST, DISPLAY_CS);
  display->begin(SSD1306_SWITCHCAPVCC, SSD1306_I2C_ADDRESS, false);

  RUN(testClear);
  RUN(testDraw);
  RUN(testText);

  delete display;
}

private:
static Adafruit_SSD1306 *display;
}
;

Adafruit_SSD1306 *TestDisplay::display;

#endif // TESTDISPLAY_H
