#ifndef TESTLINEARFUNCCLIBRATOR_H
#define TESTLINEARFUNCCLIBRATOR_H

#include <algorithm>
#include <math.h>

#include "itestsuite.h"

#include "linearfunction.h"

TEST_SUITE_NAME(TestLinearFunctionCalibrator)
private:
TestLinearFunctionCalibrator() {}

public:
static std::vector<IMathFunc<float>::Point> generateTestData(const float k,
                                                             const float b,
                                                             float x0, float dx,
                                                             int poins_Count) {
  std::vector<IMathFunc<float>::Point> res(poins_Count);

  std::for_each(res.begin(), res.end(),
                [=, &x0](IMathFunc<float>::Point &point) {
                  point.X = x0;
                  point.Y = x0 * k + b;
                  x0 += dx;
                });

  return res;
}

static void testSimpleAprox() {
  auto k = 1.0f;
  auto b = 0.0f;

  auto data = generateTestData(k, b, 0, 1, 25);

  LinearFunction<float> f;
  f.getCalibrator()->Calibrate(data);

  assert_near(k, b, f);
}

static void TestKB() {
  auto k = -1.0f;
  auto b = -1.0f;

  auto data = generateTestData(k, b, 0, 1, 25);

  LinearFunction<float> f;
  f.getCalibrator()->Calibrate(data);

  assert_near(k, b, f);
}

static void testINFk() {
  auto k = INFINITY;
  auto b = 0.0f;

  auto data = generateTestData(k, b, 0, 1, 10);

  LinearFunction<float> f;
  f.getCalibrator()->Calibrate(data);

  assert_nan(f);
}

static void testINFb() {
  auto k = 1.0f;
  auto b = INFINITY;

  auto data = generateTestData(k, b, 0, 1, 10);

  LinearFunction<float> f;
  f.getCalibrator()->Calibrate(data);

  assert_nan(f);
}

static void test1point() {
  auto k = 1.0f;
  auto b = 1.0f;

  auto data = generateTestData(k, b, 0, 1, 1);

  LinearFunction<float> f;
  f.getCalibrator()->Calibrate(data);

  assert_nan(f);
}

static void testManyData() {
  auto k = -0.196078447;
  auto b = -0.074509834;

  auto data = generateTestData(k, b, 0, 10 / 100.0f, 100);

  LinearFunction<float> f;
  f.getCalibrator()->Calibrate(data);

  assert_near(k, b, f);
}

#if 0
static void testOverloadEnd() {
  const auto k = 1.0f, b = 0.0f;
  auto data = generateTestData(k, b, 0, 1, 10);
  auto iterator = data.end();
  auto unchange = 3;

  iterator -= unchange;
  float p = NAN;
  std::for_each(iterator, data.end(), [&p](IMathFunc<float>::Point &item) {
    if (std::isnan(p))
      p = item.Y;
    else
      item.Y = p;
  });

  auto filterd = LinearFunctionCalibrator<float>::filterUnchange(data);

  ASSERT_EQUALS(data.size() - unchange, filterd.size());

  LinearFunction<float> f;
  f.getCalibrator()->Calibrate(data);

  assert_near(k, b, f);
}
#endif

static void assert_nan(const LinearFunction<float> &f) {
  static const char NAN_expected[] = "NAN expected";

  ASSERT(NAN_expected, std::isnan(f.getK()));
  ASSERT(NAN_expected, std::isnan(f.getB()));
}

static void assert_near(float k, float b, const LinearFunction<float> &f) {
  static const float sigma = 1e-5f;

  ASSERT("Linear K coeficient", fabs(f.getK() - k) < sigma);
  ASSERT("Linear b coeficient", fabs(f.getB() - b) < sigma);
}

void run() override {
  RUN(testSimpleAprox);
  RUN(TestKB);
  RUN(testINFk);
  RUN(testINFb);
  RUN(test1point);
  RUN(testManyData);
  // RUN(testOverloadEnd);
}
}
;

#endif // TESTLINEARFUNCCLIBRATOR_H
