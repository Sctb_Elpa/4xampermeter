#ifndef TESTMDPROTOCOLPERFORMANCE_H
#define TESTMDPROTOCOLPERFORMANCE_H

#include <memory>

#include "protocol.pb.h"

#include "MDProtobufMessage.h"
#include "MDProtobufProcessor.h"

#include "itestsuite.h"

using Processor_t = MDProtobufProcessor<ru_sktbelpa_AmpermeterX4_Request,
                                        ru_sktbelpa_AmpermeterX4_Response>;

TEST_SUITE_NAME(TestMDProtocolPerformance)
private:
class MyProtocolProcessor : public Processor_t::ProtocolProcessor {
public:
  bool processRequest(const Processor_t::Request_t &req,
                      Processor_t::Response_t &resp) override {
    resp.id = req.id;
    resp.deviceID = ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID;
    resp.protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION;
    resp.Global_status =
        ((req.deviceID == ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER) ||
         (req.deviceID == ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID)) &&
                (req.protocolVersion <=
                 ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION)
            ? ru_sktbelpa_AmpermeterX4_Response_STATUS_OK
            : ru_sktbelpa_AmpermeterX4_Response_STATUS_PROTOCOL_ERROR;
    return true;
  }

  std::vector<uint8_t> createProtocolErrorMessage() const override {
    return std::vector<uint8_t>();
  }
};

TestMDProtocolPerformance() {}

public:
static void testFullperform() {
  ru_sktbelpa_AmpermeterX4_Request req{
      .id = 42,
      .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
      .protocolVersion = 0};

  auto ser_req = pHalper_serialiser->Serialise(req);

  std::vector<uint8_t> test_resp;
  pProcessor->process(ser_req.data(), ser_req.size(), test_resp);

  ASSERT_EQUALS(false, test_resp.empty());

  bool isOk;

  auto resp = pHalper_deserialiser->Deserialise(test_resp.data(),
                                                test_resp.size(), &isOk);

  ASSERT_EQUALS(true, isOk);
  ASSERT_EQUALS(req.id, resp.id);
  ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID, resp.deviceID);
  ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
                resp.protocolVersion);
  ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_Response_STATUS_OK,
                resp.Global_status);
}

static void testPreformance() {
  const int calls = 10000;

  ru_sktbelpa_AmpermeterX4_Request req{
      .id = 42,
      .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
      .protocolVersion = 0};

  auto ser_req = pHalper_serialiser->Serialise(req);
  std::vector<uint8_t> test_resp;

  auto start = RTC.getRtcNanoseconds();
  for (int i = 0; i < calls; ++i) {
    pProcessor->process(ser_req.data(), ser_req.size(), test_resp);
    ASSERT_EQUALS(false, test_resp.empty());
  }

  auto end = RTC.getRtcNanoseconds();
  uint32_t time_taken = end - start;

  console_printf("%d Calls of MDProtobufProcessor::process() takes %u "
                 "nanosec. (%.1f calls/sec.)\n",
                 calls, time_taken, (float)(1e+9f * calls / time_taken));
}

void run() override {
  requestFields = ru_sktbelpa_AmpermeterX4_Request_fields;
  responceFields = ru_sktbelpa_AmpermeterX4_Response_fields;

  pDeserialiser =
      new MDProtobufDeserialiser<Processor_t::Request_t>(magick, requestFields);
  pSerialiser =
      new MDProtobufSerialiser<Processor_t::Response_t>(magick, responceFields);
  pProtocolProcessor = new MyProtocolProcessor;

  pProcessor =
      new Processor_t(*pDeserialiser, *pSerialiser, *pProtocolProcessor);

  pHalper_serialiser =
      new MDProtobufSerialiser<Processor_t::Request_t>(magick, requestFields);
  pHalper_deserialiser = new MDProtobufDeserialiser<Processor_t::Response_t>(
      magick, responceFields);

  RUN(testFullperform);
  RUN(testPreformance);

  delete pDeserialiser;
  delete pSerialiser;
  delete pProtocolProcessor;

  delete pProcessor;

  delete pHalper_serialiser;
  delete pHalper_deserialiser;
}

private:
static MDProtobufDeserialiser<Processor_t::Request_t> *pDeserialiser;
static MDProtobufSerialiser<Processor_t::Response_t> *pSerialiser;
static MyProtocolProcessor *pProtocolProcessor;

static Processor_t *pProcessor;

static MDProtobufSerialiser<Processor_t::Request_t> *pHalper_serialiser;
static MDProtobufDeserialiser<Processor_t::Response_t> *pHalper_deserialiser;

static constexpr const uint8_t magick = ru_sktbelpa_AmpermeterX4_INFO_MAGICK;
static const pb_field_t *requestFields;
static const pb_field_t *responceFields;
}
;

MDProtobufDeserialiser<Processor_t::Request_t>
    *TestMDProtocolPerformance::pDeserialiser;
MDProtobufSerialiser<Processor_t::Response_t>
    *TestMDProtocolPerformance::pSerialiser;

TestMDProtocolPerformance::MyProtocolProcessor
    *TestMDProtocolPerformance::pProtocolProcessor;

Processor_t *TestMDProtocolPerformance::pProcessor;

MDProtobufSerialiser<Processor_t::Request_t>
    *TestMDProtocolPerformance::pHalper_serialiser;
MDProtobufDeserialiser<Processor_t::Response_t>
    *TestMDProtocolPerformance::pHalper_deserialiser;

constexpr const uint8_t TestMDProtocolPerformance::magick;
const pb_field_t *TestMDProtocolPerformance::requestFields;
const pb_field_t *TestMDProtocolPerformance::responceFields;

#endif // TESTMDPROTOCOLPERFORMANCE_H
