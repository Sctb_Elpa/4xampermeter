#ifndef TESTVOLTAGECONTROLLER_H
#define TESTVOLTAGECONTROLLER_H

#include <myassert.h>

#include "ITimer.h"
#include "itestsuite.h"

#include "linearfunction.h"
#include "voltagecontroller.h"

#include "dcdcadj.h"
#include "timerexecutor.h"

#ifndef ARDUINO
#include "Adafruit_INA219.h"
#endif

class FakeActor {
public:
  FakeActor() { value = 0.0f; }
  void setValue(float value) { this->value = value; }

  float value;
};

class FakeSensor {
public:
  FakeSensor() { value = 0.0f; }
  float getValue() { return value; }

  float value;
};

class FakeSensorActorConnected {
public:
  FakeSensorActorConnected(FakeActor *pactor) : converter() {
    this->pactor = pactor;
  }
  float getValue() {
    return converter ? (*converter)(pactor->value) : pactor->value;
  }

  std::shared_ptr<IMathFunc<float>> converter;

  FakeActor *pactor;
};

class SyncPeriodicalExecutor : public ITimer {
public:
  SyncPeriodicalExecutor() { skip_delay = false; }

  ITimer &initializeMs(const uint32_t milliseconds,
                       const TimerCB &cb = TimerCB()) override {
    delegate = cb;
    period = milliseconds;
    m_microseconds = false;
    return *this;
  }

  ITimer &initializeUs(const uint32_t microseconds,
                       const TimerCB &cb = TimerCB()) override {
    delegate = cb;
    period = microseconds;
    m_microseconds = true;
    return *this;
  }

  void start(bool repeating = true) {
    this->repeating = repeating;
    while (this->repeating) {
      if (!skip_delay)
        if (m_microseconds)
          delayMicroseconds(period);
        else
          delayMilliseconds(period);

      delegate();
    }
  }

  void pause() override {}
  void resume() override {}

  void stop() { repeating = false; }

  int percentage = 0;
  bool fin = false;

  void calibrationFinished(VoltageController &c) { fin = true; }
  void calibrationpercentUpdate(VoltageController &c, int p) { percentage = p; }

  bool isRunning() const override { return repeating; }

  bool skip_delay;

private:
  TimerCB delegate;
  bool m_microseconds;
  int32_t period;

  bool repeating;
};

TEST_SUITE_NAME(TestVoltageController)
public:
/// Тест проверяет защиту контроллера от работы в режиме неполной инициализации
static void testEmpty() {
  VoltageController controller;

  ASSERT_EQUALS(controller.status(), VoltageController::CONFIG_INCOMPLEAD);
  ASSERT_EQUALS(controller.setVoltage(0.0),
                VoltageController::CONFIG_INCOMPLEAD);

  FakeActor actor;
  controller.setActor(std::make_shared<VoltageActor>(
      std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
  ASSERT_EQUALS(controller.status(), VoltageController::CONFIG_INCOMPLEAD);
  ASSERT_EQUALS(controller.setVoltage(0.0),
                VoltageController::CONFIG_INCOMPLEAD);

  const float test_val = 42.0f;
  auto f = std::make_shared<LinearFunction<float>>(1.0f / test_val);
  controller.setV2AExpression(f);

  ASSERT_EQUALS(controller.status(), VoltageController::OK);
  ASSERT_EQUALS(controller.setVoltage(test_val), VoltageController::OK);
  ASSERT_EQUALS(actor.value, test_val * f->getK());
}

/// Тест проверяет работу лимитов выходного напряжения
static void testLimits() {

  VoltageController controller;
  FakeActor actor;
  auto f = std::make_shared<LinearFunction<float>>(1.0f / 20.0f);

  controller.setActor(std::make_shared<VoltageActor>(
      std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
  controller.setV2AExpression(f);
  controller.setVoltage_min(10.0f);
  auto after_limit_change_value = actor.value;
  ASSERT_EQUALS(controller.setVoltage(5.0f), VoltageController::OUT_OF_LIMITS);
  ASSERT_EQUALS(actor.value, after_limit_change_value);

  float test_value = 15.0f;
  ASSERT_EQUALS(controller.setVoltage(test_value), VoltageController::OK);
  ASSERT_EQUALS(actor.value, test_value * f->getK());

  controller.setVoltage_max(20.0f);
  ASSERT_EQUALS(controller.setVoltage(42.0f), VoltageController::OUT_OF_LIMITS);
  ASSERT_EQUALS(actor.value, test_value * f->getK());

  test_value = 17.0f;
  ASSERT_EQUALS(controller.setVoltage(test_value), VoltageController::OK);
  ASSERT_EQUALS(actor.value, test_value * f->getK());
}

/// Тест проверяет смещение выходного напряжения, если текущее его значение
/// нахдоится за новой границей
static void testMoveLimits() {
  VoltageController controller;
  FakeActor actor;
  auto f = std::make_shared<LinearFunction<float>>(1.0f / 20.0f);

  controller.setActor(std::make_shared<VoltageActor>(
      std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
  controller.setV2AExpression(f);

  controller.setVoltage_min(10.0f);
  controller.setVoltage_max(20.0f);
  controller.setVoltage(19.0f);

  float new_max_limit = 15.0f;
  controller.setVoltage_max(new_max_limit);
  ASSERT_EQUALS(actor.value, new_max_limit * f->getK());

  ASSERT_EQUALS(controller.setVoltage(11.0f), VoltageController::OK);

  float new_min_limit = 13.0f;
  controller.setVoltage_min(new_min_limit);
  ASSERT_EQUALS(actor.value, new_min_limit * f->getK());
}

/// Тест проверяет работу логики установки выходного напряжения
static void testSetOutputVoltage() {
  FakeActor actor;
  VoltageController controler;
  auto f = std::make_shared<LinearFunction<float>>();

  const float test_value = 42.0;

  f->setK(1.0f / test_value);

  controler.setActor(std::make_shared<VoltageActor>(
      std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
  controler.setV2AExpression(f);

  ASSERT_EQUALS(controler.setVoltage(test_value), VoltageController::OK);
  ASSERT_EQUALS(actor.value, test_value * f->getK());
}

/// Тест проверяет наличие ошибки при попытке установки верхнего лимита
/// выходного напряжения ниже нижнего и наоборот
static void testLimitsOverlap() {
  VoltageController controler;

  bool assert_was = false;
  assert_catcher = [&assert_was](const char *, int, const char *,
                                 const char *) { assert_was = true; };

  controler.setVoltage_min(10.0f);
  controler.setVoltage_max(9.0f);
  ASSERT_EQUALS(assert_was, true);

  assert_was = false;
  controler.setVoltage_max(20.0f);
  controler.setVoltage_min(25.0f);
  ASSERT_EQUALS(assert_was, true);

  assert_catcher = nullptr;
}

/// Тест проверят корректность управления котроллером актора.
/// устанавливаемое значение всегда должно быть в диопазоне 0.0f <= v <= 1.0f
static void testOverUnderFlow() {
  FakeActor actor;
  VoltageController controler;
  auto f = std::make_shared<LinearFunction<float>>(10.0f);

  controler.setActor(std::make_shared<VoltageActor>(
      std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
  controler.setV2AExpression(std::make_shared<LinearFunction<float>>(10.0f));

  ASSERT_EQUALS(controler.setVoltage(1.0f), VoltageController::EXPR_OVERFLOW);
}

/// Тест проверяет реакцию конроллера на отсутствие сенсора
static void testNoSensor() {
  FakeActor actor;
  VoltageController controler;

  controler.setActor(std::make_shared<VoltageActor>(
      std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
  controler.setV2AExpression(std::make_shared<LinearFunction<float>>());

  ASSERT_EQUALS(
      controler.startCalibration(
          std::shared_ptr<SyncPeriodicalExecutor>(new SyncPeriodicalExecutor)),
      VoltageController::CONFIG_INCOMPLEAD);
}

/// Тест проверяет реакцию контроллера на выходное значение сенсора NAN
static void testSensorNAN() {
  FakeActor actor;
  VoltageController controler;
  FakeSensor sensor;

  controler.setActor(std::make_shared<VoltageActor>(
      std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
  controler.setV2AExpression(std::make_shared<LinearFunction<float>>());
  controler.setSensor(std::make_shared<VoltageSensor>(
      std::bind(&FakeSensor::getValue, &sensor)));

  sensor.value = NAN;
  ASSERT_EQUALS(
      controler.startCalibration(std::make_shared<SyncPeriodicalExecutor>()),
      VoltageController::SENSOR_ERROR);
}

/// Тест проверяет работу калибровки на тестовых акторе и сенсоре
static void testRunFakeCalibration() {
  FakeActor actor;
  VoltageController controler;
  auto f = std::make_shared<LinearFunction<float>>();
  FakeSensorActorConnected sensor(&actor);
  auto executor = std::make_shared<SyncPeriodicalExecutor>();

  sensor.converter = std::make_shared<LinearFunction<float>>(5.1f, 0.38f);

  controler.setActor(std::make_shared<VoltageActor>(
      std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
  controler.setV2AExpression(f);
  controler.setSensor(std::make_shared<VoltageSensor>(
      std::bind(&FakeSensorActorConnected::getValue, &sensor)));

  executor->skip_delay = true;

  ASSERT_EQUALS(
      controler.startCalibration(
          executor,
          std::make_shared<CalibrationFinishedCB>(
              std::bind(&SyncPeriodicalExecutor::calibrationFinished,
                        executor.get(), std::placeholders::_1)),
          std::make_shared<CalibrationProgressUpdateCB>(std::bind(
              &SyncPeriodicalExecutor::calibrationpercentUpdate, executor.get(),
              std::placeholders::_1, std::placeholders::_2))),
      VoltageController::OK);

  ASSERT_EQUALS(executor->percentage, 100);
  ASSERT_EQUALS(executor->fin, true);

  auto inv = sensor.converter->getInvertedFunction();
  auto expected_fun = static_cast<LinearFunction<float> *>(inv.get());

  static const float sigma = 1e-5f;
  ASSERT("k coefficient", fabs(f->getK() - expected_fun->getK()) < sigma);
  ASSERT("b coefficient", fabs(f->getB() - expected_fun->getB()) < sigma);
}

/// Запуск реальной калибровки
static void testRunRealCalibration() {
  VoltageController controler;
  auto f = std::make_shared<LinearFunction<float>>();
  DCDCAdj actor(2, -1);
  actor.setInversion(true);
  Adafruit_INA219 sensor(0x41);
  auto executor = std::make_shared<SyncPeriodicalExecutor>();

  actor.begin();
  sensor.begin();

  controler.setActor(std::make_shared<VoltageActor>(
      std::bind(&DCDCAdj::setValue, &actor, std::placeholders::_1)));
  controler.setV2AExpression(f);
  controler.setSensor(std::make_shared<VoltageSensor>(
      std::bind(&Adafruit_INA219::getBusVoltage_V, &sensor)));

  ASSERT_EQUALS(
      controler.startCalibration(
          executor,
          std::make_shared<CalibrationFinishedCB>(
              std::bind(&SyncPeriodicalExecutor::calibrationFinished,
                        executor.get(), std::placeholders::_1)),
          std::make_shared<CalibrationProgressUpdateCB>(std::bind(
              &SyncPeriodicalExecutor::calibrationpercentUpdate, executor.get(),
              std::placeholders::_1, std::placeholders::_2)),
          20, 100),
      VoltageController::OK);

  ASSERT_EQUALS(executor->percentage, 100);
  ASSERT_EQUALS(executor->fin, true);

  console_printf("HW calibrationg cesult: %f * x + %f\n", f->getK(), f->getB());
  ASSERT("K mast be < 0", f->getK() < 0.0f == !actor.isInverted());
  ASSERT("B mast be > 0", f->getB() > 0.0f == !actor.isInverted());

  console_printf("Voltage range %f - %f\n", controler.voltage_min(),
                 controler.voltage_max());
  const auto test_voltage =
      (controler.voltage_max() + controler.voltage_min()) / 2.0f;

  auto r = controler.setVoltage(test_voltage);
  console_printf("Setting voltage %f , result %d\n", test_voltage, r);
  ASSERT_EQUALS(r, VoltageController::OK);
  ASSERT_EQUALS(test_voltage, controler.Voltage());
  delayMilliseconds(50);

  auto real_voltage = controler.actualVoltage();
  console_printf(
      "Voltage setup error: %.2f%%, real voltage: %.2f V (setup: %.2f V)\n",
      (real_voltage - test_voltage) * 100.0f /
          (controler.voltage_max() - controler.voltage_min()),
      real_voltage, test_voltage);
}

void run() override {
  RUN(testSetOutputVoltage);
  RUN(testEmpty);
  RUN(testLimits);
  RUN(testMoveLimits);
  RUN(testLimitsOverlap);
  RUN(testOverUnderFlow);
  RUN(testNoSensor);
  RUN(testSensorNAN);
  RUN(testRunFakeCalibration);
  RUN(testRunRealCalibration);
}
}
;

#endif // TESTVOLTAGECONTROLLER_H
