#ifndef TESTVECTORITHERATOR_H
#define TESTVECTORITHERATOR_H

#include <SmingCore/HardwareSerial.h>
#include <Wiring/WVector.h>

#include "vectoritherator.h"

#include "itestsuite.h"

TEST_SUITE_NAME(TestVectorItherator)
public:
static void testInc_dec() {
  Vector<String> v;
  v.addElement("a");
  v.addElement("b");
  v.addElement("c");

  VectorItherator<String> it(v);

  ASSERT_EQUALS(&v[0], it.operator->());
  ++it;
  ASSERT_EQUALS(&v[1], it.operator->());
  ++it;
  ASSERT_EQUALS(&v[2], it.operator->());
  --it;
  ASSERT_EQUALS(&v[1], it.operator->());
}

static void testIncrement() {
  Vector<String> v;
  v.addElement("a");
  v.addElement("b");
  v.addElement("c");

  VectorItherator<String> it(v);
  VectorItherator<String> end(v, v.size());

  ASSERT_EQUALS(v[0], *it++);
  ASSERT_EQUALS(v[1], *it++);
  ASSERT_EQUALS(v[2], *it++);

  ASSERT_EQUALS(end, it);
}

static void testDecrement() {
  Vector<String> v;
  v.addElement("a");
  v.addElement("b");
  v.addElement("c");

  VectorItherator<String> begin(v);
  VectorItherator<String> it(v, v.size());

  --it;
  ASSERT_EQUALS(v[2], *it--);
  ASSERT_EQUALS(v[1], *it--);
  ASSERT_EQUALS(v[0], *it);

  ASSERT_EQUALS(begin, it);
}

void run() override {
  RUN(testInc_dec);
  RUN(testIncrement);
  RUN(testDecrement);
}
}
;

#endif // TESTVECTORITHERATOR_H
