#ifndef ITESTSUITE_H
#define ITESTSUITE_H

#include "make_unique.h"
#include <SmingCore/HardwareSerial.h>

#define console_printf(...) Serial.printf(__VA_ARGS__)

#define printf(...) Serial.printf(__VA_ARGS__)
#include "tinytest.h"
#undef printf

#include <functional>

#if defined(RUN_TESTS) && RUN_TESTS > 0
extern std::function<void(const char *file, int line, const char *f,
                          const char *msg)>
    assert_catcher;
#else
// заглушка
std::function<void(const char *file, int line, const char *f, const char *msg)>
    assert_catcher = nullptr;
#endif

#define TEST_SUITE_NAME(name)                                                  \
  class name : public ITestSuite {                                             \
  public:                                                                      \
    const char *testname() const override { return #name; }                    \
    static std::unique_ptr<ITestSuite> instance() {                            \
      return std::unique_ptr<ITestSuite>(new name);                            \
    }                                                                          \
                                                                               \
  private:

class ITestSuite {
public:
  virtual const char *testname() const = 0;
  virtual void run() = 0;
  void start() {
    console_printf("Executing %s...\n", testname());
    run();
  }
};

#endif // ITESTSUITE_H
