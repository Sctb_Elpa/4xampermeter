#ifndef TESTVOLTMETER_H
#define TESTVOLTMETER_H

#include "Adafruit_INA219.h"
#include "itestsuite.h"

TEST_SUITE_NAME(TestVoltmeter)
private:
TestVoltmeter() {}

public:
static void testGetVoltage() {
  float v = ampermeter->getBusVoltage_V();
  const float error_val = (int16_t)(((uint16_t)0xffff >> 3) * 4) * 0.001f;
  ASSERT("Read voltage error\n", v != error_val);
}

static void testGetCurrent() {
  auto v = ampermeter->getCurrent_mA();

  char err[] = "Get current error\n";
  const float err_res = (int16_t)0xffffff;

  ASSERT(err, v != err_res / 10.0f);
  ASSERT(err, v != err_res / 20.0f);
  ASSERT(err, v != err_res / 25.0f);
}

static void testGetPower() {
  auto v = ampermeter->getShuntVoltage_mV();

  ASSERT("Get shunt voltage error\n", v != (int16_t)0xffff * 0.01);
}

void run() override {
  ampermeter = new Adafruit_INA219;
  const uint8_t addrs[] = {0x41};

  for (size_t i = 0; i < sizeof(addrs); ++i) {
    ampermeter->begin(addrs[i]);

    RUN(testGetVoltage);
    RUN(testGetCurrent);
    RUN(testGetPower);
  }

  delete ampermeter;
}

private:
static Adafruit_INA219 *ampermeter;
}
;

Adafruit_INA219 *TestVoltmeter::ampermeter;

#endif // TESTVOLTMETER_H
