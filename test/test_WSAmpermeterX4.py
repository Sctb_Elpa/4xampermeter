#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import pytest
from autobahn.twisted.websocket import WebSocketClientProtocol, WebSocketClientFactory, connectWS
from twisted.internet import reactor
from urllib.parse import urlparse
import protocol_pb2

import libAmpermeterX4


def print_help():
    print("Возможно вы пытаитесь запустить этот файл напрямую?\n"
          "Запстите: $ TEST_URL=<websocket-url> make pbws_pytest.run\n")


@pytest.fixture
def url():
    if not ('TEST_URL' in os.environ.keys()):
        print("Не указан URL адрес для теста!\n")
        print_help()
        assert 0

    return os.environ['TEST_URL']


def connect(factory):
    connectWS(factory)
    reactor.run()


class Protocol(WebSocketClientProtocol):
    def onConnect(self, response):
        print("Server connected: {0}".format(response.peer))

    def onOpen(self):
        try:
            d = self.req.serialiseMDToString()
            self.sendMessage(d, isBinary=True)
        except Exception as e:
            reactor.stop()

    def onMessage(self, payload, isBinary):
        if isBinary:
            print('ressived {} bytes'.format(len(payload)))
            response = libAmpermeterX4.Response(protocol_pb2.INFO.Value('MAGICK'))
            response.parceMD(payload)
            assert response.id == self.req.id
            assert response.protocolVersion >= protocol_pb2.INFO.Value('PROTOCOL_VERSION')
            assert response.deviceID == protocol_pb2.INFO.Value('AMPERMETERX4_ID')
            self.processResponce(response)
            self.sendClose(1000)
            reactor.stop()
        else:
            print('Ressived string: {}'.format(payload))

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))

    # for subclasses
    def processResponce(self, resp):
        pass


def test_ping(url):
    class PingProtocol(Protocol):
        def __init__(self):
            super().__init__()
            self.req = libAmpermeterX4.AmpermeterX4_requestBuilder(
                protocol_pb2.INFO.Value('MAGICK')).build_ping_request()

    f = WebSocketClientFactory(url)
    f.protocol = PingProtocol
    connect(f)

