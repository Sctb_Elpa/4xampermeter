#ifndef TEST_DELIMITED_H
#define TEST_DELIMITED_H

#include "MDProtobufMessage.h"
#include "protocol.pb.h"

#include "make_unique.h"
#include <memory>

#include <cxxtest/TestSuite.h>

class TestSerialiserDeserialiser : public CxxTest::TestSuite {
public:
  static constexpr uint8_t magick = 9;
  static constexpr const pb_field_t *fields =
      ru_sktbelpa_AmpermeterX4_Request_fields;

  using serialiser_t = MDProtobufSerialiser<ru_sktbelpa_AmpermeterX4_Request>;
  using deserialiser_t =
      MDProtobufDeserialiser<ru_sktbelpa_AmpermeterX4_Request>;

  std::unique_ptr<serialiser_t> serialiser;
  std::unique_ptr<deserialiser_t> deserialiser;

  void setUp() override {
    serialiser = std::move(std::make_unique<serialiser_t>(magick, fields));
    deserialiser = std::move(std::make_unique<deserialiser_t>(magick, fields));
  }

  void testSerialise() {
    const uint8_t magick = 9;
    ru_sktbelpa_AmpermeterX4_Request msg{
        .id = 0,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID,
        .protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION};

    auto res = serialiser->Serialise(msg);

    auto buf = std::make_unique<pb_byte_t[]>(res.size());
    auto ostream = pb_ostream_from_buffer(buf.get(), res.size());
    pb_encode(&ostream, fields, &msg);

    TS_ASSERT_EQUALS(res.data()[0], magick);
    TS_ASSERT_SAME_DATA(buf.get(), &res.data()[1 + 1], ostream.bytes_written);
  }

  void testDeserialise() {
    ru_sktbelpa_AmpermeterX4_Request msg{
        .id = 0,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID,
        .protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION};

    auto res = serialiser->Serialise(msg);

    bool isdeserialised;
    auto msg_deserialised =
        deserialiser->Deserialise(res.data(), res.size(), &isdeserialised);

    TS_ASSERT_EQUALS(isdeserialised, true);
    TS_ASSERT_EQUALS(msg.id, msg_deserialised.id);
    TS_ASSERT_EQUALS(msg.deviceID, msg_deserialised.deviceID);
    TS_ASSERT_EQUALS(msg.protocolVersion, msg_deserialised.protocolVersion);
    TS_ASSERT_EQUALS(false, msg_deserialised.has_setSettings);
  }
};

constexpr uint8_t TestSerialiserDeserialiser::magick;
constexpr const pb_field_t *TestSerialiserDeserialiser::fields;

#endif // TEST_DELIMITED_H
