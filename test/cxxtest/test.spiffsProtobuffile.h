#ifndef TEST_SPIFFSPROTOBUFFILE_H
#define TEST_SPIFFSPROTOBUFFILE_H

#include <cxxtest/TestSuite.h>

#include <fstream>
#include <unistd.h>

#include "calibration_summary.pb.h"

#include "spiffsprotobuffile.h"

class TestSPIFFSProtobufFile : public CxxTest::TestSuite {
public:
  void testGetSize() {
    const std::string filename("/tmp/testfile");

    SPIFFSProtobufFile f;

    TS_ASSERT_EQUALS(0, f.size());

    const std::string s("Test string");

    {
      std::ofstream outfile(filename, std::ofstream::binary);
      outfile << s;
    }

    SPIFFSProtobufFile f_f(filename);
    TS_ASSERT_EQUALS(s.size(), f_f.size());

    unlink(filename.c_str());
  }

  void testEncode() {
    SPIFFSProtobufFile fEmpty;
    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs c{1, 2};

    TS_ASSERT(fEmpty.Encode(
        ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields, &c));
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_size,
                     fEmpty.size());

    SPIFFSProtobufFile fReal("/tmp/testfile");

    TS_ASSERT(fReal.Encode(
        ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields, &c));
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_size,
                     fReal.size());
  }

  void testDecode() {
    SPIFFSProtobufFile fEmpty;
    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs c;

    TS_ASSERT_EQUALS(
        false,
        fEmpty.Decode(ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields,
                      &c));

    SPIFFSProtobufFile fReal("/tmp/testfile");
    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs c1{1, 2}, c2;

    fReal.Encode(ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields, &c1);
    TS_ASSERT(fReal.Decode(
        ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields, &c2));
    TS_ASSERT_SAME_DATA(&c1, &c2, sizeof(c1));
  }
};

#endif // TEST_SPIFFSPROTOBUFFILE_H
