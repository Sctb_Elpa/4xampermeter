#ifndef TEST_VALIDATORS_H
#define TEST_VALIDATORS_H

#include "settingsvalidators.h"

#include <arpa/inet.h>
#include <cxxtest/TestSuite.h>

using namespace SettingsValidators;

class TestValidators : public CxxTest::TestSuite {
public:
  void testIPValidator() {
    IPValidator ipValidator;

    // values mas be NETWORK ENDIAN

    TS_ASSERT_EQUALS(false, ipValidator.validate(htonl(0)));
    TS_ASSERT_EQUALS(false, ipValidator.validate(htonl(0xffffffff)));
    TS_ASSERT_EQUALS(true, ipValidator.validate(htonl(0xC0A80001)));
    TS_ASSERT_EQUALS(false, ipValidator.validate(htonl(0xC0A80000)));
    TS_ASSERT_EQUALS(false, ipValidator.validate(htonl(0xC0A800ff)));
  }

  void testESSIDValidator() {
    ESSIDValidator essidvalidator(32);

    TS_ASSERT_EQUALS(true, essidvalidator.validate("Hello"));
    TS_ASSERT_EQUALS(
        true, essidvalidator.validate("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"));
    TS_ASSERT_EQUALS(true, essidvalidator.validate("324"));
    TS_ASSERT_EQUALS(true, essidvalidator.validate("A_-3"));
    TS_ASSERT_EQUALS(false, essidvalidator.validate(""));
    TS_ASSERT_EQUALS(
        false, essidvalidator.validate("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb"));
    TS_ASSERT_EQUALS(false, essidvalidator.validate("*sdf*"));
    TS_ASSERT_EQUALS(false, essidvalidator.validate("%^&!aasldas155-| "));
  }

  void testStringLengthValidator() {
    StringLengthValidator passwordValidator(10);
    TS_ASSERT_EQUALS(true, passwordValidator.validate("Hello"));
    TS_ASSERT_EQUALS(true, passwordValidator.validate("Password13"));
    TS_ASSERT_EQUALS(
        false, passwordValidator.validate("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAb"));
    TS_ASSERT_EQUALS(false, passwordValidator.validate("%^&!aasldas155-| "));
  }
};

#endif // TEST_VALIDATORS_H
