#ifndef TEST_NANOPB_H
#define TEST_NANOPB_H

#include <algorithm>
#include <vector>

#include "halpers.pb.h"
#include "spiffsprotobuffile.h"
#include "utils.h"

#include <cxxtest/TestSuite.h>

template <typename T> struct treeMessageEncoder {
  using Tmsg = T;

  virtual const Tmsg toMessage() const = 0;
};

class TestNanopb : public CxxTest::TestSuite {
public:
  struct TestMS {
    std::string field1;
    int field2;
    int f3;
  };
  std::vector<TestMS> data;

  void setUp() {
    data = {
        {"1", 1, 2}, {"1", 2, 10}, {"1", 3, 18},
        {"2", 1, 5}, {"2", 2, 7},  {"3", 1, -1},
    };
  }

  // Данный тест провален по тому, что колбэки полей вызываются дважды и не
  // должны менять какой-либо стейт, что отражено в (void *const *arg)
  void disabled_testRepeated_intoRepeated() {
    struct TestRepeatedHolder {
      TestRepeatedHolder(const std::vector<TestMS> &data)
          : offset(-1), data(data) {}

      const TestMS *next() {
        return (offset + 1 < data.size()) ? &data.at(++offset) : nullptr;
      }

      const TestMS *current() const {
        return (offset < data.size()) ? &data.at(offset) : nullptr;
      }

      const TestMS *look_next() const {
        return hasNext() ? &data.at(offset + 1) : nullptr;
      }

      bool hasNext() const { return (offset + 1 < data.size()); }

    private:
      int32_t offset;
      const std::vector<TestMS> &data;
    };

    TestRepeatedHolder holder(data);

    static auto encode_string = [](pb_ostream_t *stream,
                                   const pb_field_t *field, void *const *arg) {
      auto holder = static_cast<TestRepeatedHolder *>(*arg);

      auto item = holder->current();
      while (item) {
        auto &s = item->field1;

        if (!pb_encode_tag_for_field(stream, field))
          return false;
        if (!pb_encode_string(stream, (const pb_byte_t *)s.c_str(), s.length()))
          return false;

        auto next = holder->look_next();
        item = (next && (next->field1 == s)) ? holder->next() : nullptr;
      }
      return true;
    };

    static auto encodeTestRepeated = [](pb_ostream_t *stream,
                                        const pb_field_t *field,
                                        void *const *arg) {
      auto holder = static_cast<TestRepeatedHolder *>(*arg);
      const TestMS *item;
      while (item = holder->next()) {
        ru_sktbelpa_AmpermeterX4_TestRepeated TestRepeateditem{
            .field1 = {.funcs = {.encode = encode_string},
                       .arg = (void *)holder},
            .f2_m = {.funcs = {.encode = nullptr}, .arg = nullptr}};

        if (!pb_encode_tag_for_field(stream, field))
          return false;
        if (!pb_encode_submessage(stream,
                                  ru_sktbelpa_AmpermeterX4_TestRepeated_fields,
                                  &TestRepeateditem))
          return false;
      }
      return true;
    };

    ru_sktbelpa_AmpermeterX4_TestRepatedRepeated msg{
        .tr2 = {.funcs = {.encode = encodeTestRepeated}, .arg = &holder}};
    SPIFFSProtobufFile f("repeated_repeated.pb2");

    f.Encode(ru_sktbelpa_AmpermeterX4_TestRepatedRepeated_fields, &msg);
  }

  //----------------------------------------------------------------------------

  void testRepeated_intoRepeated() {
    class _tree : public treeMessageEncoder<
                      ru_sktbelpa_AmpermeterX4_TestRepatedRepeated> {
    public:
      using treeMessageEncoder::Tmsg;

      _tree(const std::vector<TestMS> &src) {
        auto it = src.cbegin();
        while (it != src.cend())
          TestRepeatedItems.emplace_back(it);
      }

      const Tmsg toMessage() const override {
        return Tmsg{
            .tr2 = {.funcs = {.encode = &_tree::encode}, .arg = (void *)this}};
      }

    private:
      class TestMSGHolder
          : public treeMessageEncoder<ru_sktbelpa_AmpermeterX4_TestMSG> {
      public:
        using treeMessageEncoder::Tmsg;

        TestMSGHolder(std::vector<TestMS>::const_iterator &cit)
            : field1(cit->field2), field2(cit->f3) {
          ++cit;
        }

        const Tmsg toMessage() const override {
          return Tmsg{.field1 = field1, .field2 = field2};
        }

      private:
        int32_t field1;
        int32_t field2;
      };

      class TestRepeatedHolder
          : public treeMessageEncoder<ru_sktbelpa_AmpermeterX4_TestRepeated> {
      public:
        using treeMessageEncoder::Tmsg;

        TestRepeatedHolder(std::vector<TestMS>::const_iterator &cit)
            : field1(cit->field1) {
          while (field1 == cit->field1) {
            TestMSGItems.emplace_back(cit);
          }
        }

        const Tmsg toMessage() const override {
          return Tmsg{
              .field1 =
                  {.funcs = {.encode =
                                 Utils::arg2string_pb_encoder<std::string>},
                   .arg = (void *)&field1},
              .f2_m = {.funcs = {.encode = encode_TestMSGList},
                       .arg = (void *)this},
          };
        }

      private:
        static bool encode_TestMSGList(pb_ostream_t *stream,
                                       const pb_field_t *field,
                                       void *const *arg) {
          const auto TestMSGItems =
              static_cast<const TestRepeatedHolder *>(*arg)->TestMSGItems;

          for (auto it = TestMSGItems.cbegin(); it != TestMSGItems.cend();
               ++it) {
            if (!pb_encode_tag_for_field(stream, field))
              return false;

            auto msg = it->toMessage();

            if (!pb_encode_submessage(
                    stream, ru_sktbelpa_AmpermeterX4_TestMSG_fields, &msg))
              return false;
          }
          return true;
        }

        std::string field1;
        std::list<TestMSGHolder> TestMSGItems;
      };

      static bool encode(pb_ostream_t *stream, const pb_field_t *field,
                         void *const *arg) {
        const auto TestRepeatedItems =
            static_cast<const _tree *>(*arg)->TestRepeatedItems;
        for (auto it = TestRepeatedItems.cbegin();
             it != TestRepeatedItems.cend(); ++it) {

          if (!pb_encode_tag_for_field(stream, field))
            return false;

          auto msg = it->toMessage();

          if (!pb_encode_submessage(
                  stream, ru_sktbelpa_AmpermeterX4_TestRepeated_fields, &msg))
            return false;
        }
        return true;
      }

      std::list<TestRepeatedHolder> TestRepeatedItems;
    };

    auto reordered = _tree(data);
    auto msg = reordered.toMessage();
    SPIFFSProtobufFile f("repeated_repeated.pb2");

    f.Encode(ru_sktbelpa_AmpermeterX4_TestRepatedRepeated_fields, &msg);
  }
};

#endif // TEST_NANOPB_H
