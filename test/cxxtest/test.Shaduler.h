#ifndef TEST_SHADULER_H
#define TEST_SHADULER_H

#include <atomic>
#include <future>

#include "shaduler.h"

#include <cxxtest/TestSuite.h>

class TestShaduler : public CxxTest::TestSuite {
public:
  void testShadulerCreate() {
    Shaduler shaduler;

    shaduler.start();
  }

  void _testRunShaduled() {
    Shaduler shaduler(10);

    shaduler.start();

    std::atomic_bool t(false);

    shaduler.shadule(SHADULABLE([&t]() {
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
      t = true;
    }));

    while (!t)
      std::this_thread::sleep_for(std::chrono::milliseconds(1));

    t = false;
  }

  void testStaduleDouble() {
    Shaduler shaduler(1);

    std::atomic_int i(0);

    auto f1 = [&i]() { i++; };

    shaduler.shadule(SHADULABLE(f1));
    shaduler.shadule(SHADULABLE(f1));

    shaduler.start();

    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    TS_ASSERT_EQUALS(1, i.load());
  }
};

#endif // TEST_SHADULER_H
