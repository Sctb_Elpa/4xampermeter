#ifndef TESTMDPROTOCOLPERFORMANCE_H
#define TESTMDPROTOCOLPERFORMANCE_H

#include <chrono>
#include <memory>

#include "protocol.pb.h"

#include "MDProtobufMessage.h"
#include "MDProtobufProcessor.h"

#include <cxxtest/TestSuite.h>

using Processor_t = MDProtobufProcessor<ru_sktbelpa_AmpermeterX4_Request,
                                        ru_sktbelpa_AmpermeterX4_Response>;

class TestMDProtocolPerformance : public CxxTest::TestSuite {
private:
  class MyProtocolProcessor : public Processor_t::ProtocolProcessor {
  public:
    bool processRequest(const Processor_t::Request_t &req,
                        Processor_t::Response_t &resp) override {
      resp.id = req.id;
      resp.deviceID = ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID;
      resp.protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION;
      resp.Global_status =
          ((req.deviceID == ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER) ||
           (req.deviceID == ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID)) &&
                  (req.protocolVersion <=
                   ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION)
              ? ru_sktbelpa_AmpermeterX4_Response_STATUS_OK
              : ru_sktbelpa_AmpermeterX4_Response_STATUS_PROTOCOL_ERROR;
      return true;
    }

    std::vector<uint8_t> createProtocolErrorMessage() const override {
      return std::vector<uint8_t>();
    }
  };

public:
  void testFullperform() {
    ru_sktbelpa_AmpermeterX4_Request req{
        .id = 42,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
        .protocolVersion = 0};

    auto ser_req = pHalper_serialiser->Serialise(req);

    std::vector<uint8_t> test_resp;
    pProcessor->process(ser_req.data(), ser_req.size(), test_resp);

    TS_ASSERT_EQUALS(false, test_resp.empty());

    bool isOk;

    auto resp = pHalper_deserialiser->Deserialise(test_resp.data(),
                                                  test_resp.size(), &isOk);

    TS_ASSERT_EQUALS(true, isOk);
    TS_ASSERT_EQUALS(req.id, resp.id);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID,
                     resp.deviceID);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
                     resp.protocolVersion);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_Response_STATUS_OK,
                     resp.Global_status);
  }

  void testPreformance() {
    const int calls = 10000;

    ru_sktbelpa_AmpermeterX4_Request req{
        .id = 42,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
        .protocolVersion = 0};

    auto ser_req = pHalper_serialiser->Serialise(req);
    std::vector<uint8_t> test_resp;

    auto start = std::chrono::system_clock::now();
    for (int i = 0; i < calls; ++i) {
      pProcessor->process(ser_req.data(), ser_req.size(), test_resp);
      TS_ASSERT_EQUALS(false, test_resp.empty());
    }

    auto end = std::chrono::system_clock::now();
    auto time_taken =
        std::chrono::duration_cast<std::chrono::nanoseconds>(end - start)
            .count();

    printf("\n%d Calls of MDProtobufProcessor::process() takes %u "
           "nanosec. (%.1f calls/sec.)\n",
           calls, time_taken, (float)(1e+9f * calls / time_taken));
  }

  void setUp() override {
    pDeserialiser = new MDProtobufDeserialiser<Processor_t::Request_t>(
        magick, requestFields);
    pSerialiser = new MDProtobufSerialiser<Processor_t::Response_t>(
        magick, responceFields);
    pProtocolProcessor = new MyProtocolProcessor;

    pProcessor =
        new Processor_t(*pDeserialiser, *pSerialiser, *pProtocolProcessor);

    pHalper_serialiser =
        new MDProtobufSerialiser<Processor_t::Request_t>(magick, requestFields);
    pHalper_deserialiser = new MDProtobufDeserialiser<Processor_t::Response_t>(
        magick, responceFields);
  }

  void tearDown() override {
    delete pDeserialiser;
    delete pSerialiser;
    delete pProtocolProcessor;

    delete pProcessor;

    delete pHalper_serialiser;
    delete pHalper_deserialiser;
  }

private:
  MDProtobufDeserialiser<Processor_t::Request_t> *pDeserialiser;
  MDProtobufSerialiser<Processor_t::Response_t> *pSerialiser;
  MyProtocolProcessor *pProtocolProcessor;

  Processor_t *pProcessor;

  MDProtobufSerialiser<Processor_t::Request_t> *pHalper_serialiser;
  MDProtobufDeserialiser<Processor_t::Response_t> *pHalper_deserialiser;

  static constexpr const uint8_t magick = ru_sktbelpa_AmpermeterX4_INFO_MAGICK;
  static const pb_field_t *requestFields;
  static const pb_field_t *responceFields;
};

constexpr const uint8_t TestMDProtocolPerformance::magick;
const pb_field_t *TestMDProtocolPerformance::requestFields =
    ru_sktbelpa_AmpermeterX4_Request_fields;
const pb_field_t *TestMDProtocolPerformance::responceFields =
    ru_sktbelpa_AmpermeterX4_Response_fields;

#endif // TESTMDPROTOCOLPERFORMANCE_H
