#ifndef TEST_LAMBDATOADRESS_H
#define TEST_LAMBDATOADRESS_H

#include <functional>

#include "ComparableFunction.h"

#include <cxxtest/TestSuite.h>

class TestComparableFunction : public CxxTest::TestSuite {
public:
  void testEqualSelf() {
    const auto lambda = COMPFUN(void(), []() {});

    TS_ASSERT_EQUALS(lambda, lambda);
  }

  void testEqualanalog() {
    const auto lambda1 = COMPFUN(void(), []() {});
    const auto lambda2 = COMPFUN(void(), []() {});

    TS_ASSERT_EQUALS(lambda1, lambda2);
  }

  void testNotEqual() {
    const auto lambda1 = COMPFUN(void(), []() {});
    const auto lambda2 = COMPFUN(void(), []() { /**/ });

    TS_ASSERT_DIFFERS(lambda1, lambda2);
  }

  static void testmethod() {}

  void testFunction() {
    const auto lambda = COMPFUN(void(), []() {});
    const auto method = COMPFUN(void(), testmethod);

    TS_ASSERT_DIFFERS(lambda, method);
  }
};

#endif // TEST_LAMBDATOADRESS_H
