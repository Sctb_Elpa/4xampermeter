#ifndef TEST_MBPROTOBUFPROCESSOR_H
#define TEST_MBPROTOBUFPROCESSOR_H

#include <algorithm>
#include <cxxtest/TestSuite.h>

#include "protocol.pb.h"

#include "make_unique.h"
#include "memory"

#include "MDProtobufMessage.h"
#include "MDProtobufProcessor.h"

#include "iconnectionlistener.h"

#define ASSERT_STRINGS_EQUAL(s1, s2) TS_ASSERT_EQUALS(0, strcmp(s1, s2))

using Processor_t = MDProtobufProcessor<ru_sktbelpa_AmpermeterX4_Request,
                                        ru_sktbelpa_AmpermeterX4_Response>;

class MyProtocolProcessor : public Processor_t::ProtocolProcessor {
public:
  bool processRequest(const Processor_t::Request_t &req,
                      Processor_t::Response_t &resp) override {
    resp.id = req.id;
    resp.deviceID = ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID;
    resp.protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION;
    resp.Global_status =
        ((req.deviceID == ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER) ||
         (req.deviceID == ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID)) &&
                (req.protocolVersion <=
                 ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION)
            ? ru_sktbelpa_AmpermeterX4_Response_STATUS_OK
            : ru_sktbelpa_AmpermeterX4_Response_STATUS_PROTOCOL_ERROR;

    if (resp.has_getSettings = req.has_setSettings) {
      strcpy(resp.getSettings.AP_ESSID, "AP_ESSID");
      strcpy(resp.getSettings.AP_password, "AP_password");
      strcpy(resp.getSettings.STA_ESSID, "STA_ESSID");
      strcpy(resp.getSettings.STA_password, "STA_password");
      resp.getSettings.AP_IP.IP_bytes = 0xc0480031;
      resp.getSettings.isAccesPointEnabled = true;
      resp.getSettings.isStationEnabled = true;
      resp.getSettings.status =
          ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_OK;
    }

    if (resp.has_measureResult = req.has_measureRequest) {
      if (resp.measureResult.has_Voltage = req.measureRequest.has_getVoltage)
        resp.measureResult.Voltage = 5.5f;

      if (req.measureRequest.has_getCurrentChanels) {
        auto &count = resp.measureResult.currentResults_count;
        count = 0;
        for (auto ch = ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS_CHANEL_0;
             ch <= ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS_CHANEL_3;
             ch =
                 static_cast<ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS>(ch << 1))
          resp.measureResult.currentResults[count++] =
              ru_sktbelpa_AmpermeterX4_CurrentResult{ch, (float)ch};
      } else {
        resp.measureResult.currentResults_count = 0;
      }
    }

    if (resp.has_outputVoltageState = req.has_outputVoltageCtrl) {
      auto &ovs = resp.outputVoltageState;
      ovs.state =
          ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_OK;
      ovs.calibration_progress = 100;
      ovs.min_voltage = 1.5f;
      ovs.max_voltage = 9.35f;
      ovs.outputVoltageSetup = 5.89f;

      ovs.calibrationCoeffs.k = -1.13f;
      ovs.calibrationCoeffs.b = 1.12f;
    }

    return true;
  }

  std::vector<uint8_t> createProtocolErrorMessage() const override {
    return std::vector<uint8_t>();
  }
};

class TestMDProtobufProcessor : public CxxTest::TestSuite {
public:
  static constexpr const uint8_t magick = ru_sktbelpa_AmpermeterX4_INFO_MAGICK;
  static constexpr const pb_field_t
      *requestFields = ru_sktbelpa_AmpermeterX4_Request_fields,
      *responceFields = ru_sktbelpa_AmpermeterX4_Response_fields;

  MDProtobufDeserialiser<Processor_t::Request_t> deserialiser;
  MDProtobufSerialiser<Processor_t::Response_t> serialiser;
  MyProtocolProcessor protocolProcessor;

  std::unique_ptr<Processor_t> processor;

  MDProtobufSerialiser<Processor_t::Request_t> halper_serialiser;
  MDProtobufDeserialiser<Processor_t::Response_t> halper_deserialiser;

  TestMDProtobufProcessor()
      : deserialiser(magick, requestFields), serialiser(magick, responceFields),
        halper_serialiser(magick, requestFields),
        halper_deserialiser(magick, responceFields) {}

  void setUp() {
    processor = std::move(std::make_unique<Processor_t>(
        deserialiser, serialiser, protocolProcessor));
  }

  void testProcess() {
    ru_sktbelpa_AmpermeterX4_Request req{
        .id = 1389,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
        .protocolVersion = 0};

    auto ser_req = halper_serialiser.Serialise(req);

    std::vector<uint8_t> test_resp;
    processor->process(ser_req.data(), ser_req.size(), test_resp);

    TS_ASSERT_EQUALS(false, test_resp.empty());

    bool isOk;

    auto resp = halper_deserialiser.Deserialise(test_resp.data(),
                                                test_resp.size(), &isOk);

    TS_ASSERT_EQUALS(true, isOk);
    TS_ASSERT_EQUALS(req.id, resp.id);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID,
                     resp.deviceID);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
                     resp.protocolVersion);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_Response_STATUS_OK,
                     resp.Global_status);
  }

  void testProtocolError() {
    const std::vector<ru_sktbelpa_AmpermeterX4_Request> requests{
        ru_sktbelpa_AmpermeterX4_Request{.id = 0, .deviceID = 42},
        ru_sktbelpa_AmpermeterX4_Request{
            .id = 0,
            .deviceID = ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID,
            .protocolVersion = 545}};

    std::for_each(
        requests.begin(), requests.end(),
        [this](const Processor_t::Request_t &req) {
          std::vector<uint8_t> test_resp;
          auto ser_req = halper_serialiser.Serialise(req);
          processor->process(ser_req.data(), ser_req.size(), test_resp);

          TS_ASSERT_EQUALS(false, test_resp.empty());

          bool isOk;

          auto resp = halper_deserialiser.Deserialise(test_resp.data(),
                                                      test_resp.size(), &isOk);

          TS_ASSERT_EQUALS(true, isOk);
          TS_ASSERT_EQUALS(req.id, resp.id);
          TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID,
                           resp.deviceID);
          TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
                           resp.protocolVersion);
          TS_ASSERT_EQUALS(
              ru_sktbelpa_AmpermeterX4_Response_STATUS_PROTOCOL_ERROR,
              resp.Global_status);
        });
  }

  void testRunProcessor() {
    ru_sktbelpa_AmpermeterX4_Request req{
        .id = 1389,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
        .protocolVersion = 0};

    auto ser_req = halper_serialiser.Serialise(req);

    std::vector<uint8_t> test_resp;
    IConnectionListener listener;
    listener.setProcessor(processor.get());

    TS_ASSERT_EQUALS(
        true, listener.runProcessor(ser_req.data(), ser_req.size(), test_resp));
  }

  void testSettingsRead() {
    ru_sktbelpa_AmpermeterX4_Request req{
        .id = 1389,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
        .protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
        .has_setSettings = true};

    std::vector<uint8_t> test_resp;
    auto ser_req = halper_serialiser.Serialise(req);

    processor->process(ser_req.data(), ser_req.size(), test_resp);

    TS_ASSERT_EQUALS(false, test_resp.empty());

    bool isOk;

    auto resp = halper_deserialiser.Deserialise(test_resp.data(),
                                                test_resp.size(), &isOk);

    TS_ASSERT_EQUALS(true, isOk);
    TS_ASSERT_EQUALS(req.id, resp.id);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID,
                     resp.deviceID);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
                     resp.protocolVersion);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_Response_STATUS_OK,
                     resp.Global_status);

    TS_ASSERT_EQUALS(true, resp.has_getSettings);
    TS_ASSERT_EQUALS(resp.getSettings.status,
                     ru_sktbelpa_AmpermeterX4_GetSettings_ErrorDescription_OK);

    ASSERT_STRINGS_EQUAL("AP_ESSID", resp.getSettings.AP_ESSID);
    ASSERT_STRINGS_EQUAL("AP_password", resp.getSettings.AP_password);
    ASSERT_STRINGS_EQUAL("STA_ESSID", resp.getSettings.STA_ESSID);
    ASSERT_STRINGS_EQUAL("STA_password", resp.getSettings.STA_password);
    TS_ASSERT_EQUALS(0xc0480031, resp.getSettings.AP_IP.IP_bytes);
    TS_ASSERT_EQUALS(true, resp.getSettings.isAccesPointEnabled);
    TS_ASSERT_EQUALS(true, resp.getSettings.isStationEnabled);
  }

  void testVoltageRead() {
    ru_sktbelpa_AmpermeterX4_Request req{
        .id = 1389,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
        .protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
        .has_setSettings = false,
        .setSettings = ru_sktbelpa_AmpermeterX4_SetSettings{},
        .has_measureRequest = true,
        .measureRequest = ru_sktbelpa_AmpermeterX4_MeasureRequest{
            .has_getVoltage = true,
            .getVoltage = true,
        }};

    std::vector<uint8_t> test_resp;
    auto ser_req = halper_serialiser.Serialise(req);

    processor->process(ser_req.data(), ser_req.size(), test_resp);

    TS_ASSERT_EQUALS(false, test_resp.empty());

    bool isOk;

    auto resp = halper_deserialiser.Deserialise(test_resp.data(),
                                                test_resp.size(), &isOk);

    TS_ASSERT_EQUALS(true, isOk);
    TS_ASSERT_EQUALS(req.id, resp.id);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_AMPERMETERX4_ID,
                     resp.deviceID);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
                     resp.protocolVersion);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_Response_STATUS_OK,
                     resp.Global_status);

    TS_ASSERT_EQUALS(true, resp.has_measureResult);
    TS_ASSERT_EQUALS(true, resp.measureResult.has_Voltage);
    TS_ASSERT_EQUALS(5.5f, resp.measureResult.Voltage);
  }

  void testCurrentRead() {
    ru_sktbelpa_AmpermeterX4_Request req{
        .id = 1389,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
        .protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
        .has_setSettings = false,
        .setSettings = ru_sktbelpa_AmpermeterX4_SetSettings{},
        .has_measureRequest = true,
        .measureRequest = ru_sktbelpa_AmpermeterX4_MeasureRequest{
            .has_getVoltage = false,
            .getVoltage = false,
            .has_getCurrentChanels = true,
            .getCurrentChanels = ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS_ALL}};

    std::vector<uint8_t> test_resp;
    auto ser_req = halper_serialiser.Serialise(req);

    processor->process(ser_req.data(), ser_req.size(), test_resp);

    TS_ASSERT_EQUALS(false, test_resp.empty());

    bool isOk;

    auto resp = halper_deserialiser.Deserialise(test_resp.data(),
                                                test_resp.size(), &isOk);

    TS_ASSERT_EQUALS(true, isOk);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_Response_STATUS_OK,
                     resp.Global_status);

    TS_ASSERT_EQUALS(true, resp.has_measureResult);
    TS_ASSERT_EQUALS(4, resp.measureResult.currentResults_count);
    int i = 0;
    for (auto ch = ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS_CHANEL_0;
         ch <= ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS_CHANEL_3;
         ch = static_cast<ru_sktbelpa_AmpermeterX4_CURRENT_CHANELS>(ch << 1)) {
      TS_ASSERT_EQUALS(ch, resp.measureResult.currentResults[i].chanel);
      TS_ASSERT_EQUALS(ch, resp.measureResult.currentResults[i++].currentValue);
    }
  }

  void testReadCalibrationData() {
    ru_sktbelpa_AmpermeterX4_Request req{
        .id = 7836,
        .deviceID = ru_sktbelpa_AmpermeterX4_INFO_ID_DISCOVER,
        .protocolVersion = ru_sktbelpa_AmpermeterX4_INFO_PROTOCOL_VERSION,
        .has_setSettings = false,
        .setSettings = ru_sktbelpa_AmpermeterX4_SetSettings{},
        .has_measureRequest = false,
        .measureRequest = ru_sktbelpa_AmpermeterX4_MeasureRequest{},
        .has_outputVoltageCtrl = true,
        .outputVoltageCtrl = ru_sktbelpa_AmpermeterX4_OutputVoltageCtrl{}};

    std::vector<uint8_t> test_resp;
    auto ser_req = halper_serialiser.Serialise(req);

    processor->process(ser_req.data(), ser_req.size(), test_resp);

    TS_ASSERT_EQUALS(false, test_resp.empty());

    bool isOk;

    auto resp = halper_deserialiser.Deserialise(test_resp.data(),
                                                test_resp.size(), &isOk);

    TS_ASSERT_EQUALS(true, isOk);
    TS_ASSERT_EQUALS(ru_sktbelpa_AmpermeterX4_Response_STATUS_OK,
                     resp.Global_status);

    TS_ASSERT_EQUALS(true, resp.has_outputVoltageState);
    TS_ASSERT_EQUALS(
        ru_sktbelpa_AmpermeterX4_OutputVoltageState_ErrorDescription_OK,
        resp.outputVoltageState.state);
    TS_ASSERT_EQUALS(100, resp.outputVoltageState.calibration_progress);
    TS_ASSERT_EQUALS(1.5f, resp.outputVoltageState.min_voltage);
    TS_ASSERT_EQUALS(9.35f, resp.outputVoltageState.max_voltage);
    TS_ASSERT_EQUALS(5.89f, resp.outputVoltageState.outputVoltageSetup);
    TS_ASSERT_EQUALS(-1.13f, resp.outputVoltageState.calibrationCoeffs.k);
    TS_ASSERT_EQUALS(1.12f, resp.outputVoltageState.calibrationCoeffs.b);
  }
};

#endif // TEST_MBPROTOBUFPROCESSOR_H
