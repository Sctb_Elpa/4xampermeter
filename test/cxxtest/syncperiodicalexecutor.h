#ifndef SYNCPERIODICALEXECUTOR_H
#define SYNCPERIODICALEXECUTOR_H

#include "ITimer.h"

class VoltageController;

class SyncPeriodicalExecutor : public TestTimer {
public:
  SyncPeriodicalExecutor() { skip_delay = false; }

  ~SyncPeriodicalExecutor() override { stop(); }

  void start(bool repeating = true) override {
    running = repeating;
    thread = std::thread([this]() {
      while (running) {
        if (!skip_delay) {
          std::this_thread::sleep_for(std::chrono::microseconds(period));
        }
        cb();
      }
    });
  }

  void stop() override {
    if (running) {
      running = false;
      if (std::this_thread::get_id() == thread.get_id()) {
        return;
      }
    }
    thread.join();
  }

  int percentage = 0;
  bool fin = false;

  void calibrationFinished(const VoltageController &c) { fin = true; }
  void calibrationpercentUpdate(const VoltageController &c, int p) {
    percentage = p;
  }

  bool skip_delay;
};

#endif // SYNCPERIODICALEXECUTOR_H
