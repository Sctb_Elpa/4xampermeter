#ifndef TESTLINEARFUNCTION_H
#define TESTLINEARFUNCTION_H

#include "linearfunction.h"
#include <cxxtest/TestSuite.h>
#include <math.h>

#include <pb.h>
#include <pb_decode.h>
#include <pb_encode.h>

#include "halpers.pb.h"

class TestLinearFunction : public CxxTest::TestSuite {
private:
  LinearFunction<float> *f;
  static const _String filename;

public:
  void testZero() { TS_ASSERT_EQUALS((*f)(0), 0); }

  void testRandom() {
    float v = rand() / 1000.0;
    TS_ASSERT_EQUALS((*f)(v), v);
  }

  void testINF() {
    TS_ASSERT_IS_INFINITE((*f)(INFINITY));
    TS_ASSERT(isinf(-(*f)(-INFINITY)));
  }

  void testNAN() { TS_ASSERT_IS_NAN((*f)(NAN)); }

  void setUp() { f = new LinearFunction<float>(); }
  void tearDown() { delete f; }

  void testSetK() {
    const float newK = 10.0;
    f->setK(newK);
    TS_ASSERT_EQUALS(f->getK(), newK);
    TS_ASSERT_EQUALS((*f)(1.0f), 1.0f * newK);
  }

  void testSetB() {
    const float newB = 10.0;
    f->setB(newB);
    TS_ASSERT_EQUALS(f->getB(), newB);
    TS_ASSERT_EQUALS((*f)(1.0f), 1.0f + newB);
  }

  void testWriteCoeffsTo() {
    f->setK(9.78).setB(31.87);

    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs c;

    f->writeCoeffsTo(&c, sizeof(c));

    TS_ASSERT_EQUALS(f->getK(), c.k);
    TS_ASSERT_EQUALS(f->getB(), c.b);

    f->setK(31.0).setB(0.37);

    f->writeCoeffsTo(&c, 0);

    TS_ASSERT_DIFFERS(f->getK(), c.k);
    TS_ASSERT_DIFFERS(f->getB(), c.b);
  }

  void testWriteCoeffsTochild() {
    f->setK(9.78).setB(31.87);

    struct sTest {
      int a;
      ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs calibrationCoeffs;
      float b;
    } s;

    f->writeCoeffsTo(&s.calibrationCoeffs, sizeof(sTest::calibrationCoeffs));

    TS_ASSERT_EQUALS(f->getK(), s.calibrationCoeffs.k);
    TS_ASSERT_EQUALS(f->getB(), s.calibrationCoeffs.b);
  }

  void _testSerialise() {
    f->setK(1.0f).setB(2.1f);

    TS_ASSERT(f->SaveToFile(filename));

    auto fd = fopen(filename.c_str(), "rb");
    auto istream = SPIFFSProtobufFile::istreamFromFile(fd);

    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs res;

    TS_ASSERT(pb_decode(&istream,
                        ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields,
                        &res));
    fclose(fd);

    TS_ASSERT_EQUALS(res.k, f->getK());
    TS_ASSERT_EQUALS(res.b, f->getB());
  }

  void testDeserialise() {
    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs v{.k = 10.3, .b = 3.17};
    auto fd = fopen(filename.c_str(), "wb");
    auto ostream = SPIFFSProtobufFile::ostreamFromFile(fd);

    pb_encode(&ostream, ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields,
              &v);
    fclose(fd);

    f->LoadFromFile(filename);

    TS_ASSERT_EQUALS(v.k, f->getK());
    TS_ASSERT_EQUALS(v.b, f->getB());
  }

  void testSerDeser() {
    f->setK(13.7f).setB(2.1f);

    TS_ASSERT(f->SaveToFile(filename));

    LinearFunction<float> fn;

    fn.LoadFromFile(filename);

    TS_ASSERT_EQUALS(f->getK(), fn.getK());
    TS_ASSERT_EQUALS(f->getB(), fn.getB());
  }

  void testCalibration() {
    std::vector<IMathFunc<float>::Point> data{
        {0.0f, 0.0f}, {0.5f, 0.5f}, {1.0f, 1.0f}};

    f->getCalibrator()->Calibrate(data);

    TS_ASSERT_EQUALS(1.0f, f->getK());
    TS_ASSERT_EQUALS(0.0f, f->getB());
  }
};

const _String TestLinearFunction::filename("/tmp/TestLinearFunction.testfile");

#endif // TESTLINEARFUNCTION_H
