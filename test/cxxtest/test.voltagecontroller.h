#ifndef TESTVOLTAGECONTROLLER_H
#define TESTVOLTAGECONTROLLER_H

#include <functional>
#include <random>

#include "make_unique.h"
#include "syncperiodicalexecutor.h"

#include <cxxtest/TestSuite.h>

#include "ITimer.h"
#include "linearfunction.h"

#include "voltagecontroller.h"

class FakeActor {
public:
  FakeActor() { value = 0.0f; }
  void setValue(float value) { this->value = value; }

  float value;
};

class FakeSensor {
public:
  FakeSensor() { value = 0.0f; }
  float getValue() { return value; }

  float value;
};

class FakeSensorActorConnected {
public:
  FakeSensorActorConnected(FakeActor *pactor) : converter() {
    this->pactor = pactor;
  }
  virtual float getValue() {
    return converter ? (*converter)(pactor->value) : pactor->value;
  }

  std::shared_ptr<IMathFunc<float>> converter;

  FakeActor *pactor;
};

class TestVoltageController : public CxxTest::TestSuite {
public:
  /// Тест проверяет защиту контроллера от работы в
  /// режиме неполной инициализации
  void testEmpty() {
    VoltageController controller;
    float actor_val;

    TS_ASSERT_EQUALS(controller.status(), VoltageController::CONFIG_INCOMPLEAD);
    TS_ASSERT_EQUALS(controller.setVoltage(0.0),
                     VoltageController::CONFIG_INCOMPLEAD);

    controller.setActor(std::make_shared<VoltageActor>(
        [&actor_val](float v) { actor_val = v; }));
    TS_ASSERT_EQUALS(controller.status(), VoltageController::CONFIG_INCOMPLEAD);
    TS_ASSERT_EQUALS(controller.setVoltage(0.0),
                     VoltageController::CONFIG_INCOMPLEAD);

    const float test_val = 42.0f;
    auto f = std::make_shared<LinearFunction<float>>(1.0f / test_val);
    controller.setV2AExpression(f);

    TS_ASSERT_EQUALS(controller.status(), VoltageController::OK);
    TS_ASSERT_EQUALS(controller.setVoltage(test_val), VoltageController::OK);
    TS_ASSERT_EQUALS(actor_val, test_val * f->getK());
  }

  /// Тест проверяет работу лимитов выходного напряжения
  void testLimits() {

    VoltageController controller;
    auto f = std::make_shared<LinearFunction<float>>(1.0f / 20.0f);
    float actor_val;

    controller.setActor(std::make_shared<VoltageActor>(
        [&actor_val](float v) { actor_val = v; }));
    controller.setV2AExpression(f);
    controller.setVoltage_min(10.0f);
    auto after_limit_change_value = actor_val;
    TS_ASSERT_EQUALS(controller.setVoltage(5.0f),
                     VoltageController::OUT_OF_LIMITS);
    TS_ASSERT_EQUALS(actor_val, after_limit_change_value);

    float test_value = 15.0f;
    TS_ASSERT_EQUALS(controller.setVoltage(test_value), VoltageController::OK);
    TS_ASSERT_EQUALS(actor_val, test_value * f->getK());

    controller.setVoltage_max(20.0f);
    TS_ASSERT_EQUALS(controller.setVoltage(42.0f),
                     VoltageController::OUT_OF_LIMITS);
    TS_ASSERT_EQUALS(actor_val, test_value * f->getK());

    test_value = 17.0f;
    TS_ASSERT_EQUALS(controller.setVoltage(test_value), VoltageController::OK);
    TS_ASSERT_EQUALS(actor_val, test_value * f->getK());
  }

  /// Тест проверяет смещение выходного напряжения, если текущее его значение
  /// нахдоится за новой границей
  void testMoveLimits() {
    VoltageController controller;
    auto f = std::make_shared<LinearFunction<float>>(1.0f / 20.0f);
    float actor_val;

    controller.setActor(std::make_shared<VoltageActor>(
        [&actor_val](float v) { actor_val = v; }));
    controller.setV2AExpression(f);

    controller.setVoltage_min(10.0f);
    controller.setVoltage_max(20.0f);
    controller.setVoltage(19.0f);

    float new_max_limit = 15.0f;
    controller.setVoltage_max(new_max_limit);
    TS_ASSERT_EQUALS(actor_val, new_max_limit * f->getK());

    TS_ASSERT_EQUALS(controller.setVoltage(11.0f), VoltageController::OK);

    float new_min_limit = 13.0f;
    controller.setVoltage_min(new_min_limit);
    TS_ASSERT_EQUALS(actor_val, new_min_limit * f->getK());
  }

  /// Тест проверяет работу логики установки выходного напряжения
  void testSetOutputVoltage() {
    VoltageController controller;
    auto f = std::make_shared<LinearFunction<float>>();
    const float test_value = 42.0;
    float actor_val;

    f->setK(1.0f / test_value);

    controller.setActor(std::make_shared<VoltageActor>(
        [&actor_val](float v) { actor_val = v; }));
    controller.setV2AExpression(f);

    TS_ASSERT_EQUALS(controller.setVoltage(test_value), VoltageController::OK);
    TS_ASSERT_EQUALS(actor_val, test_value * f->getK());
  }

  /// Тест проверяет наличие ошибки при попытке установки верхнего лимита
  /// выходного напряжения ниже нижнего и наоборот
  void testLimitsOverlap() {
    VoltageController controller;

    controller.setVoltage_min(10.0f);
    TS_ASSERT_THROWS_ANYTHING(controller.setVoltage_max(9.0f));

    controller.setVoltage_max(20.0f);
    TS_ASSERT_THROWS_ANYTHING(controller.setVoltage_min(25.0f));
  }

  /// Тест проверят корректность управления котроллером актора.
  /// устанавливаемое значение всегда должно быть в диопазоне 0.0f <= v <= 1.0f
  void testOverUnderFlow() {
    VoltageController controller;
    auto f = std::make_shared<LinearFunction<float>>(10.0f);
    float actor_val;

    controller.setActor(std::make_shared<VoltageActor>(
        [&actor_val](float v) { actor_val = v; }));
    controller.setV2AExpression(std::make_shared<LinearFunction<float>>(10.0f));

    TS_ASSERT_EQUALS(controller.setVoltage(1.0f),
                     VoltageController::EXPR_OVERFLOW);
  }

  /// Тест проверяет реакцию конроллера на отсутствие сенсора
  void testNoSensor() {
    VoltageController controller;
    float actor_val;

    controller.setActor(std::make_shared<VoltageActor>(
        [&actor_val](float v) { actor_val = v; }));
    controller.setV2AExpression(std::make_shared<LinearFunction<float>>());

    TS_ASSERT_EQUALS(controller.startCalibration(ITimer::makeTimer()),
                     VoltageController::CONFIG_INCOMPLEAD);
  }

  /// Тест проверяет реакцию контроллера на выходное значение сенсора NAN
  void testSensorNAN() {
    VoltageController controller;
    FakeSensor sensor;
    float actor_val;

    controller.setActor(std::make_shared<VoltageActor>(
        [&actor_val](float v) { actor_val = v; }));
    controller.setV2AExpression(std::make_shared<LinearFunction<float>>());
    controller.setSensor(std::make_shared<VoltageSensor>(
        std::bind(&FakeSensor::getValue, &sensor)));

    sensor.value = NAN;
    TS_ASSERT_EQUALS(controller.startCalibration(ITimer::makeTimer()),
                     VoltageController::SENSOR_ERROR);
  }

  /// Тест проверяет работу с актором
  void testControlActor() {
    FakeActor actor;
    VoltageController controler;
    auto f = std::make_shared<LinearFunction<float>>(-.1, 0.5);
    FakeSensorActorConnected sensor(&actor);
    sensor.converter = f->getInvertedFunction();
    controler.setActor(std::make_shared<VoltageActor>(
        std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
    controler.setV2AExpression(f);
    controler.setSensor(std::make_shared<VoltageSensor>(
        std::bind(&FakeSensorActorConnected::getValue, &sensor)));

    TS_ASSERT_EQUALS(controler.setVoltage(2.7), VoltageController::OK);
    TS_ASSERT_EQUALS((*f)(controler.Voltage()), actor.value);
    TS_ASSERT_EQUALS(controler.Voltage(), controler.actualVoltage());
  }

  /// Тест проверяет работу калибровки на тестовых акторе и сенсоре
  void _RunFakeCalibration(float k, float b) {
    FakeActor actor;
    VoltageController controler;
    auto f = std::make_shared<LinearFunction<float>>();
    FakeSensorActorConnected sensor(&actor);
    auto executor = std::make_shared<SyncPeriodicalExecutor>();

    sensor.converter = std::make_shared<LinearFunction<float>>(k, b);

    controler.setActor(std::make_shared<VoltageActor>(
        std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
    controler.setV2AExpression(f);
    controler.setSensor(std::make_shared<VoltageSensor>(
        std::bind(&FakeSensorActorConnected::getValue, &sensor)));

    executor->skip_delay = true;

    TS_ASSERT_EQUALS(
        controler.startCalibration(
            executor,
            std::make_shared<CalibrationFinishedCB>(
                std::bind(&SyncPeriodicalExecutor::calibrationFinished,
                          executor.get(), std::placeholders::_1)),
            std::make_shared<CalibrationProgressUpdateCB>(std::bind(
                &SyncPeriodicalExecutor::calibrationpercentUpdate,
                executor.get(), std::placeholders::_1, std::placeholders::_2))),
        VoltageController::OK);

    while (!executor->fin)
      ;

    TS_ASSERT_EQUALS(executor->percentage, 100);

    auto inv = sensor.converter->getInvertedFunction();
    auto expected_fun = static_cast<LinearFunction<float> *>(inv.get());

    static const float sigma = 1e-5f;
    TS_ASSERT(fabs(f->getK() - expected_fun->getK()) < sigma);
    TS_ASSERT(fabs(f->getB() - expected_fun->getB()) < sigma);
  }

  void testForwardFakeCalibration() { _RunFakeCalibration(5.1f, -0.38f); }
  void testBackwardFakeCalibration() { _RunFakeCalibration(-7.6f, 8.8f); }

  void testFakeCalibrationoverload() {
    struct FakeSensorActorConnectedOverload : FakeSensorActorConnected {
      FakeSensorActorConnectedOverload(FakeActor *pactor, float overload_lvl,
                                       float noise_D)
          : FakeSensorActorConnected(pactor), noise_D(noise_D),
            overload_lvl(overload_lvl), rd(), gen(rd()) {}

      float getValue() override {
        pactor->value > overload_lvl ? add_noize((*converter)(overload_lvl))
                                     : FakeSensorActorConnected::getValue();
      }

      float add_noize(float value) {
        std::normal_distribution<float> dis(value, noise_D);
        auto random_v = dis(gen);
        return random_v;
      }

    private:
      float overload_lvl, noise_D;

      std::random_device rd;
      std::mt19937 gen;
    };

    const float k = 5.1f, b = -0.38f;

    printf(">>\n");

    FakeActor actor;
    VoltageController controler;
    auto f = std::make_shared<LinearFunction<float>>();
    FakeSensorActorConnectedOverload sensor(&actor, 0.75f, 0.1f);
    auto executor = std::make_shared<SyncPeriodicalExecutor>();

    sensor.converter = std::make_shared<LinearFunction<float>>(k, b);

    controler.setActor(std::make_shared<VoltageActor>(
        std::bind(&FakeActor::setValue, &actor, std::placeholders::_1)));
    controler.setV2AExpression(f);
    controler.setSensor(std::make_shared<VoltageSensor>(
        std::bind(&FakeSensorActorConnected::getValue, &sensor)));

    executor->skip_delay = true;

    TS_ASSERT_EQUALS(
        controler.startCalibration(
            executor,
            std::make_shared<CalibrationFinishedCB>(
                std::bind(&SyncPeriodicalExecutor::calibrationFinished,
                          executor.get(), std::placeholders::_1)),
            std::make_shared<CalibrationProgressUpdateCB>(std::bind(
                &SyncPeriodicalExecutor::calibrationpercentUpdate,
                executor.get(), std::placeholders::_1, std::placeholders::_2))),
        VoltageController::OK);

    while (!executor->fin)
      ;

    TS_ASSERT_EQUALS(executor->percentage, 100);

    auto inv = sensor.converter->getInvertedFunction();
    auto expected_fun = static_cast<LinearFunction<float> *>(inv.get());

    static const float sigma = 5e-2f;
    TS_ASSERT(fabs(f->getK() - expected_fun->getK()) < sigma);
    TS_ASSERT(fabs(f->getB() - expected_fun->getB()) < sigma);
  }
};

#endif // TESTVOLTAGECONTROLLER_H
