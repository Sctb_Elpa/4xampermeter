#ifndef TEST_CXX_H
#define TEST_CXX_H

#include <ctime>

#include <cxxtest/TestSuite.h>

class A {
public:
  A() { TS_WARN("Default constructor called"); }
  ~A() { TS_WARN("Destructor called"); }

  A(const A &lvalue) { TS_WARN("Copy constructor called"); }

  A &operator=(const A &lvalue) {
    TS_WARN("Assignment constructor called");
    return *this;
  }

  A(A &&rvalue) { TS_WARN("Move constructor called"); }

  A &operator=(A &&rvalue) {
    TS_WARN("Move assignment called");
    return *this;
  }

  virtual int method() {
    ++field;
    return 0;
  }

  int field = 0;
};

class TestRVO : public CxxTest::TestSuite {
public:
  A a_method() {
    A variable;
    return variable;
  }

  A b_method() {
    A variable;

    auto t = std::time(nullptr);

    if (t % 2) {
      return variable;
    }

    if (t % 3) {
      variable.field = 1;
      return variable;
    }

    return variable;
  }

  ///////////////////////////////////////////////////

  void testCall_ignore_return() { a_method(); }

  void testSimpeRVO() { auto v = a_method(); }

  void testMultuReturn() { auto v = b_method(); }

  ///////////////////////////////////////////////////

  void testMove() {
    A v;
    v = std::move(a_method());
  }

  void testAssignment() {
    A v = b_method();
    if (v.field)
      v = a_method();
  }
};

class B {
public:
  B(A &a) : a(a) {}

  A &a;
};

class TestReference : public CxxTest::TestSuite {
public:
  void a_method(A &lvalue) { lvalue.field = 42; }

  void b_method(A &avalue) {
    A newVal;
    newVal.field = 42;
    avalue = std::move(newVal);
  }

  void testReferenceArgument() {
    A val;
    val.field = 13;
    a_method(val);
  }

  void testMoveToReference() {
    A val;
    b_method(val);
  }

  void testConstructByReference() {
    A a;
    a.field -= 1;
    B b(a);
    b.a.field = 42;
  }

  class A_ : public A {
  public:
    A_(int v) : A(), v(v) {}

    int method() override {
      --v;
      return 1;
    }

    int v;
  };

  class rvalconstructed {
  public:
    rvalconstructed(A &&a) : a(a) {}

    A &a;
  };

  void testRvalue() {
    // https://stackoverflow.com/a/9492609
    rvalconstructed rv1((A_(1)));

    /* На самом деле так делать нельзя!!!!
     * (https://stackoverflow.com/a/37289365/8065921) Временный объект A_ будет
     * сконструирован и перед конструктору как rvalue, конструктор создат ссылку
     * и сохранит её в новом объекте rvalconstructed. НО!!!! При выходе из
     * конструктора временный объект будет унечтожен И сыылка останется висячей.
     * Дело еще более портится если объект A_ был виртуально наследован. Таблица
     * виртуальных функций будет вообще непонятная. В данном тесте она говорит,
     * что A& ссылка именно на A, не A_
     */
    TS_ASSERT_EQUALS(rv1.a.method(), 0);
  }
};

class TestInterfaceReference : public CxxTest::TestSuite {
public:
  class InhA : public A {
  public:
    InhA() : A() { TS_WARN("InhA()"); }
    ~InhA() { TS_WARN("~InhA()"); }

    double d = 13.48;
  };

  template <typename T> class C {
  public:
    C(T &v) : v(v) {}

    T &v;
  };

  void testInterfaceConstructorRef() {
    InhA inha;
    C<InhA> c(inha);
    c.v.field = 42;
  }
};

#endif // TEST_CXX_H
