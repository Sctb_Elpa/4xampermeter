#ifndef TESTLINEARFUNCCLIBRATOR_H
#define TESTLINEARFUNCCLIBRATOR_H

#include <algorithm>
#include <math.h>
#include <random>

#include <cxxtest/TestSuite.h>

#include "linearfunction.h"

class TestLinearFunctionCalibrator : public CxxTest::TestSuite {
public:
  static std::vector<IMathFunc<float>::Point>
  generateTestData(const float k, const float b, float x0, float dx,
                   int poins_Count) {
    std::vector<IMathFunc<float>::Point> res(poins_Count);

    std::for_each(res.begin(), res.end(),
                  [=, &x0](IMathFunc<float>::Point &point) {
                    point.X = x0;
                    point.Y = x0 * k + b;
                    x0 += dx;
                  });

    return res;
  }

  void testSimpleAprox() {
    auto k = 1.0f;
    auto b = 0.0f;

    auto data = generateTestData(k, b, 0, 1, 25);

    LinearFunction<float> f;
    f.getCalibrator()->Calibrate(data);

    assert_near(k, b, f);
  }

  void TestKB() {
    auto k = -1.0f;
    auto b = -1.0f;

    auto data = generateTestData(k, b, 0, 1, 25);

    LinearFunction<float> f;
    f.getCalibrator()->Calibrate(data);

    assert_near(k, b, f);
  }

  void testINFk() {
    auto k = INFINITY;
    auto b = 0.0f;

    auto data = generateTestData(k, b, 0, 1, 10);

    LinearFunction<float> f;
    f.getCalibrator()->Calibrate(data);

    assert_nan(f);
  }

  void testINFb() {
    auto k = 1.0f;
    auto b = INFINITY;

    auto data = generateTestData(k, b, 0, 1, 10);

    LinearFunction<float> f;
    f.getCalibrator()->Calibrate(data);

    assert_nan(f);
  }

  void test1point() {
    auto k = 1.0f;
    auto b = 1.0f;

    auto data = generateTestData(k, b, 0, 1, 1);

    LinearFunction<float> f;
    f.getCalibrator()->Calibrate(data);

    assert_nan(f);
  }

  void testMinus() {
    LinearFunction<float> f;
    auto data = generateTestData(-1.0f, 1.0f, 0, 1, 10);

    f.getCalibrator()->Calibrate(data);
    TS_ASSERT_EQUALS(-1.0f, f.getK());
    TS_ASSERT_EQUALS(1.0f, f.getB());

    data = generateTestData(1.0f, -1.0f, 0, 1, 10);

    f.getCalibrator()->Calibrate(data);
    TS_ASSERT_EQUALS(1.0f, f.getK());
    TS_ASSERT_EQUALS(-1.0f, f.getB());
  }

  void testReversedPointsOrder() {
    LinearFunction<float> f;
    std::vector<IMathFunc<float>::Point> reversed_data(10);
    auto data = generateTestData(-1.0f, 1.0f, 0, 1, 10);
    std::reverse_copy(data.cbegin(), data.cend(), reversed_data.begin());

    f.getCalibrator()->Calibrate(reversed_data);
    TS_ASSERT_EQUALS(-1.0f, f.getK());
    TS_ASSERT_EQUALS(1.0f, f.getB());

    data = generateTestData(1.0f, -1.0f, 0, 1, 10);
    std::reverse_copy(data.cbegin(), data.cend(), reversed_data.begin());

    f.getCalibrator()->Calibrate(reversed_data);
    TS_ASSERT_EQUALS(1.0f, f.getK());
    TS_ASSERT_EQUALS(-1.0f, f.getB());
  }

#if 0
  void _testOverloadEnd() {
    const auto k = 1.0f, b = 0.0f;
    auto data = generateTestData(k, b, 0, 1, 10);
    auto iterator = data.end();
    auto unchange = 3;

    iterator -= unchange;
    float p = NAN;
    std::for_each(iterator, data.end(), [&p](IMathFunc<float>::Point &item) {
      if (std::isnan(p))
        p = item.Y;
      else
        item.Y = p;
    });

    auto filterd = LinearFunctionCalibrator<float>::filterUnchange(data);

    TS_ASSERT_EQUALS(data.size() - unchange, filterd.size());

    LinearFunction<float> f;
    f.getCalibrator()->Calibrate(data);

    TS_ASSERT_EQUALS(k, f.getK());
    TS_ASSERT_EQUALS(b, f.getB());
  }

  void _testOverloadStart() {
    const auto k = 1.0f, b = 0.0f;
    auto data = generateTestData(k, b, 0, 1, 10);
    auto unchange = 3;
    auto p = (data.begin() + unchange)->Y;
    std::for_each(data.begin(), data.begin() + unchange - 1,
                  [&p](IMathFunc<float>::Point &item) { item.Y = p; });

    auto filterd = LinearFunctionCalibrator<float>::filterUnchange(data);

    TS_ASSERT_EQUALS(data.size() - unchange, filterd.size());

    LinearFunction<float> f;
    f.getCalibrator()->Calibrate(data);

    TS_ASSERT_EQUALS(k, f.getK());
    TS_ASSERT_EQUALS(b, f.getB());
  }

  void _testOverloadBoath() {
    const auto k = 1.0f, b = 0.0f;
    auto data = generateTestData(k, b, 0, 1, 10);
    auto unchange = 3;

    float p = (data.begin() + unchange - 1)->Y;
    std::for_each(data.begin(), data.begin() + unchange - 1,
                  [&p](IMathFunc<float>::Point &item) { item.Y = p; });

    auto iterator = data.end() - unchange;
    p = NAN;
    std::for_each(iterator, data.end(), [&p](IMathFunc<float>::Point &item) {
      if (std::isnan(p))
        p = item.Y;
      else
        item.Y = p;
    });

    auto filterd = LinearFunctionCalibrator<float>::filterUnchange(data);

    TS_ASSERT_EQUALS(data.size() - unchange * 2, filterd.size());

    LinearFunction<float> f;
    f.getCalibrator()->Calibrate(data);

    TS_ASSERT_EQUALS(k, f.getK());
    TS_ASSERT_EQUALS(b, f.getB());
  }

  void _testOverloadBoathNoise() {
    auto data = generateTestData(1, 0, 0, 1, 10);
    auto unchange = 3;
    auto noise_lvl = 0.5f;

    std::random_device
        rd; // Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with
    rd() std::uniform_real_distribution<float> dis(-noise_lvl, noise_lvl);
    std::for_each(
        data.begin(), data.end(),
        [&dis, &gen](IMathFunc<float>::Point &item) { item.Y += dis(gen); });

    float p = (data.begin() + unchange - 1)->Y;
    std::for_each(data.begin(), data.begin() + unchange - 1,
                  [&p](IMathFunc<float>::Point &item) { item.Y = p; });

    auto iterator = data.end() - unchange;
    p = NAN;
    std::for_each(iterator, data.end(), [&p](IMathFunc<float>::Point &item) {
      if (std::isnan(p))
        p = item.Y;
      else
        item.Y = p;
    });

    auto filterd = LinearFunctionCalibrator<float>::filterUnchange(data);

    TS_ASSERT(filterd.size() - (data.size() - unchange * 2) <= 2);
  }
#endif
  //==========================================================================

  static void assert_nan(const LinearFunction<float> &f) {
    TS_ASSERT_IS_NAN(f.getK());
    TS_ASSERT_IS_NAN(f.getB());
  }

  static void assert_near(float k, float b, const LinearFunction<float> &f) {
    static const float sigma = 1e-5f;

    TS_ASSERT(fabs(f.getK() - k) < sigma);
    TS_ASSERT(fabs(f.getB() - b) < sigma);
  }
};

#endif // TESTLINEARFUNCCLIBRATOR_H
