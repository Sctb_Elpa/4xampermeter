#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import calibration_summary_pb2 as summary_pb2


msg = summary_pb2.calibration_pass_summary()
msg.ParseFromString(sys.stdin.buffer.read())

print('\nPoints:')
for p in msg.points:
    print("{};{};{}".format(p.x, p.y, int(p.isSignificantPoint)))

print("\nResult (k, b): {};{}\n".format(msg.result.k, msg.result.b))


