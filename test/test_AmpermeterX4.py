#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pytest
import libAmpermeterX4
import protocol_pb2
import struct
import socket
import time


def print_help():
    print("Возможно вы пытаитесь запустить этот файл напрямую?\n"
          "Запстите: $ TEST_IP=<ip_аддресс> make pb_pytest.run\n")


@pytest.fixture
def device(request):
    if not ('TEST_IP' in os.environ.keys()):
        print("Не указан IP адрес для теста!\n")
        print_help()
        assert 0

    d = libAmpermeterX4.AmpermeterX4_io(os.environ['TEST_IP'])
    d.connect()

    def fin():
        d.disconnect()

    request.addfinalizer(fin)
    return d


@pytest.fixture
def settings_req():
    return libAmpermeterX4.AmpermeterX4_requestBuilder().build_settings_request()


@pytest.fixture
def output_request():
    return libAmpermeterX4.AmpermeterX4_requestBuilder().build_measureValues_request()


@pytest.fixture
def voltage_ctl_request():
    return libAmpermeterX4.AmpermeterX4_requestBuilder().build_voltage_ctl_request()


@pytest.fixture
def network_list_request():
    return libAmpermeterX4.AmpermeterX4_requestBuilder().build_network_list_request()


def ip2int(addr):
    return struct.unpack("!I", socket.inet_aton(addr))[0]


def int2ip(addr):
    return socket.inet_ntoa(struct.pack("!I", addr))

# ############## settings simple tests ###############


def read_settings(device, settings_req):
    resp = device.process_request_sync(settings_req)
    assert resp
    assert resp.getSettings.status == protocol_pb2.GetSettings.OK
    assert resp.Global_status == protocol_pb2.Response.OK
    return resp.getSettings


def restore_settings(device, saved_settings):
    req = settings_req()
    req.setSettings.isStationEnabled = saved_settings.isStationEnabled
    req.setSettings.STA_ESSID = saved_settings.STA_ESSID
    req.setSettings.STA_password = saved_settings.STA_password

    req.setSettings.isAccesPointEnabled = saved_settings.isAccesPointEnabled
    req.setSettings.AP_ESSID = saved_settings.AP_ESSID
    req.setSettings.AP_password = saved_settings.AP_password
    req.setSettings.AP_IP.CopyFrom(saved_settings.AP_IP)
    req.setSettings.refresh_interval_ms = saved_settings.refresh_interval_ms

    resp = device.process_request_sync(req)
    assert resp
    assert isSettingOk(resp)
    assert resp.Global_status == protocol_pb2.Response.OK
    return resp


def test_prepare_request(device):
    assert device


def test_ping(device):
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_ping_request()
    resp = device.process_request_sync(req)
    assert resp


def isSettingOk(resp):
    return resp.getSettings.status == protocol_pb2.GetSettings.OK or \
           resp.getSettings.status == protocol_pb2.GetSettings.REBOOT_REQUIRED


def checkStatus(resp, expects):
    return resp.getSettings.status & ~protocol_pb2.GetSettings.REBOOT_REQUIRED == expects

# ############## settings simple tests ###############


def test_read_settings(device, settings_req):
    read_settings(device, settings_req)


def test_write_settings_back(device, settings_req):
    saved_settings = read_settings(device, settings_req)
    restore_settings(device, saved_settings)


def test_default_settings(device, settings_req):
    r = settings_req.setSettings
    r.isStationEnabled = True
    r.STA_ESSID = 'ZyXEL-ELPA'
    r.STA_password = '1b5b91bad5'

    r.isAccesPointEnabled = True
    r.AP_ESSID = 'AmpermeterX4'
    r.AP_password = ''
    r.AP_IP.IP_bytes = ip2int('192.168.42.100')

    resp = device.process_request_sync(settings_req)
    assert resp
    assert isSettingOk(resp)
    assert resp.Global_status == protocol_pb2.Response.OK


def test_readSettingsAndCalibrationInfo(device):
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_request()
    req.setSettings.CopyFrom(protocol_pb2.SetSettings())
    req.outputVoltageCtrl.CopyFrom(protocol_pb2.OutputVoltageCtrl())
    resp = device.process_request_sync(req)

    assert resp
    assert resp.Global_status == protocol_pb2.Response.OK
    assert resp.getSettings.status == protocol_pb2.GetSettings.OK
    assert resp.outputVoltageState.state == protocol_pb2.OutputVoltageState.OK


@pytest.mark.parametrize("test_ip,result",
    [(ip2int('0.0.0.0'), False),
     (ip2int('255.255.255.255'), False),
     (ip2int('192.168.34.0'), False),
     (ip2int('217.38.45.1'), True),
     (ip2int('192.168.0.119'), True)])
def test_SetIP(device, settings_req, test_ip, result):
    # save
    saved_settings = read_settings(device, settings_req)

    # test
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_settings_request()
    req.setSettings.AP_IP.IP_bytes = test_ip
    resp = device.process_request_sync(req)
    assert resp

    if result:
        assert isSettingOk(resp)
    else:
        assert checkStatus(resp, protocol_pb2.GetSettings.AP_WRONG_IP)

    assert resp.Global_status == (protocol_pb2.Response.OK if result else
        protocol_pb2.Response.ERRORS_IN_SUBCOMMANDS)
    assert result == (resp.getSettings.AP_IP == req.setSettings.AP_IP)

    # restore
    restore_settings(device, saved_settings)
    assert read_settings(device, settings_req).AP_IP == saved_settings.AP_IP

@pytest.mark.parametrize("test_essid,result",
    [('', False),
     ('test(', False),
     ('Ampermeter X4', False),
     ('_-.09AzZa', True)])
def test_setSTA_ESSID(device, settings_req, test_essid, result):
    # save
    saved_settings = read_settings(device, settings_req)

    # test
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_settings_request()
    req.setSettings.STA_ESSID = test_essid
    resp = device.process_request_sync(req)
    assert resp
    if result:
        assert isSettingOk(resp)
    else:
        assert checkStatus(resp, protocol_pb2.GetSettings.STA_ESSID_INCORRECT)

    assert resp.Global_status == protocol_pb2.Response.OK if result else \
        protocol_pb2.Response.ERRORS_IN_SUBCOMMANDS
    assert result == (resp.getSettings.STA_ESSID == req.setSettings.STA_ESSID)

    # restore
    restore_settings(device, saved_settings)
    assert read_settings(device, settings_req).STA_ESSID == saved_settings.STA_ESSID


def test_setSTA_ESSID_LONG(device, settings_req):
    # save
    saved_settings = read_settings(device, settings_req)

    # test
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_settings_request()
    req.setSettings.STA_ESSID = 'VERY_LONG_NAME_1234566789_VERY_LONG_NAME_123456789'
    with pytest.raises(TypeError):
        device.process_request_sync(req)

    assert read_settings(device, settings_req).STA_ESSID == saved_settings.STA_ESSID


@pytest.mark.parametrize("test_pass,result",
    [('', True),
     ('test(', True),
     ('Ampermeter X4', True),
     ('_-.09AzZa%^*/', True)])
def test_setSTA_PASSWORD(device, settings_req, test_pass, result):
    # save
    saved_settings = read_settings(device, settings_req)

    # test
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_settings_request()
    req.setSettings.STA_password = test_pass
    resp = device.process_request_sync(req)
    assert resp
    if result:
        assert isSettingOk(resp)
    else:
        assert resp.getSettings.status == protocol_pb2.GetSettings.STA_PASSWORD_TOO_LONG

    assert resp.Global_status == protocol_pb2.Response.OK if result else \
        protocol_pb2.Response.ERRORS_IN_SUBCOMMANDS
    assert result == (resp.getSettings.STA_password == req.setSettings.STA_password)

    # restore
    restore_settings(device, saved_settings)
    assert read_settings(device, settings_req).STA_password == saved_settings.STA_password


def test_setSTA_PASSWORD_LONG(device, settings_req):
    # save
    saved_settings = read_settings(device, settings_req)

    # test
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_settings_request()
    req.setSettings.STA_password = 'VERY_LONG_PASS_1234566789_VERY_LONG_PASS_123456789'
    with pytest.raises(TypeError):
        device.process_request_sync(req)

    assert read_settings(device, settings_req).STA_password == saved_settings.STA_password


def test_setSTA_enabled(device, settings_req):
    # save
    saved_settings = read_settings(device, settings_req)

    # test
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_settings_request()

    for v in (True, False):
        req.setSettings.isStationEnabled = v

        resp = device.process_request_sync(req)
        assert resp
        assert isSettingOk(resp)
        assert resp.Global_status == protocol_pb2.Response.OK
        assert resp.getSettings.isStationEnabled == req.setSettings.isStationEnabled

    # restore
    restore_settings(device, saved_settings)
    assert read_settings(device, settings_req).isStationEnabled == saved_settings.isStationEnabled


def test_DisableNetwork(device, settings_req):
    settings_req.setSettings.isStationEnabled = False
    settings_req.setSettings.isAccesPointEnabled = False

    resp = device.process_request_sync(settings_req)
    assert resp
    assert resp.Global_status == protocol_pb2.Response.ERRORS_IN_SUBCOMMANDS
    assert resp.getSettings.status == protocol_pb2.GetSettings.BOTH_STA_AP_DISABLED


def test_getOutputVoltage(device, output_request):
    output_request.measureRequest.getVoltage = True
    resp = device.process_request_sync(output_request)
    assert resp
    assert resp.Global_status == protocol_pb2.Response.OK
    assert resp.measureResult.HasField('Voltage')

@pytest.mark.parametrize("chanel_mask,res_len",
    [(protocol_pb2.CURRENT_CHANELS.Value('NO_CHANELS'), 0),
     (protocol_pb2.CURRENT_CHANELS.Value('CHANEL_0'), 1),
     (protocol_pb2.CURRENT_CHANELS.Value('CHANEL_1'), 1),
     (protocol_pb2.CURRENT_CHANELS.Value('CHANEL_2'), 1),
     (protocol_pb2.CURRENT_CHANELS.Value('CHANEL_3'), 1),
     (protocol_pb2.CURRENT_CHANELS.Value('CHANEL_0') | protocol_pb2.CURRENT_CHANELS.Value('CHANEL_1'), 2),
     (protocol_pb2.CURRENT_CHANELS.Value('CHANEL_1') | protocol_pb2.CURRENT_CHANELS.Value('CHANEL_3'), 2),
     (protocol_pb2.CURRENT_CHANELS.Value('ALL'), 4),
     (1 << 5, 0),
     (1 << 10, 0),
     (1030, 2),
     (0xffffffff, 4),
     (0xfffffffe, 3),
    ])
def test_getCurrent(device, output_request, chanel_mask, res_len):
    output_request.measureRequest.getCurrentChanels = chanel_mask
    resp = device.process_request_sync(output_request)
    assert resp
    assert resp.Global_status == protocol_pb2.Response.OK
    l = len(resp.measureResult.currentResults)
    assert l == res_len
    for i in range(l):
        v = resp.measureResult.currentResults._values[i]
        assert v.chanel <= protocol_pb2.CURRENT_CHANELS.Value('CHANEL_3')


def test_GetCalibrationInfo(device, voltage_ctl_request):
    resp = device.process_request_sync(voltage_ctl_request)
    assert resp
    assert resp.Global_status == protocol_pb2.Response.OK
    assert resp.outputVoltageState

    assert resp.outputVoltageState.state == protocol_pb2.OutputVoltageState.OK


@pytest.mark.parametrize("interval,result",
    [(0, False), (1, False), (25, True),
     (35, True), (100, True), (101, False), (200000, False)])
def test_setMeasurePeriod(device, settings_req, interval, result):
    # save
    saved_settings = read_settings(device, settings_req)

    # test
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_settings_request()
    req.setSettings.refresh_interval_ms = interval
    resp = device.process_request_sync(req)
    assert resp

    if result:
        assert isSettingOk(resp)
    else:
        assert resp.getSettings.status == protocol_pb2.GetSettings.REFRASH_INTERVAL_OUT_OF_RANGE

    assert resp.Global_status == protocol_pb2.Response.OK if result else \
        protocol_pb2.Response.ERRORS_IN_SUBCOMMANDS
    assert result == (resp.getSettings.refresh_interval_ms == req.setSettings.refresh_interval_ms)

    if result:
        restored_settings = restore_settings(device, saved_settings).resp.getSettings
        assert restored_settings.refresh_interval_ms == saved_settings.refresh_interval_ms


@pytest.mark.parametrize("voltage,result",
    [(5.5, True), (2, True), (7, True),
     (0, False), (20, False), (-1, False)])
def test_setOutputVoltage(device, voltage_ctl_request, voltage, result):
    voltage_ctl_request.outputVoltageCtrl.newOutputVoltage = voltage
    resp = device.process_request_sync(voltage_ctl_request)

    assert resp
    assert (resp.Global_status == protocol_pb2.Response.ERRORS_IN_SUBCOMMANDS) == (not result)
    assert resp.outputVoltageState

    assert (resp.outputVoltageState.state == protocol_pb2.OutputVoltageState.VOLTAGE_OUT_OF_LIMITS) == (not result)
    assert (resp.outputVoltageState.outputVoltageSetup == voltage) == result

    if result:
        mr = output_request()
        mr.measureRequest.getVoltage = True
        time.sleep(0.075)  # delay for update
        resp_m = device.process_request_sync(mr)

        assert abs(resp_m.measureResult.Voltage - resp.outputVoltageState.outputVoltageSetup) < 1.0


def test_getNetworkList(device, network_list_request):
    network_list_request.wifiScanRequest.startScan = True

    while True:
        try:
            resp = device.process_request_sync(network_list_request)
        except libAmpermeterX4.TimeoutError:
            time.sleep(0.1)
            continue

        assert resp
        assert resp.wifiScanResult

        if resp.wifiScanResult.scanStatus == protocol_pb2.WifiScanResult.SCAN_COMPLEAD:
            break
        else:
            network_list_request.wifiScanRequest.startScan = False


def test_calibration(device):
    r = voltage_ctl_request()
    r.outputVoltageCtrl.startCalibration = True
    r.outputVoltageCtrl.calibration_points_count = 15
    r.outputVoltageCtrl.calibration_point_time_ms = 75
    coeffs = device.process_request_sync(r).outputVoltageState.calibrationCoeffs

    i = 0
    while True:
        req = voltage_ctl_request()
        try:
            resp = device.process_request_sync(req)
        except libAmpermeterX4.TimeoutError:
            continue

        assert resp
        assert resp.Global_status == protocol_pb2.Response.OK
        if resp.outputVoltageState.state != protocol_pb2.OutputVoltageState.CALIBRATION_IN_PROGRESS:
            break

        assert resp.outputVoltageState.calibration_progress <= 100
        time.sleep(0.1)
        i += 1

    assert i > 0

    newcoeffs = device.process_request_sync(voltage_ctl_request()).outputVoltageState.calibrationCoeffs

    assert newcoeffs.k != coeffs.k
    assert newcoeffs.b != coeffs.b


def test_reboot(device):
    req = libAmpermeterX4.AmpermeterX4_requestBuilder().build_reboot_request()
    req.rebootRequest.resetDefaults = True

    with pytest.raises(libAmpermeterX4.TimeoutError):
        device.process_request_sync(req)
