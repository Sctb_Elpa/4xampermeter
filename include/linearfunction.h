#ifndef LINEARFUNCTION_H
#define LINEARFUNCTION_H

#include "make_unique.h"

#include "calibration_summary.pb.h"

#include "spiffsprotobuffile.h"

#include "imathfunc.h"

template <typename Real> class LinearFunctionCalibrator;

template <typename Real> class LinearFunction : public IMathFunc<Real> {
public:
  using typename IMathFunc<Real>::Point;

  LinearFunction(float k = 1.0f, float b = 0.0f) : k(k), b(b) {}

  Real operator()(Real v) const { return k * v + b; }

  Real getK() const { return k; }
  LinearFunction &setK(Real value) {
    k = value;
    return *this;
  }

  Real getB() const { return b; }
  LinearFunction &setB(Real value) {
    b = value;
    return *this;
  }

  std::unique_ptr<ICalibrator<Real>> getCalibrator() override {
    return std::make_unique<LinearFunctionCalibrator<Real>>(*this);
  }

  std::unique_ptr<IMathFunc<Real>> getInvertedFunction() const override {
    return std::make_unique<LinearFunction<Real>>(1.0 / k, -b / k);
  }

  _String print() const override {
    char buf[64];
    vsnprintf_wrap(buf, sizeof(buf), "%f * x + %f", k, b);
    return _String(buf);
  }

  std::unique_ptr<IMathFunc<Real>> clone() const override {
    return std::make_unique<LinearFunction<Real>>(k, b);
  }

  IMathFunc<Real> &avarege(const IMathFunc<Real> &another) override {
    static const float cp[] = {0.0f, 1.0f};
    std::vector<Point> d(2);
    int i = 0;
    for (auto it = d.begin(); it != d.end(); ++it) {
      auto x = cp[i];
      it->X = x;
      it->Y = ((*this)(x) + another(x)) / 2.0f;
      ++i;
    }
    getCalibrator()->Calibrate(d);
    return *this;
  }

  bool isIncreasing(const Point &point) const override { return k > 0; }

  void writeCoeffsTo(void *dest, size_t size) const override {
    if (size != sizeof(ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs))
      return;

    auto pDest =
        static_cast<ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs *>(dest);
    pDest->k = k;
    pDest->b = b;
  }

  void readCoeffsFrom(void *src, size_t size) override {
    if (size != sizeof(ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs))
      return;
    auto pSrc =
        static_cast<ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs *>(src);

    k = pSrc->k;
    b = pSrc->b;
  }

  bool LoadFromFile(const SPIFFSProtobufFile &file) override {
    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs t;

    auto res = file.Decode(
        ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields, &t);
    if (res) {
      k = t.k;
      b = t.b;
    }
    return res;
  }

  bool SaveToFile(const SPIFFSProtobufFile &file) override {
    ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs t{k, b};

    return file.Encode(ru_sktbelpa_AmpermeterX4_LinearCalibrationCoeffs_fields,
                       &t);
  }

private:
  Real k, b;
};

template <typename Real>
class LinearFunctionCalibrator : public ICalibrator<Real> {
public:
  using typename ICalibrator<Real>::Point;
  using typename ICalibrator<Real>::VPoints_cIT;

  LinearFunctionCalibrator(LinearFunction<Real> &function) : f(function) {}

  void Calibrate(const std::vector<Point> &points) {
    if (points.size() < 2) {
      f.setK(std::numeric_limits<Real>::quiet_NaN())
          .setB(std::numeric_limits<Real>::quiet_NaN());
      return;
    }

    doCalc(points.cbegin(), points.cend(), points.size());
  }

  void Calibrate(const VPoints_cIT &pBegin, const VPoints_cIT &pEnd) override {
    auto size = std::distance(pBegin, pEnd);
    if (size < 2) {
      f.setK(std::numeric_limits<Real>::quiet_NaN())
          .setB(std::numeric_limits<Real>::quiet_NaN());
      return;
    }

    doCalc(pBegin, pEnd, size);
  }

private:
  void doCalc(VPoints_cIT pBegin, const VPoints_cIT &pEnd, size_t size) {
    // https://prog-cpp.ru/mnk/
    Real sumx = 0;
    Real sumy = 0;
    Real sumx2 = 0;
    Real sumxy = 0;

    for (; pBegin != pEnd; ++pBegin) {
      sumx += pBegin->X;
      sumy += pBegin->Y;

      sumx2 += pBegin->X * pBegin->X;
      sumxy += pBegin->X * pBegin->Y;
    }

    auto k = (size * sumxy - (sumx * sumy)) / (size * sumx2 - sumx * sumx);
    auto b = (sumy - k * sumx) / size;

    f.setK(k).setB(b);
  }

  LinearFunction<Real> &f;
};

#endif // LINEARFUNCTION_H
