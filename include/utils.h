#ifndef UTILS_H
#define UTILS_H

#include <functional>

#include <pb_encode.h>

namespace Utils {

// https://stackoverflow.com/a/2112111/8065921
constexpr uint32_t const_stringhash(const char *input) {
  return *input
             ? static_cast<uint32_t>(*input) + 33 * const_stringhash(input + 1)
             : 5381;
}

constexpr uint32_t const_binaryhash(const uint8_t *input, const size_t len) {
  return len ? static_cast<uint32_t>(*input) +
                   33 * const_binaryhash(input + 1, len - 1)
             : 5381;
}

template <typename T> constexpr uint32_t const_binaryhash(const T &obj) {
  return const_binaryhash((const uint8_t *)&obj, sizeof(T));
}

// https://stackoverflow.com/a/20141143/8065921
template <typename T, typename U> constexpr size_t offsetOf(U T::*member) {
  return (char *)&((T *)nullptr->*member) - (char *)nullptr;
}

template <typename TString>
bool arg2string_pb_encoder(pb_ostream_t *stream, const pb_field_t *field,
                           void *const *arg) {
  auto str = static_cast<const TString *>(*arg);

  if (!pb_encode_tag_for_field(stream, field))
    return false;

  return pb_encode_string(stream, (const pb_byte_t *)str->c_str(),
                          str->length());
}

} // namespace Utils

#endif // UTILS_H
