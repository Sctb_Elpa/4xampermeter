#ifndef WSCONNECTIONLISTENER_H
#define WSCONNECTIONLISTENER_H

#include "iconnectionlistener.h"
#include "memory"

class HttpServer;
class String;
class WebsocketConnection;
class WebsocketResource;

class WSConnectionListener : public IConnectionListener {
public:
  WSConnectionListener();
  ~WSConnectionListener();
  WSConnectionListener(const WSConnectionListener &that) = delete;

  void registerOnServer(HttpServer *server, const String &path);

protected:
  void wsConnected(WebsocketConnection &socket);
  void wsOnMessage(WebsocketConnection &socket, const String &message);
  void wsOnBinaryDataRessived(WebsocketConnection &socket, uint8_t *data,
                              size_t size);
  void wsOnDisconnect(WebsocketConnection &socket);

private:
  WebsocketResource *wsResource;
};

#endif // WSCONNECTIONLISTENER_H
