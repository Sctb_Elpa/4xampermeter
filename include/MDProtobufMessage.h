#ifndef MDPROTOBUFMMESSAGE_H
#define MDPROTOBUFMMESSAGE_H

#include <pb_decode.h>
#include <pb_encode.h>

#include "IProtobufMessage.h"

template <typename TRes>
class MDProtobufDeserialiser : public IProtobufDeserialiser<TRes> {
public:
  MDProtobufDeserialiser(const char magick, const pb_msgdesc_t *fields)
      : m_magick(magick), m_fields(fields) {}

  TRes Deserialise(const uint8_t data[], const size_t size,
                   bool *isOk = nullptr) const override {
    TRes result;

    auto input_stream = pb_istream_from_buffer(data, size);

    if (!checkMagick(input_stream)) {
      if (isOk)
        *isOk = false;
      return result;
    }

    auto actual_pocket_size = getActualMessageSize(input_stream);
    if ((actual_pocket_size < 0) || (actual_pocket_size > 1500) ||
        (actual_pocket_size + 1 > size)) {
      if (isOk)
        *isOk = false;
      return result;
    }
    input_stream.bytes_left = actual_pocket_size; // force size

    if (!pb_decode(&input_stream, m_fields, &result)) {
      if (isOk)
        *isOk = false;
    } else {
      if (isOk)
        *isOk = true;
    }

    return result;
  }

protected:
  const char m_magick;
  const pb_msgdesc_t *m_fields;

private:
  bool checkMagick(pb_istream_t &isteram) const {
    uint64_t v;
    return pb_decode_varint(&isteram, &v) && (v == m_magick);
  }

  static int32_t getActualMessageSize(pb_istream_t &isteram) {
    uint64_t res;
    return pb_decode_varint(&isteram, &res) ? (res & 0xffffffff) : -1;
  }
};

template <typename TMsg>
class MDProtobufSerialiser : public IProtobufSerialiser<TMsg> {
public:
  MDProtobufSerialiser(const char magick, const pb_msgdesc_t *fields)
      : m_magick(magick), m_fields(fields) {}

  std::vector<uint8_t> Serialise(const TMsg &msg) const override {
    bool calc_ok;

    auto msg_body_size = getMessageBodySize(msg, &calc_ok);
    if (!calc_ok)
      return std::vector<uint8_t>();

    ostream os(getHeaderSize(msg_body_size) + msg_body_size);

    pb_encode_varint(os.pb_stream(), m_magick);
    pb_encode_varint(os.pb_stream(), msg_body_size);
    pb_encode(os.pb_stream(), m_fields, &msg);

    return os.getVector();
  }

  size_t CalcSerialisedSize(const TMsg &msg, bool *isOk = nullptr) override {
    auto msg_body_size = getMessageBodySize(msg, isOk);
    return getHeaderSize(msg_body_size) + msg_body_size;
  }

protected:
  const char m_magick;
  const pb_msgdesc_t *m_fields;

private:
  class ostream {
  private:
    std::vector<uint8_t> dataVector;
    size_t actuallyWriten;
    pb_ostream_t m_stream;

  public:
    ostream(size_t size)
        : dataVector(size), actuallyWriten(0),
          m_stream{
              [](pb_ostream_t *stream, const pb_byte_t *buf, size_t count) {
                // callback не должен полагаться на stream->bytes_written
                // как достоверный счетчик, ибо при создании сабстрима
                // он обнуляется.
                auto _this = static_cast<ostream *>(stream->state);
                memcpy(&_this->dataVector.data()[_this->actuallyWriten], buf,
                       count);
                _this->actuallyWriten += count;
                return true;
              },
              this, dataVector.size(), 0} {}

    pb_ostream_t *pb_stream() { return &m_stream; }
    std::vector<uint8_t> getVector() const { return dataVector; }
  };

  size_t getMessageBodySize(const TMsg &msg, bool *isOk = nullptr) const {
    size_t msg_size = 0;
    bool res = pb_get_encoded_size(&msg_size, m_fields, &msg);
    if (isOk)
      *isOk = res;
    return msg_size;
  }

  size_t getHeaderSize(const size_t msg_body_size) const {
    pb_ostream_t calc_ostream = PB_OSTREAM_SIZING;
    pb_encode_varint(&calc_ostream, msg_body_size);
    return 1 +                         // magick
           calc_ostream.bytes_written; // size_of_body size
  }
};

#endif /* MDPROTOBUFMMESSAGE_H */
