#ifndef _USER_CONFIG_H_
#define _USER_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

// UART config
#ifndef SERIAL_BAUD_RATE
#define SERIAL_BAUD_RATE COM_SPEED_SERIAL
#endif

// ESP SDK config
#define LWIP_OPEN_SRC

#ifndef USE_US_TIMER
#define USE_US_TIMER
#endif

// Default types
#define __CORRECT_ISO_CPP_STDLIB_H_PROTO
#include <limits.h>
#include <stdint.h>

// Override c_types.h include and remove buggy espconn
#define _C_TYPES_H_
#define _NO_ESPCON_

// Updated, compatible version of c_types.h
// Just removed types declared in <stdint.h>
#include <c_types.h>

// System API declarations
#include <esp_systemapi.h>

// C++ Support
//#include <esp_cplusplus.h>
// Extended string conversion for compatibility
#include <stringconversion.h>
// Network base API
#include <lwip_includes.h>

// Beta boards
#define BOARD_ESP01

#ifndef SSD1306_SWITCHCAPVCC
#define SSD1306_SWITCHCAPVCC 0x02
#endif

#ifndef SSD1306_I2C_ADDRESS
#define SSD1306_I2C_ADDRESS 0x3C
#endif

#define SDA_PIN 0
#define SCL_PIN 4

#define DISPLAY_DC 5
#define DISPLAY_RST -1 // not connected
#define DISPLAY_CS 16

#ifdef __cplusplus

class __FlashStringHelper;
}
#endif

#endif
