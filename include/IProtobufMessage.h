#ifndef IPROTOBUFMESSAGEPROCESSOR_H
#define IPROTOBUFMESSAGEPROCESSOR_H

#include <stdint.h>
#include <vector>

template <typename TRes> class IProtobufDeserialiser {
public:
  virtual TRes Deserialise(const uint8_t data[], const size_t size,
                           bool *isOk = nullptr) const = 0;
};

template <typename TMsg> class IProtobufSerialiser {
public:
  virtual std::vector<uint8_t> Serialise(const TMsg &msg) const = 0;
  virtual size_t CalcSerialisedSize(const TMsg &msg, bool *isOk = nullptr) = 0;
};

#endif // IPROTOBUFMESSAGEPROCESSOR_H
