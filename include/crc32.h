#ifndef CRC32_H
#define CRC32_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

extern const uint32_t Crc32Lookup16[16];

uint32_t crc32(const void *data, uint32_t length);

#ifdef __cplusplus
}
#endif

template <typename IT> uint32_t crc32(IT &&getter) {
  uint32_t crc = ~0; // same as 0xFFFFFFFF
  uint8_t current;

  while (getter(&current)) {
    crc = Crc32Lookup16[(crc ^ current) & 0x0F] ^ (crc >> 4);
    crc = Crc32Lookup16[(crc ^ (current >> 4)) & 0x0F] ^ (crc >> 4);
  }

  return ~crc; // same as crc ^ 0xFFFFFFFF
}

#endif // CRC32_H
