#ifndef WEBSERVER_H
#define WEBSERVER_H

class HttpServer;

HttpServer *startWebServer(int port = 80);

#endif // WEBSERVER_H
