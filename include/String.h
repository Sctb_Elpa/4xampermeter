#ifndef STRING_H
#define STRING_H

#ifdef __TEST__
#include <string>
using _String = std::string;
#define __vsnprintf(...) vsnprintf(__VA_ARGS__)
#else
#include "user_config.h"
#include <Wiring/WString.h>
using _String = String;
int m_vsnprintf(char *buf, size_t bufsize, const char *fmt, va_list args);
#define __vsnprintf(...) m_vsnprintf(__VA_ARGS__)
#endif

void vsnprintf_wrap(char *buf, size_t bufsize, const char *fmt, ...);

#endif // STRING_H
