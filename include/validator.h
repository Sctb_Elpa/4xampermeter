#ifndef VALIDATOR_H
#define VALIDATOR_H

template <typename T> class Validator {
public:
  virtual bool validate(const T &value) const = 0;
};

#endif // VALIDATOR_H
