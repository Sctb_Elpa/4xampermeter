#ifndef SETUP_H
#define SETUP_H

class IpAddress;

void ConfigureWIFI_AP();
void ConfigureWIFI_STA();

bool isAPEnabled();
bool isSTAEnabled();
bool isSTAConnected();

IpAddress getAPIP();
IpAddress getSTAIP();

#endif // SETUP_H
