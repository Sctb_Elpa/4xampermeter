#ifndef MYASSERT_H
#define MYASSERT_H

#ifdef __TEST__
#include <stdexcept>
#define myassert(expr)                                                         \
  if (!static_cast<bool>(expr)) {                                              \
    throw new std::runtime_error(#expr);                                       \
  }
#else
#define myassert(expr)                                                         \
  (static_cast<bool>(expr)                                                     \
       ? void(0)                                                               \
       : __my_assert_fun(#expr, __FILE__, __LINE__, __FUNCTION__))

extern "C" void __my_assert_fun(const char *__assertion, const char *__file,
                                unsigned int __line, const char *__function);
#endif

#endif // MYASSERT_H
