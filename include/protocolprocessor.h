#ifndef PROTOCOLPROCESSOR_H
#define PROTOCOLPROCESSOR_H

#include "protocol.pb.h"

#include "MDProtobufProcessor.h"
#include "utils.h"

class VoltageController;
class Shaduler;
class SensorManager;
class Monitor;

class ProtocolProcessor
    : public MDProtobufProcessor<
          ru_sktbelpa_AmpermeterX4_Request,
          ru_sktbelpa_AmpermeterX4_Response>::ProtocolProcessor {
public:
  using TReq = ru_sktbelpa_AmpermeterX4_Request;
  using TResp = ru_sktbelpa_AmpermeterX4_Response;

  ProtocolProcessor(VoltageController &voltageController, Shaduler &shaduler,
                    SensorManager &sensormanager, Monitor &monitor)
      : cController(voltageController, monitor), shaduler(shaduler),
        sensormanager(sensormanager) {}

  bool processRequest(const TReq &request, TResp &responce) override;
  std::vector<uint8_t> createProtocolErrorMessage() const override;

private:
  void writeOutputVoltage(ru_sktbelpa_AmpermeterX4_MeasureResult &resp);

  void writeCurrentValues(const ru_sktbelpa_AmpermeterX4_MeasureRequest &req,
                          ru_sktbelpa_AmpermeterX4_MeasureResult &resp);

  void setupNewOutputVoltage(float newVoltage, uint32_t &status);

  void startOutputVoltageCalibration(
      const ru_sktbelpa_AmpermeterX4_OutputVoltageCtrl &params,
      uint32_t &status);

  void
  writeCalibrationStatus(ru_sktbelpa_AmpermeterX4_OutputVoltageState &dest);

  class CalibrationController {
  public:
    CalibrationController(VoltageController &voltageController,
                          Monitor &monitor)
        : voltageController(voltageController), monitor(monitor),
          calibrating(false) {}

    bool isCalibrating() const { return calibrating; }
    int32_t CalibrationProgress() const;

    void startCalibration(const uint32_t pointsCount,
                          const uint32_t pointDelay);

    VoltageController &voltageController;

  private:
    int calibrating_percent;
    Monitor &monitor;
    bool calibrating;

    void calibrationFinished(VoltageController &vc);
    void calibrationProgress(VoltageController &vc, int percent);
  } cController;

  Shaduler &shaduler;
  SensorManager &sensormanager;
};

#endif // PROTOCOLPROCESSOR_H
