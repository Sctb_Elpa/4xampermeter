#ifndef IMATHFUNC_H
#define IMATHFUNC_H

#include <memory>
#include <vector>

#include "String.h"

#include "spiffsprotobuffile.h"

#define SMOOTH_SPLINE_MINIMAL
#include "SmoothSpline++.h"

template <typename Real> struct ICalibrator {
  using Point = smsp::Point<Real>;
  using VPoints_cIT = typename std::vector<Point>::const_iterator;

  virtual void Calibrate(const std::vector<Point> &points) = 0;
  virtual void Calibrate(const VPoints_cIT &pBegin,
                         const VPoints_cIT &pEnd) = 0;
};

template <typename Real> struct IMathFunc {
  using Point = smsp::Point<Real>;

  virtual Real operator()(Real) const = 0;
  virtual std::unique_ptr<IMathFunc<Real>> getInvertedFunction() const = 0;
  virtual std::unique_ptr<ICalibrator<Real>> getCalibrator() = 0;
  virtual _String print() const = 0;
  virtual std::unique_ptr<IMathFunc> clone() const = 0;
  virtual IMathFunc &avarege(const IMathFunc &another) = 0;
  virtual bool isIncreasing(const Point &point) const = 0;

  // Записать коэфициенты в указанное место, принимающая сторона должна
  // указать размер, так проверяется что она знает что хочет
  virtual void writeCoeffsTo(void *dest, size_t size) const = 0;

  // Прочитать коэфициенты из указанного места, вызывающая сторона должна
  // указать размер, так проверяется что она знает что хочет
  virtual void readCoeffsFrom(void *src, size_t size) = 0;

  virtual bool LoadFromFile(const SPIFFSProtobufFile &file) = 0;
  virtual bool SaveToFile(const SPIFFSProtobufFile &file) = 0;
};

#endif // IMATHFUNC_H
