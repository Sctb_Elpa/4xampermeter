#ifndef VECTORITHERATOR_H
#define VECTORITHERATOR_H

#include <vector>

#include <Wiring/WVector.h>

template <typename TValue> class VectorItherator {
  using Itherator_type = VectorItherator<TValue>;
  using TVector = Vector<TValue>;

public:
  VectorItherator(const TVector &wvector, unsigned int offset = 0)
      : p_M_current(*(const TValue ***)((uint8_t *)&wvector + sizeof(TVector) -
                                        sizeof(std::nullptr_t))) {
    p_M_current += offset;
  }

  inline bool operator!=(const Itherator_type &another) const {
    return p_M_current != another.p_M_current;
  }

  inline bool operator==(const Itherator_type &another) const {
    return p_M_current == another.p_M_current;
  }

  const TValue &operator*() const { return *_M_current(); }

  const TValue *operator->() const { return _M_current(); }

  Itherator_type &operator++() {
    ++p_M_current;
    return *this;
  }

  Itherator_type operator++(int) { return Itherator_type(p_M_current++); }

  Itherator_type &operator--() {
    --p_M_current;
    return *this;
  }

  Itherator_type operator--(int) { return Itherator_type(p_M_current--); }

  Itherator_type &operator+=(unsigned int __n) {
    p_M_current += __n;
    return *this;
  }

  Itherator_type operator+(unsigned int __n) const {
    return Itherator_type(p_M_current + __n);
  }

  Itherator_type &operator-=(unsigned int __n) {
    p_M_current -= __n;
    return *this;
  }

  Itherator_type operator-(unsigned int __n) const {
    return Itherator_type(p_M_current - __n);
  }

  const TValue **pointer() const { return p_M_current; }

private:
  VectorItherator(const TValue **p) : p_M_current(p) {}

  inline const TValue *_M_current() const { return *p_M_current; }

  const TValue **p_M_current; // массив указателей на реальные объекты
};

#endif // VECTORITHERATOR_H
