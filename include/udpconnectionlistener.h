#ifndef UDPCONNECTIONLISTENER_H
#define UDPCONNECTIONLISTENER_H

#include <IpAddress.h>

#include "iconnectionlistener.h"
#include "make_unique.h"

class UDPConnectionListener;
class UdpConnection;

class UDPConnectionListener : public IConnectionListener {
public:
  UDPConnectionListener();
  ~UDPConnectionListener();
  UDPConnectionListener(const UDPConnectionListener &that) = delete;

  virtual bool listen(int port = 9178);

protected:
  void onRessive(UdpConnection &connection, char *data, int size,
                 IpAddress remoteIP, uint16_t remotePort);

private:
  UdpConnection *connection;
};

#endif // UDPCONNECTIONLISTENER_H
