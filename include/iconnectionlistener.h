#ifndef ICONNECTIONLISTENER_H
#define ICONNECTIONLISTENER_H

#include <processor.h>
#include <stdint.h>

class IConnectionListener {
public:
  virtual void setProcessor(Processor *_processor) { processor = _processor; }
  virtual bool runProcessor(uint8_t *data, int size,
                            std::vector<uint8_t> &result) {
    return processor ? processor->process(data, size, result) : false;
  }

protected:
  Processor *processor = nullptr;
};

#endif // ICONNECTIONLISTENER_H
