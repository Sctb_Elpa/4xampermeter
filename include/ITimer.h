#ifndef IPERIODICALEXECUTOR_H
#define IPERIODICALEXECUTOR_H

#include "make_unique.h"
#include <functional>
#include <stdint.h>

#include <Delegate.h>

class ITimer {
public:
  using TimerCB = Delegate<void()>;

  virtual ~ITimer() {}

  virtual ITimer &initializeMs(const uint32_t milliseconds,
                               const TimerCB &cb) = 0;
  virtual ITimer &initializeUs(const uint32_t microseconds,
                               const TimerCB &cb) = 0;
  virtual void start(bool repeating = true) = 0;
  virtual void stop() = 0;
  virtual void pause() = 0;
  virtual void resume() = 0;
  virtual bool isRunning() const = 0;

  static std::unique_ptr<ITimer> makeTimer();
};

#ifdef __TEST__
#include <atomic>
#include <future>

class TestTimer : public ITimer {
public:
  ~TestTimer() override { stop(); }

  ITimer &initializeMs(const uint32_t milliseconds,
                       const TimerCB &cb) override {
    this->period = milliseconds * 1000;
    this->cb = cb;

    return *this;
  }

  ITimer &initializeUs(const uint32_t microseconds,
                       const TimerCB &cb) override {
    this->period = microseconds;
    this->cb = cb;

    return *this;
  }

  void start(bool repeating = true) override {
    running = repeating;
    thread = std::thread([this]() {
      while (running) {
        std::this_thread::sleep_for(std::chrono::microseconds(period));
        cb();
      }
    });
  }

  void stop() override {
    if (running) {
      running = false;
      thread.join();
    }
  }

  void pause() override {}
  void resume() override {}

  bool isRunning() const override { return running; }

protected:
  std::thread thread;
  uint64_t period;
  TimerCB cb;
  std::atomic<bool> running;
};
#endif

#endif // IPERIODICALEXECUTOR_H
