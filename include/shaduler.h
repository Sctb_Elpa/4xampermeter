#ifndef SHADULER_H
#define SHADULER_H

#include <deque>
#include <functional>
#include <memory>
#include <stdint.h>

#include "ComparableFunction.h"

#include "ITimer.h"

#define SHADULABLE(lambda) COMPFUN(Shaduler::FunctorType, lambda)

class Shaduler {
public:
  using FunctorType = void();
  using DelayedFunction = ComparableFunction<FunctorType>;

  Shaduler(uint32_t period = 1);
  ~Shaduler();

  void start();
  void setPeriod(uint32_t period_ms);
  void shadule(const DelayedFunction &task);

private:
  ITimer::TimerCB executor;

  uint32_t period;
  std::deque<DelayedFunction> queue;
  std::unique_ptr<ITimer> timer;
};

template <typename Callable> void runAfter(Callable &&cb, uint32_t delay) {
  std::shared_ptr<ITimer> timer(ITimer::makeTimer());
  timer
      ->initializeMs(delay, [timer, cb]() { cb(); })
      // Захват timer не даёт возможности умному указателю удалить таймер до
      // момента выхода из лямбды
      .start(false);
}

#endif // SHADULER_H
