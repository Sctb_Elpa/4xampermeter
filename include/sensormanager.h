#ifndef SENSORMANAGER_H
#define SENSORMANAGER_H

#include <initializer_list>
#include <stdint.h>
#include <vector>

#include "ITimer.h"

class Adafruit_INA219;

class SensorManager {
public:
  SensorManager(std::initializer_list<uint8_t> sensors_addrs,
                std::shared_ptr<ITimer> timer = nullptr);
  ~SensorManager();

  SensorManager &begin();
  SensorManager &setCalibration_32V_2A();
  SensorManager &setCalibration_32V_1A();
  SensorManager &setCalibration_16V_400mA();
  SensorManager &setInvertCurrent(bool invert = true) {
    invertCurrent = invert;
    return *this;
  }

  float getBusVoltage_V();
  float getBusVoltage_V_sync();
  float getShuntVoltage_mV(uint8_t chanel = 0);
  float getCurrent_mA(uint8_t chanel = 0);
  bool isInvertCurent() const { return invertCurrent; }

  SensorManager &setUpdateInterval(uint32_t ms = 50);

private:
  void readSensors();

  float busVolage;
  std::vector<Adafruit_INA219> sensors;
  std::vector<float> curentValues;
  std::shared_ptr<ITimer> timer;
  bool invertCurrent;
};

#endif // SENSORMANAGER_H
