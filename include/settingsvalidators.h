#ifndef SETTINGSVALIDATORS_H
#define SETTINGSVALIDATORS_H

#include "String.h"
#include "stdint.h"
#include "validator.h"

namespace SettingsValidators {

using IPv4_t = uint32_t;

class IPValidator : public Validator<IPv4_t> {
public:
  bool validate(const IPv4_t &value) const override {
    uint8_t lb = value >> 24;
    return (value != 0) && (value != 0xffffffff) && (lb != 0) && (lb != 0xff);
  }
};

class StringLengthValidator : public Validator<_String> {
public:
  StringLengthValidator(int max_len = 32) : max_len(max_len) {}

  bool validate(const _String &str) const override {
    return str.length() <= max_len;
  }

private:
  int max_len;
};

struct CharRegion {
  constexpr CharRegion(const char from, const char to)
      : start_char(from), end_char(to) {}
  constexpr CharRegion(const char c) : start_char(c), end_char(c) {}

  bool charInside(const char c) const {
    return c >= start_char && c <= end_char;
  }

  const char start_char, end_char;
};

class ESSIDValidator : public StringLengthValidator {
public:
  ESSIDValidator(int max_len = 32) : StringLengthValidator(max_len) {}

  bool validate(const _String &essid) const override;

private:
  static constexpr CharRegion avalable_symbols_regions[] = {
      {'a', 'z'}, {'A', 'Z'}, {'0', '9'}, '-', '.', '_'};
};

} // namespace SettingsValidators

#endif // SETTINGSVALIDATORS_H
