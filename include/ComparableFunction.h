#ifndef LAMBDAWRAPER_H
#define LAMBDAWRAPER_H

#include <functional>

#include "utils.h"

#define COMPFUN(type, expr)                                                    \
  ComparableFunction<type>(expr, Utils::const_stringhash(#expr))

template <typename T> class ComparableFunction : public std::function<T> {
public:
  ComparableFunction(const std::function<T> &&f, const uint32_t id)
      : std::function<T>(f), id(id) {}

  inline bool operator==(const ComparableFunction &other) const {
    return other.id == id;
  }

  inline bool operator!=(const ComparableFunction &other) const {
    return other.id != id;
  }

  inline uint32_t getID() const { return id; }

private:
  uint32_t id;
};

#endif // LAMBDAWRAPER_H
