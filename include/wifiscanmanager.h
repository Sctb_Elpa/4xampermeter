#ifndef WIFISCANMANAGER_H
#define WIFISCANMANAGER_H

#include "make_unique.h"

#include "protocol.pb.h"

class WifiNetworksHolder;

class WifiScanManager {
public:
  WifiScanManager();
  ~WifiScanManager();

  void StartScan();
  void fillData(ru_sktbelpa_AmpermeterX4_WifiScanResult &field) const;

private:
  void scanComplead(bool sucess, BssList bssList);
  std::unique_ptr<WifiNetworksHolder> wifiNetworksHolder;
  bool isScanningNow;
};

#endif // WIFISCANMANAGER_H
