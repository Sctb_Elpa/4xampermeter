#ifndef TIMEREXECUTOR_H
#define TIMEREXECUTOR_H

#include <Timer.h>
#include <user_config.h>

#include "ITimer.h"

class TimerExecutor : public ITimer {
public:
  ~TimerExecutor() override { stop(); }

  ITimer &initializeMs(const uint32_t milliseconds, const TimerCB &cb) override;
  ITimer &initializeUs(const uint32_t microseconds, const TimerCB &cb) override;

  void start(bool repeating = true) override { timer.start(repeating); }
  void stop() override { timer.stop(); }

  void pause() override { stop(); }
  void resume() override { start(true); }

  bool isRunning() const override { return timer.isStarted(); }

private:
  Timer timer;
};

#endif // TIMEREXECUTOR_H
