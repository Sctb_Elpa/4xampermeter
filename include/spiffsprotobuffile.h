#ifndef SPIFFSPROTOBUFFILE_H
#define SPIFFSPROTOBUFFILE_H

#include <pb.h>

#include "String.h"

class FileStream;

class SPIFFSProtobufFile {
public:
  using F = FileStream;

  SPIFFSProtobufFile(const _String &filename = _String())
      : filename(filename), lastEncodedSize(0) {}

  bool Encode(const pb_msgdesc_t *fields, const void *src_struct) const;
  bool Decode(const pb_msgdesc_t *fields, void *dest_struct) const;

  size_t size() const;

  uint32_t crc32() const;

  _String getFilename() const { return filename; }

  static pb_istream_t istreamFromFile(const F *fd);
  static pb_ostream_t ostreamFromFile(const F *fd);

private:
  _String filename;
  mutable size_t lastEncodedSize;

  bool isHasFile() const { return filename.length() != 0; }
};

#endif // SPIFFSPROTOBUFFILE_H
