#ifndef MD_PROTOBUF_PROCESSOR_H
#define MD_PROTOBUF_PROCESSOR_H

#include <memory>

#include "IProtobufMessage.h"
#include "processor.h"

#if defined(__TEST__)
#define processor_diag_msg(...)
#else
#include "user_config.h"
#define processor_diag_msg(...) debugf(__VA_ARGS__)
#endif

template <typename T, typename U> class MDProtobufProcessor : public Processor {
public:
  using Request_t = T;
  using Response_t = U;

private:
  using this_type = MDProtobufProcessor<Request_t, Response_t>;

public:
  using Serialiser_t = IProtobufSerialiser<Response_t>;
  using Deserialiser_t = IProtobufDeserialiser<Request_t>;

  class ProtocolProcessor {
  public:
    virtual bool processRequest(const Request_t &, Response_t &) = 0;
    virtual std::vector<uint8_t> createProtocolErrorMessage() const = 0;
  };

  explicit MDProtobufProcessor(const Deserialiser_t &deserialiser,
                               const Serialiser_t &serialiser,
                               this_type::ProtocolProcessor &protocolProcessor)
      : m_deserialiser(deserialiser), m_serialiser(serialiser),
        m_processor(protocolProcessor) {}

  bool process(uint8_t *input_data, int size,
               std::vector<uint8_t> &result) override {
    bool isOk;

    auto request = m_deserialiser.Deserialise(input_data,
                                              static_cast<size_t>(size), &isOk);
    if (isOk) {
      processor_diag_msg("Deserialised succesfully");
      Response_t resp{}; // {} garantie of zero-initcialisation
      if (m_processor.processRequest(request, resp)) {
        processor_diag_msg("Processed succesfully");

        result = std::move(m_serialiser.Serialise(resp));

        auto serialise_ok = !result.empty();
        if (!serialise_ok) {
          processor_diag_msg("Responce serialisation failed");
        } else {
          processor_diag_msg("Serialised succesfully");
          /*
          processor_diag_msg(" {");
          for (std::vector<uint8_t>::const_iterator it = result.begin();
               it != result.end(); it++)
            processor_diag_msg("%02X, ", *it);
          processor_diag_msg(" }");
          */
        }
        return serialise_ok;
      }
      return false;
    } else {
      processor_diag_msg("Deserialise failed");
      result = std::move(m_processor.createProtocolErrorMessage());
      return true;
    }
  }

protected:
  const Deserialiser_t &m_deserialiser;
  const Serialiser_t &m_serialiser;
  ProtocolProcessor &m_processor;
};

#endif // MD_PROTOBUF_PROCESSOR_H
