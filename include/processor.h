#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <memory>
#include <stdint.h>
#include <vector>

class Processor {
public:
  virtual bool process(uint8_t *input_data, int size,
                       std::vector<uint8_t> &result) = 0;
};

#endif // PROCESSOR_H
