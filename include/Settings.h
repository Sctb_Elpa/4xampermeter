#ifndef SETTINGS_H
#define SETTINGS_H

#include <SmingCore.h>
#include <user_config.h>

#include "halpers.pb.h"

class Settings {
public:
  Settings();
  void load(const String &filename = ".settings");
  void save();
  void saveTo(const String &filename);
  void resetDefaults();
  void printSettings();

  ru_sktbelpa_AmpermeterX4_StoredSettings s;

protected:
  void resetAndSave(const String &filename);

private:
  bool checkIsChanged();

  String filename;
};

extern Settings settings;

#endif // SETTINGS_H
