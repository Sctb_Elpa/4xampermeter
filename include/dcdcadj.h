#ifndef DCDCADJ_H
#define DCDCADJ_H

#include <stdint.h>

class HardwarePWM;

class DCDCAdj {
public:
  // Default setup F = 2kHz -> period = 12500
  // 12500 / 25 = 500 (count of analogWrite argument avalable)
  DCDCAdj(uint8_t ctrl_pin, uint8_t oe = -1, uint32_t pwmFreq = 4000);
  ~DCDCAdj();
  void begin();
  bool isEnabled() const;
  void enable(bool enable);
  void setValue(float value);

  void setInversion(bool invert);
  bool isInverted() const { return is_inverted; }

private:
  const uint8_t oePin;
  const uint32_t max_duty;

  uint8_t pwm_pin;
  HardwarePWM *HW_pwm;
  bool is_inverted;
};

#endif // DCDCADJ_H
