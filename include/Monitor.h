#ifndef MONITOR_H
#define MONITOR_H

#include "make_unique.h"

class ITimer;
class Adafruit_SSD1306;
class SensorManager;

class Monitor {
public:
  enum class MODE {
    LOGO = 0,
    CALIBRATING = 1,
    NORMAL = 2,
  };

  Monitor(Adafruit_SSD1306 &driver, SensorManager &sensorManager)
      : d(driver), sensorManager(sensorManager), frame_counter(0xffffffff),
        calibr_percent(0), displayNetworkState(displayConfig::INVALID),
        mode(MODE::LOGO) {}
  ~Monitor();

  void begin(std::unique_ptr<ITimer> &&timer);
  void begin(std::unique_ptr<ITimer> &timer);
  Monitor &setCalibrationPercentProgressbar(uint8_t percent);
  void Update();

  Monitor &switchMode(const MODE newMode);
  MODE getMode() const { return mode; }

private:
  Adafruit_SSD1306 &d;
  SensorManager &sensorManager;
  std::unique_ptr<ITimer> updateTimer;
  uint32_t frame_counter;
  uint8_t calibr_percent;
  enum class displayConfig {
    DISPLAY_AP_IP = 0,
    DISPLAY_STA_IP = 1,
    COUNT,
    INVALID = 0xff
  } displayNetworkState;

  MODE mode;

  void _begin();
  void drawWifiState(const bool force);
  bool drawBody();
  void drawVoltage();
  bool drawCurrent();
  bool drawCalibrationProgressBar();
  bool drawVoltageAndCurrent();

  void print_1_current(const uint8_t chanel, const uint16_t minus,
                       const uint16_t V_offset);
  void print_currnet_NAN_detect(const uint8_t chanel, const float I);
};

#endif // MONITOR_H
