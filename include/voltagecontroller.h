#ifndef VOLTAGECONTROLLER_H
#define VOLTAGECONTROLLER_H

#include <functional>

#include "ITimer.h"
#include "String.h"
#include "imathfunc.h"
#include "make_unique.h"

class VoltageController;

using VoltageSensor = std::function<float(void)>;
using VoltageActor = std::function<void(float)>;
using CalibrationFinishedCB = std::function<void(VoltageController &)>;
using CalibrationProgressUpdateCB =
    std::function<void(VoltageController &, int)>;

class VoltageController {
  friend class TestVoltageController;

public:
  enum enStatus {
    OK = 0,
    CONFIG_INCOMPLEAD = 1,
    OUT_OF_LIMITS = 2,
    EXPR_OVERFLOW = 3,
    SENSOR_ERROR = 4,
    IOERROR = 5,
  };

  using Point = smsp::Point<float>;
  using VPoints_cIT = std::vector<VoltageController::Point>::const_iterator;

  VoltageController();

  VoltageController &setActor(const std::shared_ptr<VoltageActor> &actor);
  VoltageController &setSensor(const std::shared_ptr<VoltageSensor> &sensor);
  VoltageController &
  setV2AExpression(const std::shared_ptr<IMathFunc<float>> &expr);

  enStatus setVoltage(const float outputVoltage);
  float Voltage() const { return m_cur_output; }
  float actualVoltage() const;

  enStatus status() const;

  float voltage_min() const { return m_voltage_min; }
  void setVoltage_min(float voltage_min);

  float voltage_max() const { return m_voltage_max; }
  void setVoltage_max(float voltage_max);

  enStatus startCalibration(
      const std::shared_ptr<ITimer> &executor,
      const std::shared_ptr<CalibrationFinishedCB> &calibrationFinishedCB =
          std::make_shared<CalibrationFinishedCB>(),
      const std::shared_ptr<CalibrationProgressUpdateCB> &calibrationInfoCB =
          std::make_shared<CalibrationProgressUpdateCB>(),
      const uint8_t points_Count = 20, const uint32_t sable_delay = 50);

  void Reset();
  enStatus LoadCalibrationData(const _String &filename);
  enStatus SaveCalibrationData(const _String &filename) const;

  std::shared_ptr<IMathFunc<float>> getCalibrationFunctrion() const {
    return m_expr;
  }

private:
  bool isVoltageInLimits(float v) {
    return (v >= m_voltage_min) && (v <= m_voltage_max);
  }

  std::shared_ptr<CalibrationFinishedCB> calibrationFinishedCB;
  std::shared_ptr<CalibrationProgressUpdateCB> calibrationInfoCB;
  std::shared_ptr<VoltageActor> m_actor;
  std::shared_ptr<VoltageSensor> m_sensor;
  std::shared_ptr<IMathFunc<float>> m_expr;
  std::shared_ptr<ITimer> calibrationexecutor;

  std::vector<Point> calibrationData;

  float m_cur_output;
  float m_voltage_min, m_voltage_max;

  uint8_t calibration_step;

  enum enCalibrationStage {
    CALIBRATION_PREPARE = 0,
    CALIBRATION_FORWARD = 1,
    CALIBRATION_CALC = 2,
    CALIBRATION_BACKWARD = 3,
    CALIBRATION_FINISH = 4
  };
  enCalibrationStage calibrationStage;

  void calibrationTimerTick2way();
  void doCalibrationStep();
  void doCalibrationCalc(bool isBackward);
  void writeGeneralSummary();
  void writeCalibrationSummary(const VPoints_cIT &filtred_begin,
                               const VPoints_cIT &filtred_end,
                               const _String &filename);
};

#endif // VOLTAGECONTROLLER_H
